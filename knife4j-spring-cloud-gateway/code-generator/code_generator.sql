/*
 Navicat Premium Data Transfer

 Source Server         : generator@114.55.137.209
 Source Server Type    : MySQL
 Source Server Version : 50649
 Source Host           : 114.55.137.209:3306
 Source Schema         : code_generator

 Target Server Type    : MySQL
 Target Server Version : 50649
 File Encoding         : 65001

 Date: 25/11/2020 14:54:30
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for gen_table
-- ----------------------------
DROP TABLE IF EXISTS `gen_table`;
CREATE TABLE `gen_table`  (
                              `table_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '业务主键',
                              `data_source_id` int(11) NULL DEFAULT NULL COMMENT '数据源主键',
                              `table_name` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '表名称',
                              `table_comment` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '表描述',
                              `sub_table_name` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '关联父表的表名',
                              `sub_table_fk_name` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '本表关联父表的外键名',
                              `class_name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '实体类名称(首字母大写)',
                              `tpl_category` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '若依操作模板（crud单表操作 tree树表操作 sub主子表操作）',
                              `package_name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '生成包路径',
                              `module_name` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '生成模块名',
                              `business_name` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '生成业务名',
                              `function_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '生成功能名',
                              `function_author` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '生成作者',
                              `gen_type` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '生成代码方式（0 zip压缩包 1 自定义路径）',
                              `gen_path` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '生成路径（不填默认项目路径）',
                              `options` varchar(1000) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '其它生成选项',
                              `create_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '创建人',
                              `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
                              `update_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '修改人',
                              `update_time` datetime(0) NULL DEFAULT NULL COMMENT '修改时间',
                              `remark` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '备注信息',
                              PRIMARY KEY (`table_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 75 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '代码生成表业务信息' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of gen_table
-- ----------------------------
INSERT INTO `gen_table` VALUES (0, 0, 'gen_table', '代码生成表业务信息', NULL, NULL, 'GenTable', 'crud', 'com.wangming.generator', 'generator', 'table', '代码生成业务信息', 'wangming', '0', '/', '{}', 'admin', '2020-11-10 23:05:15', NULL, NULL, NULL);

-- ----------------------------
-- Table structure for gen_table_column
-- ----------------------------
DROP TABLE IF EXISTS `gen_table_column`;
CREATE TABLE `gen_table_column`  (
                                     `column_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '字段主键',
                                     `table_id` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '归属表编号',
                                     `column_name` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '列名称',
                                     `column_comment` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '列描述',
                                     `column_type` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '列类型',
                                     `java_type` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT 'JAVA类型',
                                     `java_field` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT 'JAVA字段名',
                                     `is_pk` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '是否主键（1是）',
                                     `is_increment` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '是否自增（1是）',
                                     `is_required` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '是否必填（1是）',
                                     `is_insert` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '是否为插入字段（1是）',
                                     `is_edit` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '是否编辑字段（1是）',
                                     `is_list` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '是否列表字段（1是）',
                                     `is_query` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '是否查询字段（1是）',
                                     `query_type` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '查询方式（EQ等于、NE不等于、GT大于、LT小于、LIKE模糊、BETWEEN范围）',
                                     `html_type` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '显示类型（input文本框、textarea文本域、select下拉框、checkbox复选框、radio单选框、datetime日期控件）',
                                     `dict_type` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '字典类型',
                                     `sort` int(11) NULL DEFAULT NULL COMMENT '排序',
                                     `create_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '创建人',
                                     `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
                                     `update_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '修改人',
                                     `update_time` datetime(0) NULL DEFAULT NULL COMMENT '修改时间',
                                     `remark` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '备注信息',
                                     PRIMARY KEY (`column_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1528 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '代码生成业务字段信息' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of gen_table_column
-- ----------------------------
INSERT INTO `gen_table_column` VALUES (1115, '0', 'table_id', '业务主键', 'int(11)', 'Long', 'tableId', '1', '1', NULL, '1', NULL, NULL, NULL, NULL, 'input', '', 1, 'admin', '2020-11-14 13:50:11', NULL, NULL, NULL);
INSERT INTO `gen_table_column` VALUES (1116, '0', 'data_source_id', '数据源主键', 'int(11)', 'Long', 'dataSourceId', '0', '0', NULL, '1', '1', '1', '1', NULL, 'input', '', 2, 'admin', '2020-11-14 13:50:11', NULL, NULL, NULL);
INSERT INTO `gen_table_column` VALUES (1117, '0', 'table_name', '表名称', 'varchar(200)', 'String', 'tableName', '0', '0', '1', '1', '1', '1', '1', 'LIKE', 'input', '', 3, 'admin', '2020-11-14 13:50:11', NULL, NULL, NULL);
INSERT INTO `gen_table_column` VALUES (1118, '0', 'table_comment', '表描述', 'varchar(500)', 'String', 'tableComment', '0', '0', NULL, '1', '1', '1', '1', NULL, 'textarea', '', 4, 'admin', '2020-11-14 13:50:11', NULL, NULL, NULL);
INSERT INTO `gen_table_column` VALUES (1119, '0', 'sub_table_name', '关联父表的表名', 'varchar(64)', 'String', 'subTableName', '0', '0', NULL, '1', '1', '1', '1', 'LIKE', 'input', '', 5, 'admin', '2020-11-14 13:50:11', NULL, NULL, NULL);
INSERT INTO `gen_table_column` VALUES (1120, '0', 'sub_table_fk_name', '本表关联父表的外键名', 'varchar(64)', 'String', 'subTableFkName', '0', '0', NULL, '1', '1', '1', '1', 'LIKE', 'input', '', 6, 'admin', '2020-11-14 13:50:11', NULL, NULL, NULL);
INSERT INTO `gen_table_column` VALUES (1121, '0', 'class_name', '实体类名称(首字母大写)', 'varchar(100)', 'String', 'className', '0', '0', '1', '1', '1', '1', '1', 'LIKE', 'input', '', 7, 'admin', '2020-11-14 13:50:11', NULL, NULL, NULL);
INSERT INTO `gen_table_column` VALUES (1122, '0', 'tpl_category', '若依操作模板（crud单表操作 tree树表操作 sub主子表操作）', 'varchar(20)', 'String', 'tplCategory', '0', '0', '1', '1', '1', '1', '1', NULL, 'input', '', 8, 'admin', '2020-11-14 13:50:11', NULL, NULL, NULL);
INSERT INTO `gen_table_column` VALUES (1123, '0', 'package_name', '生成包路径', 'varchar(100)', 'String', 'packageName', '0', '0', NULL, '1', '1', '1', '1', 'LIKE', 'input', '', 9, 'admin', '2020-11-14 13:50:11', NULL, NULL, NULL);
INSERT INTO `gen_table_column` VALUES (1124, '0', 'module_name', '生成模块名', 'varchar(30)', 'String', 'moduleName', '0', '0', NULL, '1', '1', '1', '1', 'LIKE', 'input', '', 10, 'admin', '2020-11-14 13:50:11', NULL, NULL, NULL);
INSERT INTO `gen_table_column` VALUES (1125, '0', 'business_name', '生成业务名', 'varchar(30)', 'String', 'businessName', '0', '0', NULL, '1', '1', '1', '1', 'LIKE', 'input', '', 11, 'admin', '2020-11-14 13:50:11', NULL, NULL, NULL);
INSERT INTO `gen_table_column` VALUES (1126, '0', 'function_name', '生成功能名', 'varchar(50)', 'String', 'functionName', '0', '0', NULL, '1', '1', '1', '1', 'LIKE', 'input', '', 12, 'admin', '2020-11-14 13:50:11', NULL, NULL, NULL);
INSERT INTO `gen_table_column` VALUES (1127, '0', 'function_author', '生成作者', 'varchar(50)', 'String', 'functionAuthor', '0', '0', NULL, '1', '1', '1', '1', NULL, 'input', '', 13, 'admin', '2020-11-14 13:50:11', NULL, NULL, NULL);
INSERT INTO `gen_table_column` VALUES (1128, '0', 'gen_type', '生成代码方式（0 zip压缩包 1 自定义路径）', 'char(1)', 'String', 'genType', '0', '0', '1', '1', '1', '1', '1', NULL, 'select', '', 14, 'admin', '2020-11-14 13:50:11', NULL, NULL, NULL);
INSERT INTO `gen_table_column` VALUES (1129, '0', 'gen_path', '生成路径（不填默认项目路径）', 'varchar(200)', 'String', 'genPath', '0', '0', NULL, '1', '1', '1', '1', NULL, 'input', '', 15, 'admin', '2020-11-14 13:50:11', NULL, NULL, NULL);
INSERT INTO `gen_table_column` VALUES (1130, '0', 'options', '其它生成选项', 'varchar(1000)', 'String', 'options', '0', '0', NULL, '1', '1', '1', '1', NULL, 'textarea', '', 16, 'admin', '2020-11-14 13:50:11', NULL, NULL, NULL);
INSERT INTO `gen_table_column` VALUES (1131, '0', 'create_by', '创建人', 'varchar(64)', 'String', 'createBy', '0', '0', NULL, '1', NULL, NULL, NULL, NULL, 'input', '', 17, 'admin', '2020-11-14 13:50:11', NULL, NULL, NULL);
INSERT INTO `gen_table_column` VALUES (1132, '0', 'create_time', '创建时间', 'datetime', 'Date', 'createTime', '0', '0', NULL, '1', NULL, NULL, NULL, NULL, 'datetime', '', 18, 'admin', '2020-11-14 13:50:11', NULL, NULL, NULL);
INSERT INTO `gen_table_column` VALUES (1133, '0', 'update_by', '修改人', 'varchar(64)', 'String', 'updateBy', '0', '0', NULL, '1', '1', NULL, NULL, NULL, 'input', '', 19, 'admin', '2020-11-14 13:50:11', NULL, NULL, NULL);
INSERT INTO `gen_table_column` VALUES (1134, '0', 'update_time', '修改时间', 'datetime', 'Date', 'updateTime', '0', '0', NULL, '1', '1', NULL, NULL, NULL, 'datetime', '', 20, 'admin', '2020-11-14 13:50:11', NULL, NULL, NULL);
INSERT INTO `gen_table_column` VALUES (1135, '0', 'remark', '备注信息', 'varchar(500)', 'String', 'remark', '0', '0', NULL, '1', '1', '1', NULL, NULL, 'textarea', '', 21, 'admin', '2020-11-14 13:50:11', NULL, NULL, NULL);

-- ----------------------------
-- Table structure for sys_config
-- ----------------------------
DROP TABLE IF EXISTS `sys_config`;
CREATE TABLE `sys_config`  (
                               `config_id` int(5) NOT NULL AUTO_INCREMENT COMMENT '参数主键',
                               `config_name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '参数名称',
                               `config_key` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '参数键名',
                               `config_value` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '参数键值',
                               `is_built_in` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT 'N' COMMENT '系统内置（Y是 N否）',
                               `dict_type` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '关联的字典类型',
                               `input_type` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '输入类型（0-text 1-radio 2-select）',
                               `create_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '创建者',
                               `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
                               `update_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '更新者',
                               `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
                               `remark` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '备注',
                               PRIMARY KEY (`config_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 113 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '参数配置表' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of sys_config
-- ----------------------------
INSERT INTO `sys_config` VALUES (100, '代码生成默认作者', 'gen_author', 'wangming', 'Y', NULL, '0', 'admin', '2020-11-14 11:24:57', 'admin', '2020-11-25 11:55:13', '代码生成默认作者');
INSERT INTO `sys_config` VALUES (101, '默认生成包路径', 'gen_package_name', 'com.wangming.generator', 'Y', NULL, '0', 'admin', '2020-11-14 11:32:13', '', NULL, '代码生成默认生成包路径');
INSERT INTO `sys_config` VALUES (102, '代码生成模版', 'gen_template', 'mybatis-plus', 'Y', 'template_type', '1', 'admin', '2020-11-14 11:33:15', 'admin', '2020-11-25 12:03:40', '代码生成模版（mybatis、 mybatis-plus、ruoyi）');
INSERT INTO `sys_config` VALUES (103, '自动去除表前缀', 'gen_auto_remove_pre', 'Y', 'Y', 'sys_yes_no', '1', 'admin', '2020-11-14 11:34:16', '', NULL, '代码生成自动去除表前缀（ Y 是 N 否 ）');
INSERT INTO `sys_config` VALUES (104, '代码生成表前缀', 'gen_table_prefix', 'c_ckc_', 'Y', NULL, '0', 'admin', '2020-11-14 11:35:16', 'admin', '2020-11-15 13:56:43', '代码生成表前缀');
INSERT INTO `sys_config` VALUES (105, '代码预览主题', 'gen_preview_theme', 'googlecode', 'Y', 'preview_theme', '2', 'admin', '2020-11-14 11:36:00', 'admin', '2020-11-25 13:06:36', '默认代码预览主题');
INSERT INTO `sys_config` VALUES (106, '是否生成swagger注解', 'gen_swagger_enable', 'Y', 'Y', 'sys_yes_no', '1', 'admin', '2020-11-14 11:38:11', 'admin', '2020-11-15 01:59:32', '代码生成是否生成swagger注解（Y 是， N 否）');
INSERT INTO `sys_config` VALUES (107, '是否生成excel注解', 'gen_excel_enable', 'N', 'Y', 'sys_yes_no', '1', 'admin', '2020-11-18 10:19:12', 'admin', '2020-11-25 13:06:36', '代码生成是否生成excel注解（Y 是， N 否）');

-- ----------------------------
-- Table structure for sys_data_source
-- ----------------------------
DROP TABLE IF EXISTS `sys_data_source`;
CREATE TABLE `sys_data_source`  (
                                    `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '数据源主键',
                                    `name` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '数据库名称',
                                    `db_type` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '数据库名称',
                                    `oracle_conn_mode` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT 'oracle连接方式',
                                    `service_name_or_sid` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT 'oracle连接服务名或SID',
                                    `host` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '主机地址',
                                    `port` int(11) NULL DEFAULT NULL COMMENT '端口号',
                                    `username` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '连接用户名',
                                    `password` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '连接密码',
                                    `status` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '状态（0正常 1停用）',
                                    `create_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '创建人',
                                    `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
                                    `update_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '修改人',
                                    `update_time` datetime(0) NULL DEFAULT NULL COMMENT '修改时间',
                                    `remark` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '备注信息',
                                    PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 18 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '表数据源配置' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of sys_data_source
-- ----------------------------
INSERT INTO `sys_data_source` VALUES (0, 'code_generator', 'mysql', 'service_name', '', '114.55.137.209', 3306, 'generator', 'generator', '0', 'admin', '2020-11-10 22:41:17', 'admin', '2020-11-10 22:41:17', '演示数据源');

-- ----------------------------
-- Table structure for sys_dict_data
-- ----------------------------
DROP TABLE IF EXISTS `sys_dict_data`;
CREATE TABLE `sys_dict_data`  (
                                  `dict_code` int(11) NOT NULL AUTO_INCREMENT COMMENT '字典编码',
                                  `dict_sort` int(11) NULL DEFAULT NULL COMMENT '字典排序',
                                  `dict_label` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '字典标签',
                                  `dict_value` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '字典键值',
                                  `dict_type` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '字典类型',
                                  `css_class` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '样式属性（其他样式扩展）',
                                  `list_class` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '表格字典样式',
                                  `is_default` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT 'N' COMMENT '系统默认值（Y是 N否）',
                                  `is_built_in` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT 'N' COMMENT '系统内置（Y是 N否）',
                                  `status` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '状态（0正常 1停用）',
                                  `create_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '创建人',
                                  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
                                  `update_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '修改人',
                                  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '修改时间',
                                  `remark` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '备注信息',
                                  PRIMARY KEY (`dict_code`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 136 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '字典数据' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of sys_dict_data
-- ----------------------------
INSERT INTO `sys_dict_data` VALUES (1, 1, '男', '0', 'sys_user_sex', '', '', 'Y', 'Y', '0', 'admin', '2020-11-10 22:04:46', 'admin', '2020-11-10 22:04:46', '男');
INSERT INTO `sys_dict_data` VALUES (2, 2, '女', '1', 'sys_user_sex', '', '', 'N', 'Y', '0', 'admin', '2020-11-10 22:04:46', 'admin', '2020-11-10 22:04:46', '女');
INSERT INTO `sys_dict_data` VALUES (3, 3, '未知', '2', 'sys_user_sex', '', '', 'N', 'Y', '0', 'admin', '2020-11-10 22:04:46', 'admin', '2020-11-10 22:04:46', '未知');
INSERT INTO `sys_dict_data` VALUES (4, 1, '显示', '0', 'sys_show_hide', '', 'primary', 'Y', 'Y', '0', 'admin', '2020-11-10 22:04:46', 'admin', '2020-11-10 22:04:46', '显示');
INSERT INTO `sys_dict_data` VALUES (5, 2, '隐藏', '1', 'sys_show_hide', '', 'danger', 'N', 'Y', '0', 'admin', '2020-11-10 22:04:46', 'admin', '2020-11-10 22:04:46', '隐藏');
INSERT INTO `sys_dict_data` VALUES (6, 1, '正常', '0', 'sys_normal_disable', '', 'primary', 'Y', 'Y', '0', 'admin', '2020-11-10 22:04:47', 'admin', '2020-11-10 22:04:47', '正常');
INSERT INTO `sys_dict_data` VALUES (7, 2, '停用', '1', 'sys_normal_disable', '', 'danger', 'N', 'Y', '0', 'admin', '2020-11-10 22:04:47', 'admin', '2020-11-10 22:04:47', '停用');
INSERT INTO `sys_dict_data` VALUES (8, 1, '正常', '0', 'sys_job_status', '', 'primary', 'Y', 'Y', '0', 'admin', '2020-11-10 22:04:47', 'admin', '2020-11-10 22:04:47', '正常');
INSERT INTO `sys_dict_data` VALUES (9, 2, '暂停', '1', 'sys_job_status', '', 'danger', 'N', 'Y', '0', 'admin', '2020-11-10 22:04:47', 'admin', '2020-11-10 22:04:47', '暂停');
INSERT INTO `sys_dict_data` VALUES (10, 1, '默认', 'DEFAULT', 'sys_job_group', '', '', 'Y', 'Y', '0', 'admin', '2020-11-10 22:04:47', 'admin', '2020-11-10 22:04:47', '默认');
INSERT INTO `sys_dict_data` VALUES (11, 2, '系统', 'SYSTEM', 'sys_job_group', '', '', 'N', 'Y', '0', 'admin', '2020-11-10 22:04:47', 'admin', '2020-11-10 22:04:47', '系统');
INSERT INTO `sys_dict_data` VALUES (12, 1, '是', 'Y', 'sys_yes_no', '', 'primary', 'Y', 'Y', '0', 'admin', '2020-11-10 22:04:47', 'admin', '2020-11-10 22:04:47', '是');
INSERT INTO `sys_dict_data` VALUES (13, 2, '否', 'N', 'sys_yes_no', '', 'danger', 'N', 'Y', '0', 'admin', '2020-11-10 22:04:47', 'admin', '2020-11-10 22:04:47', '否');
INSERT INTO `sys_dict_data` VALUES (14, 1, '成功', '0', 'sys_common_status', '', 'primary', 'N', 'Y', '0', 'admin', '2020-11-10 22:04:47', 'admin', '2020-11-10 22:04:47', '成功');
INSERT INTO `sys_dict_data` VALUES (15, 2, '失败', '1', 'sys_common_status', '', 'danger', 'N', 'Y', '0', 'admin', '2020-11-10 22:04:47', 'admin', '2020-11-10 22:04:47', '失败');
INSERT INTO `sys_dict_data` VALUES (16, 1, 'MySQL', 'mysql', 'sys_db_type', '', 'default', 'Y', 'Y', '0', 'admin', '2020-11-10 22:04:47', 'admin', '2020-11-10 22:04:47', 'MySQL');
INSERT INTO `sys_dict_data` VALUES (17, 2, 'Oracle', 'oracle', 'sys_db_type', '', 'default', 'N', 'Y', '0', 'admin', '2020-11-10 22:04:47', 'admin', '2020-11-10 22:04:47', 'Oracle');
INSERT INTO `sys_dict_data` VALUES (18, 1, '服务名', 'service_name', 'sys_oracle_mode', NULL, 'default', 'Y', 'Y', '0', 'admin', '2020-11-10 22:04:47', 'admin', '2020-11-10 22:04:47', '服务名');
INSERT INTO `sys_dict_data` VALUES (19, 2, 'SID', 'sid', 'sys_oracle_mode', NULL, 'default', 'N', 'Y', '0', 'admin', '2020-11-10 22:04:47', 'admin', '2020-11-10 22:04:47', 'SID');
INSERT INTO `sys_dict_data` VALUES (20, 2, 'SQLite', 'sqlite', 'sys_db_type', '', 'default', 'N', 'Y', '1', 'admin', '2020-11-10 22:04:47', 'admin', '2020-11-10 22:04:47', 'SQLite');
INSERT INTO `sys_dict_data` VALUES (21, 4, 'PostgreSQL', 'pgsql', 'sys_db_type', '', 'default', 'N', 'Y', '0', 'admin', '2020-11-10 22:04:47', 'admin', '2020-11-10 22:04:47', 'PostgreSQL');
INSERT INTO `sys_dict_data` VALUES (22, 5, 'DB2', 'db2', 'sys_db_type', '', 'default', 'N', 'Y', '1', 'admin', '2020-11-10 22:04:47', 'admin', '2020-11-10 22:04:47', 'DB2');
INSERT INTO `sys_dict_data` VALUES (23, 6, 'SQL Server', 'sqlserver', 'sys_db_type', '', 'default', 'N', 'Y', '0', 'admin', '2020-11-10 22:04:47', 'admin', '2020-11-10 22:04:47', 'SQL Server');
INSERT INTO `sys_dict_data` VALUES (25, 1, '默认', 'default', 'dict_list_class', '', 'default', 'Y', 'Y', '0', 'admin', '2020-11-11 17:18:52', NULL, NULL, '默认');
INSERT INTO `sys_dict_data` VALUES (26, 2, '主要', 'primary', 'dict_list_class', '', 'primary', 'N', 'Y', '0', 'admin', '2020-11-11 17:19:24', NULL, NULL, '主要');
INSERT INTO `sys_dict_data` VALUES (27, 3, '成功', 'success', 'dict_list_class', '', 'success', 'N', 'Y', '0', 'admin', '2020-11-11 17:19:57', NULL, NULL, '成功');
INSERT INTO `sys_dict_data` VALUES (28, 4, '信息', 'info', 'dict_list_class', '', 'info', 'N', 'Y', '0', 'admin', '2020-11-11 17:20:27', NULL, NULL, '信息');
INSERT INTO `sys_dict_data` VALUES (29, 5, '警告', 'warning', 'dict_list_class', '', 'warning', 'N', 'Y', '0', 'admin', '2020-11-11 17:20:57', NULL, NULL, '警告');
INSERT INTO `sys_dict_data` VALUES (30, 6, '危险', 'danger', 'dict_list_class', '', 'danger', 'N', 'Y', '0', 'admin', '2020-11-11 17:21:22', NULL, NULL, '危险');
INSERT INTO `sys_dict_data` VALUES (31, 1, 'mybatis', 'mybatis', 'template_type', '', 'default', 'N', 'Y', '0', 'admin', '2020-11-11 17:26:26', 'admin', '2020-11-12 21:40:40', '标准 mybatis crud 模版');
INSERT INTO `sys_dict_data` VALUES (32, 0, 'a11y-dark', 'a11y-dark', 'preview_theme', '', 'default', 'N', 'Y', '0', 'admin', '2020-11-11 17:46:39', NULL, NULL, 'a11y-dark');
INSERT INTO `sys_dict_data` VALUES (33, 1, 'a11y-light', 'a11y-light', 'preview_theme', '', 'default', 'N', 'Y', '0', 'admin', '2020-11-11 17:46:39', NULL, NULL, 'a11y-light');
INSERT INTO `sys_dict_data` VALUES (34, 2, 'agate', 'agate', 'preview_theme', '', 'default', 'N', 'Y', '0', 'admin', '2020-11-11 17:46:39', NULL, NULL, 'agate');
INSERT INTO `sys_dict_data` VALUES (35, 3, 'an-old-hope', 'an-old-hope', 'preview_theme', '', 'default', 'N', 'Y', '0', 'admin', '2020-11-11 17:46:39', NULL, NULL, 'an-old-hope');
INSERT INTO `sys_dict_data` VALUES (36, 4, 'androidstudio', 'androidstudio', 'preview_theme', '', 'default', 'N', 'Y', '0', 'admin', '2020-11-11 17:46:39', NULL, NULL, 'androidstudio');
INSERT INTO `sys_dict_data` VALUES (37, 5, 'arduino-light', 'arduino-light', 'preview_theme', '', 'default', 'N', 'Y', '0', 'admin', '2020-11-11 17:46:39', NULL, NULL, 'arduino-light');
INSERT INTO `sys_dict_data` VALUES (38, 6, 'arta', 'arta', 'preview_theme', '', 'default', 'N', 'Y', '0', 'admin', '2020-11-11 17:46:39', NULL, NULL, 'arta');
INSERT INTO `sys_dict_data` VALUES (39, 7, 'ascetic', 'ascetic', 'preview_theme', '', 'default', 'N', 'Y', '0', 'admin', '2020-11-11 17:46:39', NULL, NULL, 'ascetic');
INSERT INTO `sys_dict_data` VALUES (40, 8, 'atelier-cave-dark', 'atelier-cave-dark', 'preview_theme', '', 'default', 'N', 'Y', '0', 'admin', '2020-11-11 17:46:39', NULL, NULL, 'atelier-cave-dark');
INSERT INTO `sys_dict_data` VALUES (41, 9, 'atelier-cave-light', 'atelier-cave-light', 'preview_theme', '', 'default', 'N', 'Y', '0', 'admin', '2020-11-11 17:46:39', NULL, NULL, 'atelier-cave-light');
INSERT INTO `sys_dict_data` VALUES (42, 10, 'atelier-dune-dark', 'atelier-dune-dark', 'preview_theme', '', 'default', 'N', 'Y', '0', 'admin', '2020-11-11 17:46:39', NULL, NULL, 'atelier-dune-dark');
INSERT INTO `sys_dict_data` VALUES (43, 11, 'atelier-dune-light', 'atelier-dune-light', 'preview_theme', '', 'default', 'N', 'Y', '0', 'admin', '2020-11-11 17:46:39', NULL, NULL, 'atelier-dune-light');
INSERT INTO `sys_dict_data` VALUES (44, 12, 'atelier-estuary-dark', 'atelier-estuary-dark', 'preview_theme', '', 'default', 'N', 'Y', '0', 'admin', '2020-11-11 17:46:40', NULL, NULL, 'atelier-estuary-dark');
INSERT INTO `sys_dict_data` VALUES (45, 13, 'atelier-estuary-light', 'atelier-estuary-light', 'preview_theme', '', 'default', 'N', 'Y', '0', 'admin', '2020-11-11 17:46:40', NULL, NULL, 'atelier-estuary-light');
INSERT INTO `sys_dict_data` VALUES (46, 14, 'atelier-forest-dark', 'atelier-forest-dark', 'preview_theme', '', 'default', 'N', 'Y', '0', 'admin', '2020-11-11 17:46:40', NULL, NULL, 'atelier-forest-dark');
INSERT INTO `sys_dict_data` VALUES (47, 15, 'atelier-forest-light', 'atelier-forest-light', 'preview_theme', '', 'default', 'N', 'Y', '0', 'admin', '2020-11-11 17:46:40', NULL, NULL, 'atelier-forest-light');
INSERT INTO `sys_dict_data` VALUES (48, 16, 'atelier-heath-dark', 'atelier-heath-dark', 'preview_theme', '', 'default', 'N', 'Y', '0', 'admin', '2020-11-11 17:46:40', NULL, NULL, 'atelier-heath-dark');
INSERT INTO `sys_dict_data` VALUES (49, 17, 'atelier-heath-light', 'atelier-heath-light', 'preview_theme', '', 'default', 'N', 'Y', '0', 'admin', '2020-11-11 17:46:40', NULL, NULL, 'atelier-heath-light');
INSERT INTO `sys_dict_data` VALUES (50, 18, 'atelier-lakeside-dark', 'atelier-lakeside-dark', 'preview_theme', '', 'default', 'N', 'Y', '0', 'admin', '2020-11-11 17:46:40', NULL, NULL, 'atelier-lakeside-dark');
INSERT INTO `sys_dict_data` VALUES (51, 19, 'atelier-lakeside-light', 'atelier-lakeside-light', 'preview_theme', '', 'default', 'N', 'Y', '0', 'admin', '2020-11-11 17:46:40', NULL, NULL, 'atelier-lakeside-light');
INSERT INTO `sys_dict_data` VALUES (52, 20, 'atelier-plateau-dark', 'atelier-plateau-dark', 'preview_theme', '', 'default', 'N', 'Y', '0', 'admin', '2020-11-11 17:46:40', NULL, NULL, 'atelier-plateau-dark');
INSERT INTO `sys_dict_data` VALUES (53, 21, 'atelier-plateau-light', 'atelier-plateau-light', 'preview_theme', '', 'default', 'N', 'Y', '0', 'admin', '2020-11-11 17:46:40', NULL, NULL, 'atelier-plateau-light');
INSERT INTO `sys_dict_data` VALUES (54, 22, 'atelier-savanna-dark', 'atelier-savanna-dark', 'preview_theme', '', 'default', 'N', 'Y', '0', 'admin', '2020-11-11 17:46:40', NULL, NULL, 'atelier-savanna-dark');
INSERT INTO `sys_dict_data` VALUES (55, 23, 'atelier-savanna-light', 'atelier-savanna-light', 'preview_theme', '', 'default', 'N', 'Y', '0', 'admin', '2020-11-11 17:46:40', NULL, NULL, 'atelier-savanna-light');
INSERT INTO `sys_dict_data` VALUES (56, 24, 'atelier-seaside-dark', 'atelier-seaside-dark', 'preview_theme', '', 'default', 'N', 'Y', '0', 'admin', '2020-11-11 17:46:40', NULL, NULL, 'atelier-seaside-dark');
INSERT INTO `sys_dict_data` VALUES (57, 25, 'atelier-seaside-light', 'atelier-seaside-light', 'preview_theme', '', 'default', 'N', 'Y', '0', 'admin', '2020-11-11 17:46:40', NULL, NULL, 'atelier-seaside-light');
INSERT INTO `sys_dict_data` VALUES (58, 26, 'atelier-sulphurpool-dark', 'atelier-sulphurpool-dark', 'preview_theme', '', 'default', 'N', 'Y', '0', 'admin', '2020-11-11 17:46:41', NULL, NULL, 'atelier-sulphurpool-dark');
INSERT INTO `sys_dict_data` VALUES (59, 27, 'atelier-sulphurpool-light', 'atelier-sulphurpool-light', 'preview_theme', '', 'default', 'N', 'Y', '0', 'admin', '2020-11-11 17:46:41', NULL, NULL, 'atelier-sulphurpool-light');
INSERT INTO `sys_dict_data` VALUES (60, 28, 'atom-one-dark-reasonable', 'atom-one-dark-reasonable', 'preview_theme', '', 'default', 'N', 'Y', '0', 'admin', '2020-11-11 17:46:41', NULL, NULL, 'atom-one-dark-reasonable');
INSERT INTO `sys_dict_data` VALUES (61, 29, 'atom-one-dark', 'atom-one-dark', 'preview_theme', '', 'default', 'N', 'Y', '0', 'admin', '2020-11-11 17:46:41', NULL, NULL, 'atom-one-dark');
INSERT INTO `sys_dict_data` VALUES (62, 30, 'atom-one-light', 'atom-one-light', 'preview_theme', '', 'default', 'N', 'Y', '0', 'admin', '2020-11-11 17:46:41', NULL, NULL, 'atom-one-light');
INSERT INTO `sys_dict_data` VALUES (63, 31, 'brown-paper', 'brown-paper', 'preview_theme', '', 'default', 'N', 'Y', '0', 'admin', '2020-11-11 17:46:41', NULL, NULL, 'brown-paper');
INSERT INTO `sys_dict_data` VALUES (64, 32, 'codepen-embed', 'codepen-embed', 'preview_theme', '', 'default', 'N', 'Y', '0', 'admin', '2020-11-11 17:46:41', NULL, NULL, 'codepen-embed');
INSERT INTO `sys_dict_data` VALUES (65, 33, 'color-brewer', 'color-brewer', 'preview_theme', '', 'default', 'N', 'Y', '0', 'admin', '2020-11-11 17:46:41', NULL, NULL, 'color-brewer');
INSERT INTO `sys_dict_data` VALUES (66, 34, 'darcula', 'darcula', 'preview_theme', '', 'default', 'N', 'Y', '0', 'admin', '2020-11-11 17:46:41', NULL, NULL, 'darcula');
INSERT INTO `sys_dict_data` VALUES (67, 35, 'dark', 'dark', 'preview_theme', '', 'default', 'N', 'Y', '0', 'admin', '2020-11-11 17:46:41', NULL, NULL, 'dark');
INSERT INTO `sys_dict_data` VALUES (68, 36, 'default', 'default', 'preview_theme', '', 'default', 'N', 'Y', '0', 'admin', '2020-11-11 17:46:41', NULL, NULL, 'default');
INSERT INTO `sys_dict_data` VALUES (69, 37, 'docco', 'docco', 'preview_theme', '', 'default', 'N', 'Y', '0', 'admin', '2020-11-11 17:46:41', NULL, NULL, 'docco');
INSERT INTO `sys_dict_data` VALUES (70, 38, 'dracula', 'dracula', 'preview_theme', '', 'default', 'N', 'Y', '0', 'admin', '2020-11-11 17:46:41', NULL, NULL, 'dracula');
INSERT INTO `sys_dict_data` VALUES (71, 39, 'far', 'far', 'preview_theme', '', 'default', 'N', 'Y', '0', 'admin', '2020-11-11 17:46:41', NULL, NULL, 'far');
INSERT INTO `sys_dict_data` VALUES (72, 40, 'foundation', 'foundation', 'preview_theme', '', 'default', 'N', 'Y', '0', 'admin', '2020-11-11 17:46:41', NULL, NULL, 'foundation');
INSERT INTO `sys_dict_data` VALUES (73, 41, 'github-gist', 'github-gist', 'preview_theme', '', 'default', 'N', 'Y', '0', 'admin', '2020-11-11 17:46:42', NULL, NULL, 'github-gist');
INSERT INTO `sys_dict_data` VALUES (74, 42, 'github', 'github', 'preview_theme', '', 'default', 'N', 'Y', '0', 'admin', '2020-11-11 17:46:42', NULL, NULL, 'github');
INSERT INTO `sys_dict_data` VALUES (75, 43, 'gml', 'gml', 'preview_theme', '', 'default', 'N', 'Y', '0', 'admin', '2020-11-11 17:46:42', NULL, NULL, 'gml');
INSERT INTO `sys_dict_data` VALUES (76, 44, 'googlecode', 'googlecode', 'preview_theme', '', 'default', 'N', 'Y', '0', 'admin', '2020-11-11 17:46:42', NULL, NULL, 'googlecode');
INSERT INTO `sys_dict_data` VALUES (77, 45, 'gradient-dark', 'gradient-dark', 'preview_theme', '', 'default', 'N', 'Y', '0', 'admin', '2020-11-11 17:46:42', NULL, NULL, 'gradient-dark');
INSERT INTO `sys_dict_data` VALUES (78, 46, 'gradient-light', 'gradient-light', 'preview_theme', '', 'default', 'N', 'Y', '0', 'admin', '2020-11-11 17:46:42', NULL, NULL, 'gradient-light');
INSERT INTO `sys_dict_data` VALUES (79, 47, 'grayscale', 'grayscale', 'preview_theme', '', 'default', 'N', 'Y', '0', 'admin', '2020-11-11 17:46:42', NULL, NULL, 'grayscale');
INSERT INTO `sys_dict_data` VALUES (80, 48, 'gruvbox-dark', 'gruvbox-dark', 'preview_theme', '', 'default', 'N', 'Y', '0', 'admin', '2020-11-11 17:46:42', NULL, NULL, 'gruvbox-dark');
INSERT INTO `sys_dict_data` VALUES (81, 49, 'gruvbox-light', 'gruvbox-light', 'preview_theme', '', 'default', 'N', 'Y', '0', 'admin', '2020-11-11 17:46:42', NULL, NULL, 'gruvbox-light');
INSERT INTO `sys_dict_data` VALUES (82, 50, 'hopscotch', 'hopscotch', 'preview_theme', '', 'default', 'N', 'Y', '0', 'admin', '2020-11-11 17:46:42', NULL, NULL, 'hopscotch');
INSERT INTO `sys_dict_data` VALUES (83, 51, 'hybrid', 'hybrid', 'preview_theme', '', 'default', 'N', 'Y', '0', 'admin', '2020-11-11 17:46:42', NULL, NULL, 'hybrid');
INSERT INTO `sys_dict_data` VALUES (84, 52, 'idea', 'idea', 'preview_theme', '', 'default', 'N', 'Y', '0', 'admin', '2020-11-11 17:46:42', NULL, NULL, 'idea');
INSERT INTO `sys_dict_data` VALUES (85, 53, 'ir-black', 'ir-black', 'preview_theme', '', 'default', 'N', 'Y', '0', 'admin', '2020-11-11 17:46:42', NULL, NULL, 'ir-black');
INSERT INTO `sys_dict_data` VALUES (86, 54, 'isbl-editor-dark', 'isbl-editor-dark', 'preview_theme', '', 'default', 'N', 'Y', '0', 'admin', '2020-11-11 17:46:42', NULL, NULL, 'isbl-editor-dark');
INSERT INTO `sys_dict_data` VALUES (87, 55, 'isbl-editor-light', 'isbl-editor-light', 'preview_theme', '', 'default', 'N', 'Y', '0', 'admin', '2020-11-11 17:46:42', NULL, NULL, 'isbl-editor-light');
INSERT INTO `sys_dict_data` VALUES (88, 56, 'kimbie', 'kimbie', 'preview_theme', '', 'default', 'N', 'Y', '0', 'admin', '2020-11-11 17:46:43', NULL, NULL, 'kimbie');
INSERT INTO `sys_dict_data` VALUES (89, 57, 'kimbie', 'kimbie', 'preview_theme', '', 'default', 'N', 'Y', '0', 'admin', '2020-11-11 17:46:43', NULL, NULL, 'kimbie');
INSERT INTO `sys_dict_data` VALUES (90, 58, 'lightfair', 'lightfair', 'preview_theme', '', 'default', 'N', 'Y', '0', 'admin', '2020-11-11 17:46:43', NULL, NULL, 'lightfair');
INSERT INTO `sys_dict_data` VALUES (91, 59, 'lioshi', 'lioshi', 'preview_theme', '', 'default', 'N', 'Y', '0', 'admin', '2020-11-11 17:46:43', NULL, NULL, 'lioshi');
INSERT INTO `sys_dict_data` VALUES (92, 60, 'magula', 'magula', 'preview_theme', '', 'default', 'N', 'Y', '0', 'admin', '2020-11-11 17:46:43', NULL, NULL, 'magula');
INSERT INTO `sys_dict_data` VALUES (93, 61, 'mono-blue', 'mono-blue', 'preview_theme', '', 'default', 'N', 'Y', '0', 'admin', '2020-11-11 17:46:43', NULL, NULL, 'mono-blue');
INSERT INTO `sys_dict_data` VALUES (94, 62, 'monokai-sublime', 'monokai-sublime', 'preview_theme', '', 'default', 'N', 'Y', '0', 'admin', '2020-11-11 17:46:43', NULL, NULL, 'monokai-sublime');
INSERT INTO `sys_dict_data` VALUES (95, 63, 'monokai', 'monokai', 'preview_theme', '', 'default', 'N', 'Y', '0', 'admin', '2020-11-11 17:46:43', NULL, NULL, 'monokai');
INSERT INTO `sys_dict_data` VALUES (96, 64, 'night-owl', 'night-owl', 'preview_theme', '', 'default', 'N', 'Y', '0', 'admin', '2020-11-11 17:46:43', NULL, NULL, 'night-owl');
INSERT INTO `sys_dict_data` VALUES (97, 65, 'nnfx-dark', 'nnfx-dark', 'preview_theme', '', 'default', 'N', 'Y', '0', 'admin', '2020-11-11 17:46:43', NULL, NULL, 'nnfx-dark');
INSERT INTO `sys_dict_data` VALUES (98, 66, 'nnfx', 'nnfx', 'preview_theme', '', 'default', 'N', 'Y', '0', 'admin', '2020-11-11 17:46:43', NULL, NULL, 'nnfx');
INSERT INTO `sys_dict_data` VALUES (99, 67, 'nord', 'nord', 'preview_theme', '', 'default', 'N', 'Y', '0', 'admin', '2020-11-11 17:46:43', NULL, NULL, 'nord');
INSERT INTO `sys_dict_data` VALUES (100, 68, 'obsidian', 'obsidian', 'preview_theme', '', 'default', 'N', 'Y', '0', 'admin', '2020-11-11 17:46:43', NULL, NULL, 'obsidian');
INSERT INTO `sys_dict_data` VALUES (101, 69, 'ocean', 'ocean', 'preview_theme', '', 'default', 'N', 'Y', '0', 'admin', '2020-11-11 17:46:43', NULL, NULL, 'ocean');
INSERT INTO `sys_dict_data` VALUES (102, 70, 'paraiso-dark', 'paraiso-dark', 'preview_theme', '', 'default', 'N', 'Y', '0', 'admin', '2020-11-11 17:46:44', NULL, NULL, 'paraiso-dark');
INSERT INTO `sys_dict_data` VALUES (103, 71, 'paraiso-light', 'paraiso-light', 'preview_theme', '', 'default', 'N', 'Y', '0', 'admin', '2020-11-11 17:46:44', NULL, NULL, 'paraiso-light');
INSERT INTO `sys_dict_data` VALUES (104, 72, 'pojoaque', 'pojoaque', 'preview_theme', '', 'default', 'N', 'Y', '0', 'admin', '2020-11-11 17:46:44', NULL, NULL, 'pojoaque');
INSERT INTO `sys_dict_data` VALUES (105, 73, 'purebasic', 'purebasic', 'preview_theme', '', 'default', 'N', 'Y', '0', 'admin', '2020-11-11 17:46:44', NULL, NULL, 'purebasic');
INSERT INTO `sys_dict_data` VALUES (106, 74, 'qtcreator_dark', 'qtcreator_dark', 'preview_theme', '', 'default', 'N', 'Y', '0', 'admin', '2020-11-11 17:46:44', NULL, NULL, 'qtcreator_dark');
INSERT INTO `sys_dict_data` VALUES (107, 75, 'qtcreator_light', 'qtcreator_light', 'preview_theme', '', 'default', 'N', 'Y', '0', 'admin', '2020-11-11 17:46:44', NULL, NULL, 'qtcreator_light');
INSERT INTO `sys_dict_data` VALUES (108, 76, 'railscasts', 'railscasts', 'preview_theme', '', 'default', 'N', 'Y', '0', 'admin', '2020-11-11 17:46:44', NULL, NULL, 'railscasts');
INSERT INTO `sys_dict_data` VALUES (109, 77, 'rainbow', 'rainbow', 'preview_theme', '', 'default', 'N', 'Y', '0', 'admin', '2020-11-11 17:46:44', NULL, NULL, 'rainbow');
INSERT INTO `sys_dict_data` VALUES (110, 78, 'routeros', 'routeros', 'preview_theme', '', 'default', 'N', 'Y', '0', 'admin', '2020-11-11 17:46:44', NULL, NULL, 'routeros');
INSERT INTO `sys_dict_data` VALUES (111, 79, 'school-book', 'school-book', 'preview_theme', '', 'default', 'N', 'Y', '0', 'admin', '2020-11-11 17:46:44', NULL, NULL, 'school-book');
INSERT INTO `sys_dict_data` VALUES (112, 80, 'shades-of-purple', 'shades-of-purple', 'preview_theme', '', 'default', 'N', 'Y', '0', 'admin', '2020-11-11 17:46:44', NULL, NULL, 'shades-of-purple');
INSERT INTO `sys_dict_data` VALUES (113, 81, 'solarized-dark', 'solarized-dark', 'preview_theme', '', 'default', 'N', 'Y', '0', 'admin', '2020-11-11 17:46:44', NULL, NULL, 'solarized-dark');
INSERT INTO `sys_dict_data` VALUES (114, 82, 'solarized-light', 'solarized-light', 'preview_theme', '', 'default', 'N', 'Y', '0', 'admin', '2020-11-11 17:46:44', NULL, NULL, 'solarized-light');
INSERT INTO `sys_dict_data` VALUES (115, 83, 'srcery', 'srcery', 'preview_theme', '', 'default', 'N', 'Y', '0', 'admin', '2020-11-11 17:46:44', NULL, NULL, 'srcery');
INSERT INTO `sys_dict_data` VALUES (116, 84, 'sunburst', 'sunburst', 'preview_theme', '', 'default', 'N', 'Y', '0', 'admin', '2020-11-11 17:46:44', NULL, NULL, 'sunburst');
INSERT INTO `sys_dict_data` VALUES (117, 85, 'tomorrow-night-blue', 'tomorrow-night-blue', 'preview_theme', '', 'default', 'N', 'Y', '0', 'admin', '2020-11-11 17:46:45', NULL, NULL, 'tomorrow-night-blue');
INSERT INTO `sys_dict_data` VALUES (118, 86, 'tomorrow-night-bright', 'tomorrow-night-bright', 'preview_theme', '', 'default', 'N', 'Y', '0', 'admin', '2020-11-11 17:46:45', NULL, NULL, 'tomorrow-night-bright');
INSERT INTO `sys_dict_data` VALUES (119, 87, 'tomorrow-night-eighties', 'tomorrow-night-eighties', 'preview_theme', '', 'default', 'N', 'Y', '0', 'admin', '2020-11-11 17:46:45', NULL, NULL, 'tomorrow-night-eighties');
INSERT INTO `sys_dict_data` VALUES (120, 88, 'tomorrow-night', 'tomorrow-night', 'preview_theme', '', 'default', 'N', 'Y', '0', 'admin', '2020-11-11 17:46:45', NULL, NULL, 'tomorrow-night');
INSERT INTO `sys_dict_data` VALUES (121, 89, 'tomorrow', 'tomorrow', 'preview_theme', '', 'default', 'N', 'Y', '0', 'admin', '2020-11-11 17:46:45', NULL, NULL, 'tomorrow');
INSERT INTO `sys_dict_data` VALUES (122, 90, 'vs', 'vs', 'preview_theme', '', 'default', 'N', 'Y', '0', 'admin', '2020-11-11 17:46:45', NULL, NULL, 'vs');
INSERT INTO `sys_dict_data` VALUES (123, 91, 'vs2015', 'vs2015', 'preview_theme', '', 'default', 'N', 'Y', '0', 'admin', '2020-11-11 17:46:45', NULL, NULL, 'vs2015');
INSERT INTO `sys_dict_data` VALUES (124, 92, 'xcode', 'xcode', 'preview_theme', '', 'default', 'N', 'Y', '0', 'admin', '2020-11-11 17:46:45', NULL, NULL, 'xcode');
INSERT INTO `sys_dict_data` VALUES (125, 93, 'xt256', 'xt256', 'preview_theme', '', 'default', 'N', 'Y', '0', 'admin', '2020-11-11 17:46:45', NULL, NULL, 'xt256');
INSERT INTO `sys_dict_data` VALUES (126, 94, 'zenburn', 'zenburn', 'preview_theme', '', 'default', 'N', 'Y', '0', 'admin', '2020-11-11 17:46:45', NULL, NULL, 'zenburn');
INSERT INTO `sys_dict_data` VALUES (127, 2, 'mybatis-plus', 'mybatis-plus', 'template_type', '', 'default', 'Y', 'Y', '0', 'admin', '2020-11-12 21:40:31', NULL, NULL, '标准 mybatis-plus crud 模版');
INSERT INTO `sys_dict_data` VALUES (128, 3, 'ruoyi', 'ruoyi', 'template_type', '', 'default', 'N', 'Y', '0', 'admin', '2020-11-15 00:48:35', 'admin', '2020-11-15 00:48:47', 'ruoyi');
INSERT INTO `sys_dict_data` VALUES (129, 1, '系统配置', '1', 'config_type', '', 'default', 'Y', 'Y', '0', 'admin', '2020-11-15 09:13:26', NULL, NULL, '系统配置');
INSERT INTO `sys_dict_data` VALUES (130, 2, '生成配置', '2', 'config_type', '', 'default', 'N', 'Y', '0', 'admin', '2020-11-15 09:13:43', NULL, NULL, '生成配置');
INSERT INTO `sys_dict_data` VALUES (131, 3, '下拉框', '2', 'control_type', '', 'default', 'N', 'Y', '0', 'admin', '2020-11-19 09:29:50', 'admin', '2020-11-25 12:07:56', '系统下拉框控件');
INSERT INTO `sys_dict_data` VALUES (132, 2, '单选框', '1', 'control_type', '', 'default', 'N', 'Y', '0', 'admin', '2020-11-19 09:30:42', 'admin', '2020-11-25 12:07:20', '系统单选框组控件');
INSERT INTO `sys_dict_data` VALUES (133, 1, '文本框', '0', 'control_type', '', 'default', 'Y', 'Y', '0', 'admin', '2020-11-19 09:31:30', 'admin', '2020-11-25 12:07:08', '系统默认输入框组件');

-- ----------------------------
-- Table structure for sys_dict_type
-- ----------------------------
DROP TABLE IF EXISTS `sys_dict_type`;
CREATE TABLE `sys_dict_type`  (
                                  `dict_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '字典主键',
                                  `dict_name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '字典名称',
                                  `dict_type` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '字典类型',
                                  `status` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '状态（0正常 1停用）',
                                  `create_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '创建人',
                                  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
                                  `update_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '修改人',
                                  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '修改时间',
                                  `remark` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '备注信息',
                                  PRIMARY KEY (`dict_id`) USING BTREE,
                                  UNIQUE INDEX `dict_type`(`dict_type`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 17 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '字典类型' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of sys_dict_type
-- ----------------------------
INSERT INTO `sys_dict_type` VALUES (1, '用户性别', 'sys_user_sex', '0', 'admin', '2020-11-10 22:04:40', 'admin', '2020-11-10 22:04:40', '用户性别');
INSERT INTO `sys_dict_type` VALUES (2, '菜单状态', 'sys_show_hide', '0', 'admin', '2020-11-10 22:04:46', 'admin', '2020-11-10 22:04:46', '菜单状态');
INSERT INTO `sys_dict_type` VALUES (3, '系统开关', 'sys_normal_disable', '0', 'admin', '2020-11-10 22:04:46', 'admin', '2020-11-10 22:04:46', '系统开关');
INSERT INTO `sys_dict_type` VALUES (4, '任务状态', 'sys_job_status', '0', 'admin', '2020-11-10 22:04:46', 'admin', '2020-11-10 22:04:46', '任务状态');
INSERT INTO `sys_dict_type` VALUES (5, '任务分组', 'sys_job_group', '0', 'admin', '2020-11-10 22:04:46', 'admin', '2020-11-10 22:04:46', '任务分组');
INSERT INTO `sys_dict_type` VALUES (6, '系统是否', 'sys_yes_no', '0', 'admin', '2020-11-10 22:04:46', 'admin', '2020-11-10 22:04:46', '系统是否');
INSERT INTO `sys_dict_type` VALUES (7, '模版类型', 'template_type', '0', 'admin', '2020-11-10 22:04:46', 'admin', '2020-11-12 21:38:09', '代码生成模版类型');
INSERT INTO `sys_dict_type` VALUES (8, '通知状态', 'sys_notice_status', '0', 'admin', '2020-11-10 22:04:46', 'admin', '2020-11-10 22:04:46', '通知状态');
INSERT INTO `sys_dict_type` VALUES (9, '操作类型', 'sys_oper_type', '0', 'admin', '2020-11-10 22:04:46', 'admin', '2020-11-10 22:04:46', '操作类型');
INSERT INTO `sys_dict_type` VALUES (10, '系统状态', 'sys_common_status', '0', 'admin', '2020-11-10 22:04:46', 'admin', '2020-11-10 22:04:46', '系统状态');
INSERT INTO `sys_dict_type` VALUES (11, '数据库类型', 'sys_db_type', '0', 'admin', '2020-11-10 22:04:46', 'admin', '2020-11-10 22:04:46', '数据库类型');
INSERT INTO `sys_dict_type` VALUES (12, 'Oracle连接方式', 'sys_oracle_mode', '0', 'admin', '2020-11-10 22:04:46', 'admin', '2020-11-10 22:04:46', 'Oracle连接方式');
INSERT INTO `sys_dict_type` VALUES (13, '预览主题', 'preview_theme', '0', 'admin', '2020-11-11 13:43:36', 'admin', '2020-11-11 13:43:45', '代码生成预览主题');
INSERT INTO `sys_dict_type` VALUES (14, '回显样式', 'dict_list_class', '0', 'admin', '2020-11-11 17:14:41', NULL, NULL, '字典数据表格回显样式');
INSERT INTO `sys_dict_type` VALUES (15, '配置类型', 'config_type', '0', 'admin', '2020-11-13 14:52:07', 'admin', '2020-11-15 09:15:01', '配置类型');
INSERT INTO `sys_dict_type` VALUES (16, '控件类型', 'control_type', '0', 'admin', '2020-11-19 09:29:12', NULL, NULL, '管理控件的类型（select,radio,input）');

SET FOREIGN_KEY_CHECKS = 1;
