/*
 Navicat Premium Data Transfer
 ORACLE
*/


-- ----------------------------
-- Table structure for gen_table
-- ----------------------------
CREATE TABLE "GEN_TABLE" (
  "TABLE_ID" NUMBER(11) NOT NULL ,
  "DATA_SOURCE_ID" NUMBER(11) ,
  "TABLE_NAME" NVARCHAR2(200) NOT NULL ,
  "TABLE_COMMENT" NVARCHAR2(500) ,
  "SUB_TABLE_NAME" NVARCHAR2(64) ,
  "SUB_TABLE_FK_NAME" NVARCHAR2(64) ,
  "CLASS_NAME" NVARCHAR2(100) NOT NULL ,
  "TPL_CATEGORY" NVARCHAR2(20) NOT NULL ,
  "PACKAGE_NAME" NVARCHAR2(100) ,
  "MODULE_NAME" NVARCHAR2(30) ,
  "BUSINESS_NAME" NVARCHAR2(30) ,
  "FUNCTION_NAME" NVARCHAR2(50) ,
  "FUNCTION_AUTHOR" NVARCHAR2(50) ,
  "GEN_TYPE" NCHAR(1) NOT NULL ,
  "GEN_PATH" NVARCHAR2(200) ,
  "OPTIONS" NVARCHAR2(1000) ,
  "CREATE_BY" NVARCHAR2(64) ,
  "CREATE_TIME" DATE ,
  "UPDATE_BY" NVARCHAR2(64) ,
  "UPDATE_TIME" DATE ,
  "REMARK" NVARCHAR2(500)
)
;
COMMENT ON COLUMN "GEN_TABLE"."TABLE_ID" IS '业务主键';
COMMENT ON COLUMN "GEN_TABLE"."DATA_SOURCE_ID" IS '数据源主键';
COMMENT ON COLUMN "GEN_TABLE"."TABLE_NAME" IS '表名称';
COMMENT ON COLUMN "GEN_TABLE"."TABLE_COMMENT" IS '表描述';
COMMENT ON COLUMN "GEN_TABLE"."SUB_TABLE_NAME" IS '关联父表的表名';
COMMENT ON COLUMN "GEN_TABLE"."SUB_TABLE_FK_NAME" IS '本表关联父表的外键名';
COMMENT ON COLUMN "GEN_TABLE"."CLASS_NAME" IS '实体类名称(首字母大写)';
COMMENT ON COLUMN "GEN_TABLE"."TPL_CATEGORY" IS '若依操作模板（crud单表操作 tree树表操作 sub主子表操作）';
COMMENT ON COLUMN "GEN_TABLE"."PACKAGE_NAME" IS '生成包路径';
COMMENT ON COLUMN "GEN_TABLE"."MODULE_NAME" IS '生成模块名';
COMMENT ON COLUMN "GEN_TABLE"."BUSINESS_NAME" IS '生成业务名';
COMMENT ON COLUMN "GEN_TABLE"."FUNCTION_NAME" IS '生成功能名';
COMMENT ON COLUMN "GEN_TABLE"."FUNCTION_AUTHOR" IS '生成作者';
COMMENT ON COLUMN "GEN_TABLE"."GEN_TYPE" IS '生成代码方式（0 zip压缩包 1 自定义路径）';
COMMENT ON COLUMN "GEN_TABLE"."GEN_PATH" IS '生成路径（不填默认项目路径）';
COMMENT ON COLUMN "GEN_TABLE"."OPTIONS" IS '其它生成选项';
COMMENT ON COLUMN "GEN_TABLE"."CREATE_BY" IS '创建人';
COMMENT ON COLUMN "GEN_TABLE"."CREATE_TIME" IS '创建时间';
COMMENT ON COLUMN "GEN_TABLE"."UPDATE_BY" IS '修改人';
COMMENT ON COLUMN "GEN_TABLE"."UPDATE_TIME" IS '修改时间';
COMMENT ON COLUMN "GEN_TABLE"."REMARK" IS '备注信息';
COMMENT ON TABLE "GEN_TABLE" IS '代码生成表业务信息';

-- ----------------------------
-- Records of "gen_table"
-- ----------------------------
INSERT INTO "GEN_TABLE" VALUES ('75', '0', 'gen_table', '代码生成表业务信息', NULL, NULL, 'GenTable', 'crud', 'com.wangming.generator', 'generator', 'table', '代码生成业务信息', 'wangming', '0', '/', '{}', 'admin', TO_DATE('2020-11-10 23:05:15', 'SYYYY-MM-DD HH24:MI:SS'), NULL, NULL, NULL);
COMMIT;

-- ----------------------------
-- Table structure for GEN_TABLE_COLUMN
-- ----------------------------
CREATE TABLE "GEN_TABLE_COLUMN" (
  "COLUMN_ID" NUMBER(11) NOT NULL ,
  "TABLE_ID" NVARCHAR2(64) ,
  "COLUMN_NAME" NVARCHAR2(200) ,
  "COLUMN_COMMENT" NVARCHAR2(500) ,
  "COLUMN_TYPE" NVARCHAR2(100) ,
  "JAVA_TYPE" NVARCHAR2(500) ,
  "JAVA_FIELD" NVARCHAR2(200) ,
  "IS_PK" NCHAR(1) ,
  "IS_INCREMENT" NCHAR(1) ,
  "IS_REQUIRED" NCHAR(1) ,
  "IS_INSERT" NCHAR(1) ,
  "IS_EDIT" NCHAR(1) ,
  "IS_LIST" NCHAR(1) ,
  "IS_QUERY" NCHAR(1) ,
  "QUERY_TYPE" NVARCHAR2(200) ,
  "HTML_TYPE" NVARCHAR2(200) ,
  "DICT_TYPE" NVARCHAR2(200) ,
  "SORT" NUMBER(11) ,
  "CREATE_BY" NVARCHAR2(64) ,
  "CREATE_TIME" DATE ,
  "UPDATE_BY" NVARCHAR2(64) ,
  "UPDATE_TIME" DATE ,
  "REMARK" NVARCHAR2(200)
)
;
COMMENT ON COLUMN "GEN_TABLE_COLUMN"."COLUMN_ID" IS '字段主键';
COMMENT ON COLUMN "GEN_TABLE_COLUMN"."TABLE_ID" IS '归属表编号';
COMMENT ON COLUMN "GEN_TABLE_COLUMN"."COLUMN_NAME" IS '列名称';
COMMENT ON COLUMN "GEN_TABLE_COLUMN"."COLUMN_COMMENT" IS '列描述';
COMMENT ON COLUMN "GEN_TABLE_COLUMN"."COLUMN_TYPE" IS '列类型';
COMMENT ON COLUMN "GEN_TABLE_COLUMN"."JAVA_TYPE" IS 'JAVA类型';
COMMENT ON COLUMN "GEN_TABLE_COLUMN"."JAVA_FIELD" IS 'JAVA字段名';
COMMENT ON COLUMN "GEN_TABLE_COLUMN"."IS_PK" IS '是否主键（1是）';
COMMENT ON COLUMN "GEN_TABLE_COLUMN"."IS_INCREMENT" IS '是否自增（1是）';
COMMENT ON COLUMN "GEN_TABLE_COLUMN"."IS_REQUIRED" IS '是否必填（1是）';
COMMENT ON COLUMN "GEN_TABLE_COLUMN"."IS_INSERT" IS '是否为插入字段（1是）';
COMMENT ON COLUMN "GEN_TABLE_COLUMN"."IS_EDIT" IS '是否编辑字段（1是）';
COMMENT ON COLUMN "GEN_TABLE_COLUMN"."IS_LIST" IS '是否列表字段（1是）';
COMMENT ON COLUMN "GEN_TABLE_COLUMN"."IS_QUERY" IS '是否查询字段（1是）';
COMMENT ON COLUMN "GEN_TABLE_COLUMN"."QUERY_TYPE" IS '查询方式（EQ等于、NE不等于、GT大于、LT小于、LIKE模糊、BETWEEN范围）';
COMMENT ON COLUMN "GEN_TABLE_COLUMN"."HTML_TYPE" IS '显示类型（input文本框、textarea文本域、select下拉框、checkbox复选框、radio单选框、datetime日期控件）';
COMMENT ON COLUMN "GEN_TABLE_COLUMN"."DICT_TYPE" IS '字典类型';
COMMENT ON COLUMN "GEN_TABLE_COLUMN"."SORT" IS '排序';
COMMENT ON COLUMN "GEN_TABLE_COLUMN"."CREATE_BY" IS '创建人';
COMMENT ON COLUMN "GEN_TABLE_COLUMN"."CREATE_TIME" IS '创建时间';
COMMENT ON COLUMN "GEN_TABLE_COLUMN"."UPDATE_BY" IS '修改人';
COMMENT ON COLUMN "GEN_TABLE_COLUMN"."UPDATE_TIME" IS '修改时间';
COMMENT ON COLUMN "GEN_TABLE_COLUMN"."REMARK" IS '备注信息';
COMMENT ON TABLE "GEN_TABLE_COLUMN" IS '代码生成业务字段信息';

-- ----------------------------
-- Records of "GEN_TABLE_COLUMN"
-- ----------------------------
INSERT INTO "GEN_TABLE_COLUMN" VALUES ('1115', '0', 'table_id', '业务主键', 'int(11)', 'Long', 'tableId', '1', '1', NULL, '1', NULL, NULL, NULL, NULL, 'input', '', '1', 'admin', TO_DATE('2020-11-14 13:50:11', 'SYYYY-MM-DD HH24:MI:SS'), NULL, NULL, NULL);
INSERT INTO "GEN_TABLE_COLUMN" VALUES ('1116', '0', 'data_source_id', '数据源主键', 'int(11)', 'Long', 'dataSourceId', '0', '0', NULL, '1', '1', '1', '1', NULL, 'input', '', '2', 'admin', TO_DATE('2020-11-14 13:50:11', 'SYYYY-MM-DD HH24:MI:SS'), NULL, NULL, NULL);
INSERT INTO "GEN_TABLE_COLUMN" VALUES ('1117', '0', 'table_name', '表名称', 'varchar(200)', 'String', 'tableName', '0', '0', '1', '1', '1', '1', '1', 'LIKE', 'input', '', '3', 'admin', TO_DATE('2020-11-14 13:50:11', 'SYYYY-MM-DD HH24:MI:SS'), NULL, NULL, NULL);
INSERT INTO "GEN_TABLE_COLUMN" VALUES ('1118', '0', 'table_comment', '表描述', 'varchar(500)', 'String', 'tableComment', '0', '0', NULL, '1', '1', '1', '1', NULL, 'textarea', '', '4', 'admin', TO_DATE('2020-11-14 13:50:11', 'SYYYY-MM-DD HH24:MI:SS'), NULL, NULL, NULL);
INSERT INTO "GEN_TABLE_COLUMN" VALUES ('1119', '0', 'sub_table_name', '关联父表的表名', 'varchar(64)', 'String', 'subTableName', '0', '0', NULL, '1', '1', '1', '1', 'LIKE', 'input', '', '5', 'admin', TO_DATE('2020-11-14 13:50:11', 'SYYYY-MM-DD HH24:MI:SS'), NULL, NULL, NULL);
INSERT INTO "GEN_TABLE_COLUMN" VALUES ('1120', '0', 'sub_table_fk_name', '本表关联父表的外键名', 'varchar(64)', 'String', 'subTableFkName', '0', '0', NULL, '1', '1', '1', '1', 'LIKE', 'input', '', '6', 'admin', TO_DATE('2020-11-14 13:50:11', 'SYYYY-MM-DD HH24:MI:SS'), NULL, NULL, NULL);
INSERT INTO "GEN_TABLE_COLUMN" VALUES ('1121', '0', 'class_name', '实体类名称(首字母大写)', 'varchar(100)', 'String', 'className', '0', '0', '1', '1', '1', '1', '1', 'LIKE', 'input', '', '7', 'admin', TO_DATE('2020-11-14 13:50:11', 'SYYYY-MM-DD HH24:MI:SS'), NULL, NULL, NULL);
INSERT INTO "GEN_TABLE_COLUMN" VALUES ('1122', '0', 'tpl_category', '若依操作模板（crud单表操作 tree树表操作 sub主子表操作）', 'varchar(20)', 'String', 'tplCategory', '0', '0', '1', '1', '1', '1', '1', NULL, 'input', '', '8', 'admin', TO_DATE('2020-11-14 13:50:11', 'SYYYY-MM-DD HH24:MI:SS'), NULL, NULL, NULL);
INSERT INTO "GEN_TABLE_COLUMN" VALUES ('1123', '0', 'package_name', '生成包路径', 'varchar(100)', 'String', 'packageName', '0', '0', NULL, '1', '1', '1', '1', 'LIKE', 'input', '', '9', 'admin', TO_DATE('2020-11-14 13:50:11', 'SYYYY-MM-DD HH24:MI:SS'), NULL, NULL, NULL);
INSERT INTO "GEN_TABLE_COLUMN" VALUES ('1124', '0', 'module_name', '生成模块名', 'varchar(30)', 'String', 'moduleName', '0', '0', NULL, '1', '1', '1', '1', 'LIKE', 'input', '', '10', 'admin', TO_DATE('2020-11-14 13:50:11', 'SYYYY-MM-DD HH24:MI:SS'), NULL, NULL, NULL);
INSERT INTO "GEN_TABLE_COLUMN" VALUES ('1125', '0', 'business_name', '生成业务名', 'varchar(30)', 'String', 'businessName', '0', '0', NULL, '1', '1', '1', '1', 'LIKE', 'input', '', '11', 'admin', TO_DATE('2020-11-14 13:50:11', 'SYYYY-MM-DD HH24:MI:SS'), NULL, NULL, NULL);
INSERT INTO "GEN_TABLE_COLUMN" VALUES ('1126', '0', 'function_name', '生成功能名', 'varchar(50)', 'String', 'functionName', '0', '0', NULL, '1', '1', '1', '1', 'LIKE', 'input', '', '12', 'admin', TO_DATE('2020-11-14 13:50:11', 'SYYYY-MM-DD HH24:MI:SS'), NULL, NULL, NULL);
INSERT INTO "GEN_TABLE_COLUMN" VALUES ('1127', '0', 'function_author', '生成作者', 'varchar(50)', 'String', 'functionAuthor', '0', '0', NULL, '1', '1', '1', '1', NULL, 'input', '', '13', 'admin', TO_DATE('2020-11-14 13:50:11', 'SYYYY-MM-DD HH24:MI:SS'), NULL, NULL, NULL);
INSERT INTO "GEN_TABLE_COLUMN" VALUES ('1128', '0', 'gen_type', '生成代码方式（0 zip压缩包 1 自定义路径）', 'char(1)', 'String', 'genType', '0', '0', '1', '1', '1', '1', '1', NULL, 'select', '', '14', 'admin', TO_DATE('2020-11-14 13:50:11', 'SYYYY-MM-DD HH24:MI:SS'), NULL, NULL, NULL);
INSERT INTO "GEN_TABLE_COLUMN" VALUES ('1129', '0', 'gen_path', '生成路径（不填默认项目路径）', 'varchar(200)', 'String', 'genPath', '0', '0', NULL, '1', '1', '1', '1', NULL, 'input', '', '15', 'admin', TO_DATE('2020-11-14 13:50:11', 'SYYYY-MM-DD HH24:MI:SS'), NULL, NULL, NULL);
INSERT INTO "GEN_TABLE_COLUMN" VALUES ('1130', '0', 'options', '其它生成选项', 'varchar(1000)', 'String', 'options', '0', '0', NULL, '1', '1', '1', '1', NULL, 'textarea', '', '16', 'admin', TO_DATE('2020-11-14 13:50:11', 'SYYYY-MM-DD HH24:MI:SS'), NULL, NULL, NULL);
INSERT INTO "GEN_TABLE_COLUMN" VALUES ('1131', '0', 'create_by', '创建人', 'varchar(64)', 'String', 'createBy', '0', '0', NULL, '1', NULL, NULL, NULL, NULL, 'input', '', '17', 'admin', TO_DATE('2020-11-14 13:50:11', 'SYYYY-MM-DD HH24:MI:SS'), NULL, NULL, NULL);
INSERT INTO "GEN_TABLE_COLUMN" VALUES ('1132', '0', 'create_time', '创建时间', 'datetime', 'Date', 'createTime', '0', '0', NULL, '1', NULL, NULL, NULL, NULL, 'datetime', '', '18', 'admin', TO_DATE('2020-11-14 13:50:11', 'SYYYY-MM-DD HH24:MI:SS'), NULL, NULL, NULL);
INSERT INTO "GEN_TABLE_COLUMN" VALUES ('1133', '0', 'update_by', '修改人', 'varchar(64)', 'String', 'updateBy', '0', '0', NULL, '1', '1', NULL, NULL, NULL, 'input', '', '19', 'admin', TO_DATE('2020-11-14 13:50:11', 'SYYYY-MM-DD HH24:MI:SS'), NULL, NULL, NULL);
INSERT INTO "GEN_TABLE_COLUMN" VALUES ('1134', '0', 'update_time', '修改时间', 'datetime', 'Date', 'updateTime', '0', '0', NULL, '1', '1', NULL, NULL, NULL, 'datetime', '', '20', 'admin', TO_DATE('2020-11-14 13:50:11', 'SYYYY-MM-DD HH24:MI:SS'), NULL, NULL, NULL);
INSERT INTO "GEN_TABLE_COLUMN" VALUES ('1135', '0', 'remark', '备注信息', 'varchar(500)', 'String', 'remark', '0', '0', NULL, '1', '1', '1', NULL, NULL, 'textarea', '', '21', 'admin', TO_DATE('2020-11-14 13:50:11', 'SYYYY-MM-DD HH24:MI:SS'), NULL, NULL, NULL);
COMMIT;

-- ----------------------------
-- Table structure for SYS_CONFIG
-- ----------------------------
CREATE TABLE "SYS_CONFIG" (
  "CONFIG_ID" NUMBER(11) NOT NULL ,
  "CONFIG_NAME" NVARCHAR2(100) ,
  "CONFIG_KEY" NVARCHAR2(100) NOT NULL ,
  "CONFIG_VALUE" NVARCHAR2(500) NOT NULL ,
  "IS_BUILT_IN" NCHAR(1) NOT NULL ,
  "DICT_TYPE" NVARCHAR2(100) ,
  "INPUT_TYPE" NVARCHAR2(100) NOT NULL ,
  "CREATE_BY" NVARCHAR2(64) ,
  "CREATE_TIME" DATE ,
  "UPDATE_BY" NVARCHAR2(64) ,
  "UPDATE_TIME" DATE ,
  "REMARK" NVARCHAR2(500) 
)
;
COMMENT ON COLUMN "SYS_CONFIG"."CONFIG_ID" IS '参数主键';
COMMENT ON COLUMN "SYS_CONFIG"."CONFIG_NAME" IS '参数名称';
COMMENT ON COLUMN "SYS_CONFIG"."CONFIG_KEY" IS '参数键名';
COMMENT ON COLUMN "SYS_CONFIG"."CONFIG_VALUE" IS '参数键值';
COMMENT ON COLUMN "SYS_CONFIG"."IS_BUILT_IN" IS '系统内置（Y是 N否）';
COMMENT ON COLUMN "SYS_CONFIG"."DICT_TYPE" IS '关联的字典类型';
COMMENT ON COLUMN "SYS_CONFIG"."INPUT_TYPE" IS '输入类型（0-text 1-radio 2-select）';
COMMENT ON COLUMN "SYS_CONFIG"."CREATE_BY" IS '创建者';
COMMENT ON COLUMN "SYS_CONFIG"."CREATE_TIME" IS '创建时间';
COMMENT ON COLUMN "SYS_CONFIG"."UPDATE_BY" IS '更新者';
COMMENT ON COLUMN "SYS_CONFIG"."UPDATE_TIME" IS '更新时间';
COMMENT ON COLUMN "SYS_CONFIG"."REMARK" IS '备注';
COMMENT ON TABLE "SYS_CONFIG" IS '参数配置表';

-- ----------------------------
-- Records of "SYS_CONFIG"
-- ----------------------------
INSERT INTO "SYS_CONFIG" VALUES ('100', '代码生成默认作者', 'gen_author', 'wangming', 'Y', NULL, '0', 'admin', TO_DATE('2020-11-14 11:24:57', 'SYYYY-MM-DD HH24:MI:SS'), 'admin', TO_DATE('2020-11-25 11:55:13', 'SYYYY-MM-DD HH24:MI:SS'), '代码生成默认作者');
INSERT INTO "SYS_CONFIG" VALUES ('101', '默认生成包路径', 'gen_package_name', 'com.wangming.generator', 'Y', NULL, '0', 'admin', TO_DATE('2020-11-14 11:32:13', 'SYYYY-MM-DD HH24:MI:SS'), '', NULL, '代码生成默认生成包路径');
INSERT INTO "SYS_CONFIG" VALUES ('102', '代码生成模版', 'gen_template', 'mybatis-plus', 'Y', 'template_type', '1', 'admin', TO_DATE('2020-11-14 11:33:15', 'SYYYY-MM-DD HH24:MI:SS'), 'admin', TO_DATE('2020-11-25 12:03:40', 'SYYYY-MM-DD HH24:MI:SS'), '代码生成模版（mybatis、 mybatis-plus、ruoyi）');
INSERT INTO "SYS_CONFIG" VALUES ('103', '自动去除表前缀', 'gen_auto_remove_pre', 'Y', 'Y', 'sys_yes_no', '1', 'admin', TO_DATE('2020-11-14 11:34:16', 'SYYYY-MM-DD HH24:MI:SS'), '', NULL, '代码生成自动去除表前缀（ Y 是 N 否 ）');
INSERT INTO "SYS_CONFIG" VALUES ('104', '代码生成表前缀', 'gen_table_prefix', 'c_ckc_', 'Y', NULL, '0', 'admin', TO_DATE('2020-11-14 11:35:16', 'SYYYY-MM-DD HH24:MI:SS'), 'admin', TO_DATE('2020-11-15 13:56:43', 'SYYYY-MM-DD HH24:MI:SS'), '代码生成表前缀');
INSERT INTO "SYS_CONFIG" VALUES ('105', '代码预览主题', 'gen_preview_theme', 'googlecode', 'Y', 'preview_theme', '2', 'admin', TO_DATE('2020-11-14 11:36:00', 'SYYYY-MM-DD HH24:MI:SS'), 'admin', TO_DATE('2020-11-25 13:06:36', 'SYYYY-MM-DD HH24:MI:SS'), '默认代码预览主题');
INSERT INTO "SYS_CONFIG" VALUES ('106', '是否生成swagger注解', 'gen_swagger_enable', 'Y', 'Y', 'sys_yes_no', '1', 'admin', TO_DATE('2020-11-14 11:38:11', 'SYYYY-MM-DD HH24:MI:SS'), 'admin', TO_DATE('2020-11-15 01:59:32', 'SYYYY-MM-DD HH24:MI:SS'), '代码生成是否生成swagger注解（Y 是， N 否）');
INSERT INTO "SYS_CONFIG" VALUES ('107', '是否生成excel注解', 'gen_excel_enable', 'N', 'Y', 'sys_yes_no', '1', 'admin', TO_DATE('2020-11-18 10:19:12', 'SYYYY-MM-DD HH24:MI:SS'), 'admin', TO_DATE('2020-11-25 13:06:36', 'SYYYY-MM-DD HH24:MI:SS'), '代码生成是否生成excel注解（Y 是， N 否）');
COMMIT;

-- ----------------------------
-- Table structure for SYS_DATA_SOURCE
-- ----------------------------
CREATE TABLE "SYS_DATA_SOURCE" (
  "ID" NUMBER(11) NOT NULL ,
  "NAME" NVARCHAR2(30) ,
  "DB_TYPE" NVARCHAR2(30) ,
  "ORACLE_CONN_MODE" NVARCHAR2(20) ,
  "SERVICE_NAME_OR_SID" NVARCHAR2(100) ,
  "HOST" NVARCHAR2(20) ,
  "PORT" NUMBER(11) ,
  "USERNAME" NVARCHAR2(50) ,
  "PASSWORD" NVARCHAR2(100) ,
  "STATUS" NCHAR(1) ,
  "CREATE_BY" NVARCHAR2(64) ,
  "CREATE_TIME" DATE ,
  "UPDATE_BY" NVARCHAR2(64) ,
  "UPDATE_TIME" DATE ,
  "REMARK" NVARCHAR2(500) 
)
;
COMMENT ON COLUMN "SYS_DATA_SOURCE"."ID" IS '数据源主键';
COMMENT ON COLUMN "SYS_DATA_SOURCE"."NAME" IS '数据库名称';
COMMENT ON COLUMN "SYS_DATA_SOURCE"."DB_TYPE" IS '数据库名称';
COMMENT ON COLUMN "SYS_DATA_SOURCE"."ORACLE_CONN_MODE" IS 'oracle连接方式';
COMMENT ON COLUMN "SYS_DATA_SOURCE"."SERVICE_NAME_OR_SID" IS 'oracle连接服务名或SID';
COMMENT ON COLUMN "SYS_DATA_SOURCE"."HOST" IS '主机地址';
COMMENT ON COLUMN "SYS_DATA_SOURCE"."PORT" IS '端口号';
COMMENT ON COLUMN "SYS_DATA_SOURCE"."USERNAME" IS '连接用户名';
COMMENT ON COLUMN "SYS_DATA_SOURCE"."PASSWORD" IS '连接密码';
COMMENT ON COLUMN "SYS_DATA_SOURCE"."STATUS" IS '状态（0正常 1停用）';
COMMENT ON COLUMN "SYS_DATA_SOURCE"."CREATE_BY" IS '创建人';
COMMENT ON COLUMN "SYS_DATA_SOURCE"."CREATE_TIME" IS '创建时间';
COMMENT ON COLUMN "SYS_DATA_SOURCE"."UPDATE_BY" IS '修改人';
COMMENT ON COLUMN "SYS_DATA_SOURCE"."UPDATE_TIME" IS '修改时间';
COMMENT ON COLUMN "SYS_DATA_SOURCE"."REMARK" IS '备注信息';
COMMENT ON TABLE "SYS_DATA_SOURCE" IS '表数据源配置';

-- ----------------------------
-- Records of "SYS_DATA_SOURCE"
-- ----------------------------
INSERT INTO "SYS_DATA_SOURCE" VALUES ('18', 'code_generator', 'mysql', 'service_name', '', '114.55.137.209', '3306', 'generator', 'generator', '0', 'admin', TO_DATE('2020-11-10 22:41:17', 'SYYYY-MM-DD HH24:MI:SS'), 'admin', TO_DATE('2020-11-10 22:41:17', 'SYYYY-MM-DD HH24:MI:SS'), '演示数据源');
COMMIT;

-- ----------------------------
-- Table structure for SYS_DICT_DATA
-- ----------------------------
CREATE TABLE "SYS_DICT_DATA" (
  "DICT_CODE" NUMBER(11) NOT NULL ,
  "DICT_SORT" NUMBER(11) ,
  "DICT_LABEL" NVARCHAR2(100) ,
  "DICT_VALUE" NVARCHAR2(100) ,
  "DICT_TYPE" NVARCHAR2(100) NOT NULL ,
  "CSS_CLASS" NVARCHAR2(100) ,
  "LIST_CLASS" NVARCHAR2(100) ,
  "IS_DEFAULT" NCHAR(1) NOT NULL ,
  "IS_BUILT_IN" NCHAR(1) NOT NULL ,
  "STATUS" NCHAR(1) ,
  "CREATE_BY" NVARCHAR2(64) ,
  "CREATE_TIME" DATE ,
  "UPDATE_BY" NVARCHAR2(64) ,
  "UPDATE_TIME" DATE ,
  "REMARK" NVARCHAR2(500) 
)
;
COMMENT ON COLUMN "SYS_DICT_DATA"."DICT_CODE" IS '字典编码';
COMMENT ON COLUMN "SYS_DICT_DATA"."DICT_SORT" IS '字典排序';
COMMENT ON COLUMN "SYS_DICT_DATA"."DICT_LABEL" IS '字典标签';
COMMENT ON COLUMN "SYS_DICT_DATA"."DICT_VALUE" IS '字典键值';
COMMENT ON COLUMN "SYS_DICT_DATA"."DICT_TYPE" IS '字典类型';
COMMENT ON COLUMN "SYS_DICT_DATA"."CSS_CLASS" IS '样式属性（其他样式扩展）';
COMMENT ON COLUMN "SYS_DICT_DATA"."LIST_CLASS" IS '表格字典样式';
COMMENT ON COLUMN "SYS_DICT_DATA"."IS_DEFAULT" IS '系统默认值（Y是 N否）';
COMMENT ON COLUMN "SYS_DICT_DATA"."IS_BUILT_IN" IS '系统内置（Y是 N否）';
COMMENT ON COLUMN "SYS_DICT_DATA"."STATUS" IS '状态（0正常 1停用）';
COMMENT ON COLUMN "SYS_DICT_DATA"."CREATE_BY" IS '创建人';
COMMENT ON COLUMN "SYS_DICT_DATA"."CREATE_TIME" IS '创建时间';
COMMENT ON COLUMN "SYS_DICT_DATA"."UPDATE_BY" IS '修改人';
COMMENT ON COLUMN "SYS_DICT_DATA"."UPDATE_TIME" IS '修改时间';
COMMENT ON COLUMN "SYS_DICT_DATA"."REMARK" IS '备注信息';
COMMENT ON TABLE "SYS_DICT_DATA" IS '字典数据';

-- ----------------------------
-- Records of "SYS_DICT_DATA"
-- ----------------------------
INSERT INTO "SYS_DICT_DATA" VALUES ('1', '1', '男', '0', 'sys_user_sex', '', '', 'Y', 'Y', '0', 'admin', TO_DATE('2020-11-10 22:04:46', 'SYYYY-MM-DD HH24:MI:SS'), 'admin', TO_DATE('2020-11-10 22:04:46', 'SYYYY-MM-DD HH24:MI:SS'), '男');
INSERT INTO "SYS_DICT_DATA" VALUES ('2', '2', '女', '1', 'sys_user_sex', '', '', 'N', 'Y', '0', 'admin', TO_DATE('2020-11-10 22:04:46', 'SYYYY-MM-DD HH24:MI:SS'), 'admin', TO_DATE('2020-11-10 22:04:46', 'SYYYY-MM-DD HH24:MI:SS'), '女');
INSERT INTO "SYS_DICT_DATA" VALUES ('3', '3', '未知', '2', 'sys_user_sex', '', '', 'N', 'Y', '0', 'admin', TO_DATE('2020-11-10 22:04:46', 'SYYYY-MM-DD HH24:MI:SS'), 'admin', TO_DATE('2020-11-10 22:04:46', 'SYYYY-MM-DD HH24:MI:SS'), '未知');
INSERT INTO "SYS_DICT_DATA" VALUES ('4', '1', '显示', '0', 'sys_show_hide', '', 'primary', 'Y', 'Y', '0', 'admin', TO_DATE('2020-11-10 22:04:46', 'SYYYY-MM-DD HH24:MI:SS'), 'admin', TO_DATE('2020-11-10 22:04:46', 'SYYYY-MM-DD HH24:MI:SS'), '显示');
INSERT INTO "SYS_DICT_DATA" VALUES ('5', '2', '隐藏', '1', 'sys_show_hide', '', 'danger', 'N', 'Y', '0', 'admin', TO_DATE('2020-11-10 22:04:46', 'SYYYY-MM-DD HH24:MI:SS'), 'admin', TO_DATE('2020-11-10 22:04:46', 'SYYYY-MM-DD HH24:MI:SS'), '隐藏');
INSERT INTO "SYS_DICT_DATA" VALUES ('6', '1', '正常', '0', 'sys_normal_disable', '', 'primary', 'Y', 'Y', '0', 'admin', TO_DATE('2020-11-10 22:04:47', 'SYYYY-MM-DD HH24:MI:SS'), 'admin', TO_DATE('2020-11-10 22:04:47', 'SYYYY-MM-DD HH24:MI:SS'), '正常');
INSERT INTO "SYS_DICT_DATA" VALUES ('7', '2', '停用', '1', 'sys_normal_disable', '', 'danger', 'N', 'Y', '0', 'admin', TO_DATE('2020-11-10 22:04:47', 'SYYYY-MM-DD HH24:MI:SS'), 'admin', TO_DATE('2020-11-10 22:04:47', 'SYYYY-MM-DD HH24:MI:SS'), '停用');
INSERT INTO "SYS_DICT_DATA" VALUES ('8', '1', '正常', '0', 'sys_job_status', '', 'primary', 'Y', 'Y', '0', 'admin', TO_DATE('2020-11-10 22:04:47', 'SYYYY-MM-DD HH24:MI:SS'), 'admin', TO_DATE('2020-11-10 22:04:47', 'SYYYY-MM-DD HH24:MI:SS'), '正常');
INSERT INTO "SYS_DICT_DATA" VALUES ('9', '2', '暂停', '1', 'sys_job_status', '', 'danger', 'N', 'Y', '0', 'admin', TO_DATE('2020-11-10 22:04:47', 'SYYYY-MM-DD HH24:MI:SS'), 'admin', TO_DATE('2020-11-10 22:04:47', 'SYYYY-MM-DD HH24:MI:SS'), '暂停');
INSERT INTO "SYS_DICT_DATA" VALUES ('10', '1', '默认', 'DEFAULT', 'sys_job_group', '', '', 'Y', 'Y', '0', 'admin', TO_DATE('2020-11-10 22:04:47', 'SYYYY-MM-DD HH24:MI:SS'), 'admin', TO_DATE('2020-11-10 22:04:47', 'SYYYY-MM-DD HH24:MI:SS'), '默认');
INSERT INTO "SYS_DICT_DATA" VALUES ('11', '2', '系统', 'SYSTEM', 'sys_job_group', '', '', 'N', 'Y', '0', 'admin', TO_DATE('2020-11-10 22:04:47', 'SYYYY-MM-DD HH24:MI:SS'), 'admin', TO_DATE('2020-11-10 22:04:47', 'SYYYY-MM-DD HH24:MI:SS'), '系统');
INSERT INTO "SYS_DICT_DATA" VALUES ('12', '1', '是', 'Y', 'sys_yes_no', '', 'primary', 'Y', 'Y', '0', 'admin', TO_DATE('2020-11-10 22:04:47', 'SYYYY-MM-DD HH24:MI:SS'), 'admin', TO_DATE('2020-11-10 22:04:47', 'SYYYY-MM-DD HH24:MI:SS'), '是');
INSERT INTO "SYS_DICT_DATA" VALUES ('13', '2', '否', 'N', 'sys_yes_no', '', 'danger', 'N', 'Y', '0', 'admin', TO_DATE('2020-11-10 22:04:47', 'SYYYY-MM-DD HH24:MI:SS'), 'admin', TO_DATE('2020-11-10 22:04:47', 'SYYYY-MM-DD HH24:MI:SS'), '否');
INSERT INTO "SYS_DICT_DATA" VALUES ('14', '1', '成功', '0', 'sys_common_status', '', 'primary', 'N', 'Y', '0', 'admin', TO_DATE('2020-11-10 22:04:47', 'SYYYY-MM-DD HH24:MI:SS'), 'admin', TO_DATE('2020-11-10 22:04:47', 'SYYYY-MM-DD HH24:MI:SS'), '成功');
INSERT INTO "SYS_DICT_DATA" VALUES ('15', '2', '失败', '1', 'sys_common_status', '', 'danger', 'N', 'Y', '0', 'admin', TO_DATE('2020-11-10 22:04:47', 'SYYYY-MM-DD HH24:MI:SS'), 'admin', TO_DATE('2020-11-10 22:04:47', 'SYYYY-MM-DD HH24:MI:SS'), '失败');
INSERT INTO "SYS_DICT_DATA" VALUES ('16', '1', 'MySQL', 'mysql', 'sys_db_type', '', 'default', 'Y', 'Y', '0', 'admin', TO_DATE('2020-11-10 22:04:47', 'SYYYY-MM-DD HH24:MI:SS'), 'admin', TO_DATE('2020-11-10 22:04:47', 'SYYYY-MM-DD HH24:MI:SS'), 'MySQL');
INSERT INTO "SYS_DICT_DATA" VALUES ('17', '2', 'Oracle', 'oracle', 'sys_db_type', '', 'default', 'N', 'Y', '0', 'admin', TO_DATE('2020-11-10 22:04:47', 'SYYYY-MM-DD HH24:MI:SS'), 'admin', TO_DATE('2020-11-10 22:04:47', 'SYYYY-MM-DD HH24:MI:SS'), 'Oracle');
INSERT INTO "SYS_DICT_DATA" VALUES ('18', '1', '服务名', 'service_name', 'sys_oracle_mode', NULL, 'default', 'Y', 'Y', '0', 'admin', TO_DATE('2020-11-10 22:04:47', 'SYYYY-MM-DD HH24:MI:SS'), 'admin', TO_DATE('2020-11-10 22:04:47', 'SYYYY-MM-DD HH24:MI:SS'), '服务名');
INSERT INTO "SYS_DICT_DATA" VALUES ('19', '2', 'SID', 'sid', 'sys_oracle_mode', NULL, 'default', 'N', 'Y', '0', 'admin', TO_DATE('2020-11-10 22:04:47', 'SYYYY-MM-DD HH24:MI:SS'), 'admin', TO_DATE('2020-11-10 22:04:47', 'SYYYY-MM-DD HH24:MI:SS'), 'SID');
INSERT INTO "SYS_DICT_DATA" VALUES ('20', '2', 'SQLite', 'sqlite', 'sys_db_type', '', 'default', 'N', 'Y', '1', 'admin', TO_DATE('2020-11-10 22:04:47', 'SYYYY-MM-DD HH24:MI:SS'), 'admin', TO_DATE('2020-11-10 22:04:47', 'SYYYY-MM-DD HH24:MI:SS'), 'SQLite');
INSERT INTO "SYS_DICT_DATA" VALUES ('21', '4', 'PostgreSQL', 'pgsql', 'sys_db_type', '', 'default', 'N', 'Y', '0', 'admin', TO_DATE('2020-11-10 22:04:47', 'SYYYY-MM-DD HH24:MI:SS'), 'admin', TO_DATE('2020-11-10 22:04:47', 'SYYYY-MM-DD HH24:MI:SS'), 'PostgreSQL');
INSERT INTO "SYS_DICT_DATA" VALUES ('22', '5', 'DB2', 'db2', 'sys_db_type', '', 'default', 'N', 'Y', '1', 'admin', TO_DATE('2020-11-10 22:04:47', 'SYYYY-MM-DD HH24:MI:SS'), 'admin', TO_DATE('2020-11-10 22:04:47', 'SYYYY-MM-DD HH24:MI:SS'), 'DB2');
INSERT INTO "SYS_DICT_DATA" VALUES ('23', '6', 'SQL Server', 'sqlserver', 'sys_db_type', '', 'default', 'N', 'Y', '0', 'admin', TO_DATE('2020-11-10 22:04:47', 'SYYYY-MM-DD HH24:MI:SS'), 'admin', TO_DATE('2020-11-10 22:04:47', 'SYYYY-MM-DD HH24:MI:SS'), 'SQL Server');
INSERT INTO "SYS_DICT_DATA" VALUES ('25', '1', '默认', 'default', 'dict_list_class', '', 'default', 'Y', 'Y', '0', 'admin', TO_DATE('2020-11-11 17:18:52', 'SYYYY-MM-DD HH24:MI:SS'), NULL, NULL, '默认');
INSERT INTO "SYS_DICT_DATA" VALUES ('26', '2', '主要', 'primary', 'dict_list_class', '', 'primary', 'N', 'Y', '0', 'admin', TO_DATE('2020-11-11 17:19:24', 'SYYYY-MM-DD HH24:MI:SS'), NULL, NULL, '主要');
INSERT INTO "SYS_DICT_DATA" VALUES ('27', '3', '成功', 'success', 'dict_list_class', '', 'success', 'N', 'Y', '0', 'admin', TO_DATE('2020-11-11 17:19:57', 'SYYYY-MM-DD HH24:MI:SS'), NULL, NULL, '成功');
INSERT INTO "SYS_DICT_DATA" VALUES ('28', '4', '信息', 'info', 'dict_list_class', '', 'info', 'N', 'Y', '0', 'admin', TO_DATE('2020-11-11 17:20:27', 'SYYYY-MM-DD HH24:MI:SS'), NULL, NULL, '信息');
INSERT INTO "SYS_DICT_DATA" VALUES ('29', '5', '警告', 'warning', 'dict_list_class', '', 'warning', 'N', 'Y', '0', 'admin', TO_DATE('2020-11-11 17:20:57', 'SYYYY-MM-DD HH24:MI:SS'), NULL, NULL, '警告');
INSERT INTO "SYS_DICT_DATA" VALUES ('30', '6', '危险', 'danger', 'dict_list_class', '', 'danger', 'N', 'Y', '0', 'admin', TO_DATE('2020-11-11 17:21:22', 'SYYYY-MM-DD HH24:MI:SS'), NULL, NULL, '危险');
INSERT INTO "SYS_DICT_DATA" VALUES ('31', '1', 'mybatis', 'mybatis', 'template_type', '', 'default', 'N', 'Y', '0', 'admin', TO_DATE('2020-11-11 17:26:26', 'SYYYY-MM-DD HH24:MI:SS'), 'admin', TO_DATE('2020-11-12 21:40:40', 'SYYYY-MM-DD HH24:MI:SS'), '标准 mybatis crud 模版');
INSERT INTO "SYS_DICT_DATA" VALUES ('32', '0', 'a11y-dark', 'a11y-dark', 'preview_theme', '', 'default', 'N', 'Y', '0', 'admin', TO_DATE('2020-11-11 17:46:39', 'SYYYY-MM-DD HH24:MI:SS'), NULL, NULL, 'a11y-dark');
INSERT INTO "SYS_DICT_DATA" VALUES ('33', '1', 'a11y-light', 'a11y-light', 'preview_theme', '', 'default', 'N', 'Y', '0', 'admin', TO_DATE('2020-11-11 17:46:39', 'SYYYY-MM-DD HH24:MI:SS'), NULL, NULL, 'a11y-light');
INSERT INTO "SYS_DICT_DATA" VALUES ('34', '2', 'agate', 'agate', 'preview_theme', '', 'default', 'N', 'Y', '0', 'admin', TO_DATE('2020-11-11 17:46:39', 'SYYYY-MM-DD HH24:MI:SS'), NULL, NULL, 'agate');
INSERT INTO "SYS_DICT_DATA" VALUES ('35', '3', 'an-old-hope', 'an-old-hope', 'preview_theme', '', 'default', 'N', 'Y', '0', 'admin', TO_DATE('2020-11-11 17:46:39', 'SYYYY-MM-DD HH24:MI:SS'), NULL, NULL, 'an-old-hope');
INSERT INTO "SYS_DICT_DATA" VALUES ('36', '4', 'androidstudio', 'androidstudio', 'preview_theme', '', 'default', 'N', 'Y', '0', 'admin', TO_DATE('2020-11-11 17:46:39', 'SYYYY-MM-DD HH24:MI:SS'), NULL, NULL, 'androidstudio');
INSERT INTO "SYS_DICT_DATA" VALUES ('37', '5', 'arduino-light', 'arduino-light', 'preview_theme', '', 'default', 'N', 'Y', '0', 'admin', TO_DATE('2020-11-11 17:46:39', 'SYYYY-MM-DD HH24:MI:SS'), NULL, NULL, 'arduino-light');
INSERT INTO "SYS_DICT_DATA" VALUES ('38', '6', 'arta', 'arta', 'preview_theme', '', 'default', 'N', 'Y', '0', 'admin', TO_DATE('2020-11-11 17:46:39', 'SYYYY-MM-DD HH24:MI:SS'), NULL, NULL, 'arta');
INSERT INTO "SYS_DICT_DATA" VALUES ('39', '7', 'ascetic', 'ascetic', 'preview_theme', '', 'default', 'N', 'Y', '0', 'admin', TO_DATE('2020-11-11 17:46:39', 'SYYYY-MM-DD HH24:MI:SS'), NULL, NULL, 'ascetic');
INSERT INTO "SYS_DICT_DATA" VALUES ('40', '8', 'atelier-cave-dark', 'atelier-cave-dark', 'preview_theme', '', 'default', 'N', 'Y', '0', 'admin', TO_DATE('2020-11-11 17:46:39', 'SYYYY-MM-DD HH24:MI:SS'), NULL, NULL, 'atelier-cave-dark');
INSERT INTO "SYS_DICT_DATA" VALUES ('41', '9', 'atelier-cave-light', 'atelier-cave-light', 'preview_theme', '', 'default', 'N', 'Y', '0', 'admin', TO_DATE('2020-11-11 17:46:39', 'SYYYY-MM-DD HH24:MI:SS'), NULL, NULL, 'atelier-cave-light');
INSERT INTO "SYS_DICT_DATA" VALUES ('42', '10', 'atelier-dune-dark', 'atelier-dune-dark', 'preview_theme', '', 'default', 'N', 'Y', '0', 'admin', TO_DATE('2020-11-11 17:46:39', 'SYYYY-MM-DD HH24:MI:SS'), NULL, NULL, 'atelier-dune-dark');
INSERT INTO "SYS_DICT_DATA" VALUES ('43', '11', 'atelier-dune-light', 'atelier-dune-light', 'preview_theme', '', 'default', 'N', 'Y', '0', 'admin', TO_DATE('2020-11-11 17:46:39', 'SYYYY-MM-DD HH24:MI:SS'), NULL, NULL, 'atelier-dune-light');
INSERT INTO "SYS_DICT_DATA" VALUES ('44', '12', 'atelier-estuary-dark', 'atelier-estuary-dark', 'preview_theme', '', 'default', 'N', 'Y', '0', 'admin', TO_DATE('2020-11-11 17:46:40', 'SYYYY-MM-DD HH24:MI:SS'), NULL, NULL, 'atelier-estuary-dark');
INSERT INTO "SYS_DICT_DATA" VALUES ('45', '13', 'atelier-estuary-light', 'atelier-estuary-light', 'preview_theme', '', 'default', 'N', 'Y', '0', 'admin', TO_DATE('2020-11-11 17:46:40', 'SYYYY-MM-DD HH24:MI:SS'), NULL, NULL, 'atelier-estuary-light');
INSERT INTO "SYS_DICT_DATA" VALUES ('46', '14', 'atelier-forest-dark', 'atelier-forest-dark', 'preview_theme', '', 'default', 'N', 'Y', '0', 'admin', TO_DATE('2020-11-11 17:46:40', 'SYYYY-MM-DD HH24:MI:SS'), NULL, NULL, 'atelier-forest-dark');
INSERT INTO "SYS_DICT_DATA" VALUES ('47', '15', 'atelier-forest-light', 'atelier-forest-light', 'preview_theme', '', 'default', 'N', 'Y', '0', 'admin', TO_DATE('2020-11-11 17:46:40', 'SYYYY-MM-DD HH24:MI:SS'), NULL, NULL, 'atelier-forest-light');
INSERT INTO "SYS_DICT_DATA" VALUES ('48', '16', 'atelier-heath-dark', 'atelier-heath-dark', 'preview_theme', '', 'default', 'N', 'Y', '0', 'admin', TO_DATE('2020-11-11 17:46:40', 'SYYYY-MM-DD HH24:MI:SS'), NULL, NULL, 'atelier-heath-dark');
INSERT INTO "SYS_DICT_DATA" VALUES ('49', '17', 'atelier-heath-light', 'atelier-heath-light', 'preview_theme', '', 'default', 'N', 'Y', '0', 'admin', TO_DATE('2020-11-11 17:46:40', 'SYYYY-MM-DD HH24:MI:SS'), NULL, NULL, 'atelier-heath-light');
INSERT INTO "SYS_DICT_DATA" VALUES ('50', '18', 'atelier-lakeside-dark', 'atelier-lakeside-dark', 'preview_theme', '', 'default', 'N', 'Y', '0', 'admin', TO_DATE('2020-11-11 17:46:40', 'SYYYY-MM-DD HH24:MI:SS'), NULL, NULL, 'atelier-lakeside-dark');
INSERT INTO "SYS_DICT_DATA" VALUES ('51', '19', 'atelier-lakeside-light', 'atelier-lakeside-light', 'preview_theme', '', 'default', 'N', 'Y', '0', 'admin', TO_DATE('2020-11-11 17:46:40', 'SYYYY-MM-DD HH24:MI:SS'), NULL, NULL, 'atelier-lakeside-light');
INSERT INTO "SYS_DICT_DATA" VALUES ('52', '20', 'atelier-plateau-dark', 'atelier-plateau-dark', 'preview_theme', '', 'default', 'N', 'Y', '0', 'admin', TO_DATE('2020-11-11 17:46:40', 'SYYYY-MM-DD HH24:MI:SS'), NULL, NULL, 'atelier-plateau-dark');
INSERT INTO "SYS_DICT_DATA" VALUES ('53', '21', 'atelier-plateau-light', 'atelier-plateau-light', 'preview_theme', '', 'default', 'N', 'Y', '0', 'admin', TO_DATE('2020-11-11 17:46:40', 'SYYYY-MM-DD HH24:MI:SS'), NULL, NULL, 'atelier-plateau-light');
INSERT INTO "SYS_DICT_DATA" VALUES ('54', '22', 'atelier-savanna-dark', 'atelier-savanna-dark', 'preview_theme', '', 'default', 'N', 'Y', '0', 'admin', TO_DATE('2020-11-11 17:46:40', 'SYYYY-MM-DD HH24:MI:SS'), NULL, NULL, 'atelier-savanna-dark');
INSERT INTO "SYS_DICT_DATA" VALUES ('55', '23', 'atelier-savanna-light', 'atelier-savanna-light', 'preview_theme', '', 'default', 'N', 'Y', '0', 'admin', TO_DATE('2020-11-11 17:46:40', 'SYYYY-MM-DD HH24:MI:SS'), NULL, NULL, 'atelier-savanna-light');
INSERT INTO "SYS_DICT_DATA" VALUES ('56', '24', 'atelier-seaside-dark', 'atelier-seaside-dark', 'preview_theme', '', 'default', 'N', 'Y', '0', 'admin', TO_DATE('2020-11-11 17:46:40', 'SYYYY-MM-DD HH24:MI:SS'), NULL, NULL, 'atelier-seaside-dark');
INSERT INTO "SYS_DICT_DATA" VALUES ('57', '25', 'atelier-seaside-light', 'atelier-seaside-light', 'preview_theme', '', 'default', 'N', 'Y', '0', 'admin', TO_DATE('2020-11-11 17:46:40', 'SYYYY-MM-DD HH24:MI:SS'), NULL, NULL, 'atelier-seaside-light');
INSERT INTO "SYS_DICT_DATA" VALUES ('58', '26', 'atelier-sulphurpool-dark', 'atelier-sulphurpool-dark', 'preview_theme', '', 'default', 'N', 'Y', '0', 'admin', TO_DATE('2020-11-11 17:46:41', 'SYYYY-MM-DD HH24:MI:SS'), NULL, NULL, 'atelier-sulphurpool-dark');
INSERT INTO "SYS_DICT_DATA" VALUES ('59', '27', 'atelier-sulphurpool-light', 'atelier-sulphurpool-light', 'preview_theme', '', 'default', 'N', 'Y', '0', 'admin', TO_DATE('2020-11-11 17:46:41', 'SYYYY-MM-DD HH24:MI:SS'), NULL, NULL, 'atelier-sulphurpool-light');
INSERT INTO "SYS_DICT_DATA" VALUES ('60', '28', 'atom-one-dark-reasonable', 'atom-one-dark-reasonable', 'preview_theme', '', 'default', 'N', 'Y', '0', 'admin', TO_DATE('2020-11-11 17:46:41', 'SYYYY-MM-DD HH24:MI:SS'), NULL, NULL, 'atom-one-dark-reasonable');
INSERT INTO "SYS_DICT_DATA" VALUES ('61', '29', 'atom-one-dark', 'atom-one-dark', 'preview_theme', '', 'default', 'N', 'Y', '0', 'admin', TO_DATE('2020-11-11 17:46:41', 'SYYYY-MM-DD HH24:MI:SS'), NULL, NULL, 'atom-one-dark');
INSERT INTO "SYS_DICT_DATA" VALUES ('62', '30', 'atom-one-light', 'atom-one-light', 'preview_theme', '', 'default', 'N', 'Y', '0', 'admin', TO_DATE('2020-11-11 17:46:41', 'SYYYY-MM-DD HH24:MI:SS'), NULL, NULL, 'atom-one-light');
INSERT INTO "SYS_DICT_DATA" VALUES ('63', '31', 'brown-paper', 'brown-paper', 'preview_theme', '', 'default', 'N', 'Y', '0', 'admin', TO_DATE('2020-11-11 17:46:41', 'SYYYY-MM-DD HH24:MI:SS'), NULL, NULL, 'brown-paper');
INSERT INTO "SYS_DICT_DATA" VALUES ('64', '32', 'codepen-embed', 'codepen-embed', 'preview_theme', '', 'default', 'N', 'Y', '0', 'admin', TO_DATE('2020-11-11 17:46:41', 'SYYYY-MM-DD HH24:MI:SS'), NULL, NULL, 'codepen-embed');
INSERT INTO "SYS_DICT_DATA" VALUES ('65', '33', 'color-brewer', 'color-brewer', 'preview_theme', '', 'default', 'N', 'Y', '0', 'admin', TO_DATE('2020-11-11 17:46:41', 'SYYYY-MM-DD HH24:MI:SS'), NULL, NULL, 'color-brewer');
INSERT INTO "SYS_DICT_DATA" VALUES ('66', '34', 'darcula', 'darcula', 'preview_theme', '', 'default', 'N', 'Y', '0', 'admin', TO_DATE('2020-11-11 17:46:41', 'SYYYY-MM-DD HH24:MI:SS'), NULL, NULL, 'darcula');
INSERT INTO "SYS_DICT_DATA" VALUES ('67', '35', 'dark', 'dark', 'preview_theme', '', 'default', 'N', 'Y', '0', 'admin', TO_DATE('2020-11-11 17:46:41', 'SYYYY-MM-DD HH24:MI:SS'), NULL, NULL, 'dark');
INSERT INTO "SYS_DICT_DATA" VALUES ('68', '36', 'default', 'default', 'preview_theme', '', 'default', 'N', 'Y', '0', 'admin', TO_DATE('2020-11-11 17:46:41', 'SYYYY-MM-DD HH24:MI:SS'), NULL, NULL, 'default');
INSERT INTO "SYS_DICT_DATA" VALUES ('69', '37', 'docco', 'docco', 'preview_theme', '', 'default', 'N', 'Y', '0', 'admin', TO_DATE('2020-11-11 17:46:41', 'SYYYY-MM-DD HH24:MI:SS'), NULL, NULL, 'docco');
INSERT INTO "SYS_DICT_DATA" VALUES ('70', '38', 'dracula', 'dracula', 'preview_theme', '', 'default', 'N', 'Y', '0', 'admin', TO_DATE('2020-11-11 17:46:41', 'SYYYY-MM-DD HH24:MI:SS'), NULL, NULL, 'dracula');
INSERT INTO "SYS_DICT_DATA" VALUES ('71', '39', 'far', 'far', 'preview_theme', '', 'default', 'N', 'Y', '0', 'admin', TO_DATE('2020-11-11 17:46:41', 'SYYYY-MM-DD HH24:MI:SS'), NULL, NULL, 'far');
INSERT INTO "SYS_DICT_DATA" VALUES ('72', '40', 'foundation', 'foundation', 'preview_theme', '', 'default', 'N', 'Y', '0', 'admin', TO_DATE('2020-11-11 17:46:41', 'SYYYY-MM-DD HH24:MI:SS'), NULL, NULL, 'foundation');
INSERT INTO "SYS_DICT_DATA" VALUES ('73', '41', 'github-gist', 'github-gist', 'preview_theme', '', 'default', 'N', 'Y', '0', 'admin', TO_DATE('2020-11-11 17:46:42', 'SYYYY-MM-DD HH24:MI:SS'), NULL, NULL, 'github-gist');
INSERT INTO "SYS_DICT_DATA" VALUES ('74', '42', 'github', 'github', 'preview_theme', '', 'default', 'N', 'Y', '0', 'admin', TO_DATE('2020-11-11 17:46:42', 'SYYYY-MM-DD HH24:MI:SS'), NULL, NULL, 'github');
INSERT INTO "SYS_DICT_DATA" VALUES ('75', '43', 'gml', 'gml', 'preview_theme', '', 'default', 'N', 'Y', '0', 'admin', TO_DATE('2020-11-11 17:46:42', 'SYYYY-MM-DD HH24:MI:SS'), NULL, NULL, 'gml');
INSERT INTO "SYS_DICT_DATA" VALUES ('76', '44', 'googlecode', 'googlecode', 'preview_theme', '', 'default', 'N', 'Y', '0', 'admin', TO_DATE('2020-11-11 17:46:42', 'SYYYY-MM-DD HH24:MI:SS'), NULL, NULL, 'googlecode');
INSERT INTO "SYS_DICT_DATA" VALUES ('77', '45', 'gradient-dark', 'gradient-dark', 'preview_theme', '', 'default', 'N', 'Y', '0', 'admin', TO_DATE('2020-11-11 17:46:42', 'SYYYY-MM-DD HH24:MI:SS'), NULL, NULL, 'gradient-dark');
INSERT INTO "SYS_DICT_DATA" VALUES ('78', '46', 'gradient-light', 'gradient-light', 'preview_theme', '', 'default', 'N', 'Y', '0', 'admin', TO_DATE('2020-11-11 17:46:42', 'SYYYY-MM-DD HH24:MI:SS'), NULL, NULL, 'gradient-light');
INSERT INTO "SYS_DICT_DATA" VALUES ('79', '47', 'grayscale', 'grayscale', 'preview_theme', '', 'default', 'N', 'Y', '0', 'admin', TO_DATE('2020-11-11 17:46:42', 'SYYYY-MM-DD HH24:MI:SS'), NULL, NULL, 'grayscale');
INSERT INTO "SYS_DICT_DATA" VALUES ('80', '48', 'gruvbox-dark', 'gruvbox-dark', 'preview_theme', '', 'default', 'N', 'Y', '0', 'admin', TO_DATE('2020-11-11 17:46:42', 'SYYYY-MM-DD HH24:MI:SS'), NULL, NULL, 'gruvbox-dark');
INSERT INTO "SYS_DICT_DATA" VALUES ('81', '49', 'gruvbox-light', 'gruvbox-light', 'preview_theme', '', 'default', 'N', 'Y', '0', 'admin', TO_DATE('2020-11-11 17:46:42', 'SYYYY-MM-DD HH24:MI:SS'), NULL, NULL, 'gruvbox-light');
INSERT INTO "SYS_DICT_DATA" VALUES ('82', '50', 'hopscotch', 'hopscotch', 'preview_theme', '', 'default', 'N', 'Y', '0', 'admin', TO_DATE('2020-11-11 17:46:42', 'SYYYY-MM-DD HH24:MI:SS'), NULL, NULL, 'hopscotch');
INSERT INTO "SYS_DICT_DATA" VALUES ('83', '51', 'hybrid', 'hybrid', 'preview_theme', '', 'default', 'N', 'Y', '0', 'admin', TO_DATE('2020-11-11 17:46:42', 'SYYYY-MM-DD HH24:MI:SS'), NULL, NULL, 'hybrid');
INSERT INTO "SYS_DICT_DATA" VALUES ('84', '52', 'idea', 'idea', 'preview_theme', '', 'default', 'N', 'Y', '0', 'admin', TO_DATE('2020-11-11 17:46:42', 'SYYYY-MM-DD HH24:MI:SS'), NULL, NULL, 'idea');
INSERT INTO "SYS_DICT_DATA" VALUES ('85', '53', 'ir-black', 'ir-black', 'preview_theme', '', 'default', 'N', 'Y', '0', 'admin', TO_DATE('2020-11-11 17:46:42', 'SYYYY-MM-DD HH24:MI:SS'), NULL, NULL, 'ir-black');
INSERT INTO "SYS_DICT_DATA" VALUES ('86', '54', 'isbl-editor-dark', 'isbl-editor-dark', 'preview_theme', '', 'default', 'N', 'Y', '0', 'admin', TO_DATE('2020-11-11 17:46:42', 'SYYYY-MM-DD HH24:MI:SS'), NULL, NULL, 'isbl-editor-dark');
INSERT INTO "SYS_DICT_DATA" VALUES ('87', '55', 'isbl-editor-light', 'isbl-editor-light', 'preview_theme', '', 'default', 'N', 'Y', '0', 'admin', TO_DATE('2020-11-11 17:46:42', 'SYYYY-MM-DD HH24:MI:SS'), NULL, NULL, 'isbl-editor-light');
INSERT INTO "SYS_DICT_DATA" VALUES ('88', '56', 'kimbie', 'kimbie', 'preview_theme', '', 'default', 'N', 'Y', '0', 'admin', TO_DATE('2020-11-11 17:46:43', 'SYYYY-MM-DD HH24:MI:SS'), NULL, NULL, 'kimbie');
INSERT INTO "SYS_DICT_DATA" VALUES ('89', '57', 'kimbie', 'kimbie', 'preview_theme', '', 'default', 'N', 'Y', '0', 'admin', TO_DATE('2020-11-11 17:46:43', 'SYYYY-MM-DD HH24:MI:SS'), NULL, NULL, 'kimbie');
INSERT INTO "SYS_DICT_DATA" VALUES ('90', '58', 'lightfair', 'lightfair', 'preview_theme', '', 'default', 'N', 'Y', '0', 'admin', TO_DATE('2020-11-11 17:46:43', 'SYYYY-MM-DD HH24:MI:SS'), NULL, NULL, 'lightfair');
INSERT INTO "SYS_DICT_DATA" VALUES ('91', '59', 'lioshi', 'lioshi', 'preview_theme', '', 'default', 'N', 'Y', '0', 'admin', TO_DATE('2020-11-11 17:46:43', 'SYYYY-MM-DD HH24:MI:SS'), NULL, NULL, 'lioshi');
INSERT INTO "SYS_DICT_DATA" VALUES ('92', '60', 'magula', 'magula', 'preview_theme', '', 'default', 'N', 'Y', '0', 'admin', TO_DATE('2020-11-11 17:46:43', 'SYYYY-MM-DD HH24:MI:SS'), NULL, NULL, 'magula');
INSERT INTO "SYS_DICT_DATA" VALUES ('93', '61', 'mono-blue', 'mono-blue', 'preview_theme', '', 'default', 'N', 'Y', '0', 'admin', TO_DATE('2020-11-11 17:46:43', 'SYYYY-MM-DD HH24:MI:SS'), NULL, NULL, 'mono-blue');
INSERT INTO "SYS_DICT_DATA" VALUES ('94', '62', 'monokai-sublime', 'monokai-sublime', 'preview_theme', '', 'default', 'N', 'Y', '0', 'admin', TO_DATE('2020-11-11 17:46:43', 'SYYYY-MM-DD HH24:MI:SS'), NULL, NULL, 'monokai-sublime');
INSERT INTO "SYS_DICT_DATA" VALUES ('95', '63', 'monokai', 'monokai', 'preview_theme', '', 'default', 'N', 'Y', '0', 'admin', TO_DATE('2020-11-11 17:46:43', 'SYYYY-MM-DD HH24:MI:SS'), NULL, NULL, 'monokai');
INSERT INTO "SYS_DICT_DATA" VALUES ('96', '64', 'night-owl', 'night-owl', 'preview_theme', '', 'default', 'N', 'Y', '0', 'admin', TO_DATE('2020-11-11 17:46:43', 'SYYYY-MM-DD HH24:MI:SS'), NULL, NULL, 'night-owl');
INSERT INTO "SYS_DICT_DATA" VALUES ('97', '65', 'nnfx-dark', 'nnfx-dark', 'preview_theme', '', 'default', 'N', 'Y', '0', 'admin', TO_DATE('2020-11-11 17:46:43', 'SYYYY-MM-DD HH24:MI:SS'), NULL, NULL, 'nnfx-dark');
INSERT INTO "SYS_DICT_DATA" VALUES ('98', '66', 'nnfx', 'nnfx', 'preview_theme', '', 'default', 'N', 'Y', '0', 'admin', TO_DATE('2020-11-11 17:46:43', 'SYYYY-MM-DD HH24:MI:SS'), NULL, NULL, 'nnfx');
INSERT INTO "SYS_DICT_DATA" VALUES ('99', '67', 'nord', 'nord', 'preview_theme', '', 'default', 'N', 'Y', '0', 'admin', TO_DATE('2020-11-11 17:46:43', 'SYYYY-MM-DD HH24:MI:SS'), NULL, NULL, 'nord');
INSERT INTO "SYS_DICT_DATA" VALUES ('100', '68', 'obsidian', 'obsidian', 'preview_theme', '', 'default', 'N', 'Y', '0', 'admin', TO_DATE('2020-11-11 17:46:43', 'SYYYY-MM-DD HH24:MI:SS'), NULL, NULL, 'obsidian');
INSERT INTO "SYS_DICT_DATA" VALUES ('101', '69', 'ocean', 'ocean', 'preview_theme', '', 'default', 'N', 'Y', '0', 'admin', TO_DATE('2020-11-11 17:46:43', 'SYYYY-MM-DD HH24:MI:SS'), NULL, NULL, 'ocean');
INSERT INTO "SYS_DICT_DATA" VALUES ('102', '70', 'paraiso-dark', 'paraiso-dark', 'preview_theme', '', 'default', 'N', 'Y', '0', 'admin', TO_DATE('2020-11-11 17:46:44', 'SYYYY-MM-DD HH24:MI:SS'), NULL, NULL, 'paraiso-dark');
INSERT INTO "SYS_DICT_DATA" VALUES ('103', '71', 'paraiso-light', 'paraiso-light', 'preview_theme', '', 'default', 'N', 'Y', '0', 'admin', TO_DATE('2020-11-11 17:46:44', 'SYYYY-MM-DD HH24:MI:SS'), NULL, NULL, 'paraiso-light');
INSERT INTO "SYS_DICT_DATA" VALUES ('104', '72', 'pojoaque', 'pojoaque', 'preview_theme', '', 'default', 'N', 'Y', '0', 'admin', TO_DATE('2020-11-11 17:46:44', 'SYYYY-MM-DD HH24:MI:SS'), NULL, NULL, 'pojoaque');
INSERT INTO "SYS_DICT_DATA" VALUES ('105', '73', 'purebasic', 'purebasic', 'preview_theme', '', 'default', 'N', 'Y', '0', 'admin', TO_DATE('2020-11-11 17:46:44', 'SYYYY-MM-DD HH24:MI:SS'), NULL, NULL, 'purebasic');
INSERT INTO "SYS_DICT_DATA" VALUES ('106', '74', 'qtcreator_dark', 'qtcreator_dark', 'preview_theme', '', 'default', 'N', 'Y', '0', 'admin', TO_DATE('2020-11-11 17:46:44', 'SYYYY-MM-DD HH24:MI:SS'), NULL, NULL, 'qtcreator_dark');
INSERT INTO "SYS_DICT_DATA" VALUES ('107', '75', 'qtcreator_light', 'qtcreator_light', 'preview_theme', '', 'default', 'N', 'Y', '0', 'admin', TO_DATE('2020-11-11 17:46:44', 'SYYYY-MM-DD HH24:MI:SS'), NULL, NULL, 'qtcreator_light');
INSERT INTO "SYS_DICT_DATA" VALUES ('108', '76', 'railscasts', 'railscasts', 'preview_theme', '', 'default', 'N', 'Y', '0', 'admin', TO_DATE('2020-11-11 17:46:44', 'SYYYY-MM-DD HH24:MI:SS'), NULL, NULL, 'railscasts');
INSERT INTO "SYS_DICT_DATA" VALUES ('109', '77', 'rainbow', 'rainbow', 'preview_theme', '', 'default', 'N', 'Y', '0', 'admin', TO_DATE('2020-11-11 17:46:44', 'SYYYY-MM-DD HH24:MI:SS'), NULL, NULL, 'rainbow');
INSERT INTO "SYS_DICT_DATA" VALUES ('110', '78', 'routeros', 'routeros', 'preview_theme', '', 'default', 'N', 'Y', '0', 'admin', TO_DATE('2020-11-11 17:46:44', 'SYYYY-MM-DD HH24:MI:SS'), NULL, NULL, 'routeros');
INSERT INTO "SYS_DICT_DATA" VALUES ('111', '79', 'school-book', 'school-book', 'preview_theme', '', 'default', 'N', 'Y', '0', 'admin', TO_DATE('2020-11-11 17:46:44', 'SYYYY-MM-DD HH24:MI:SS'), NULL, NULL, 'school-book');
INSERT INTO "SYS_DICT_DATA" VALUES ('112', '80', 'shades-of-purple', 'shades-of-purple', 'preview_theme', '', 'default', 'N', 'Y', '0', 'admin', TO_DATE('2020-11-11 17:46:44', 'SYYYY-MM-DD HH24:MI:SS'), NULL, NULL, 'shades-of-purple');
INSERT INTO "SYS_DICT_DATA" VALUES ('113', '81', 'solarized-dark', 'solarized-dark', 'preview_theme', '', 'default', 'N', 'Y', '0', 'admin', TO_DATE('2020-11-11 17:46:44', 'SYYYY-MM-DD HH24:MI:SS'), NULL, NULL, 'solarized-dark');
INSERT INTO "SYS_DICT_DATA" VALUES ('114', '82', 'solarized-light', 'solarized-light', 'preview_theme', '', 'default', 'N', 'Y', '0', 'admin', TO_DATE('2020-11-11 17:46:44', 'SYYYY-MM-DD HH24:MI:SS'), NULL, NULL, 'solarized-light');
INSERT INTO "SYS_DICT_DATA" VALUES ('115', '83', 'srcery', 'srcery', 'preview_theme', '', 'default', 'N', 'Y', '0', 'admin', TO_DATE('2020-11-11 17:46:44', 'SYYYY-MM-DD HH24:MI:SS'), NULL, NULL, 'srcery');
INSERT INTO "SYS_DICT_DATA" VALUES ('116', '84', 'sunburst', 'sunburst', 'preview_theme', '', 'default', 'N', 'Y', '0', 'admin', TO_DATE('2020-11-11 17:46:44', 'SYYYY-MM-DD HH24:MI:SS'), NULL, NULL, 'sunburst');
INSERT INTO "SYS_DICT_DATA" VALUES ('117', '85', 'tomorrow-night-blue', 'tomorrow-night-blue', 'preview_theme', '', 'default', 'N', 'Y', '0', 'admin', TO_DATE('2020-11-11 17:46:45', 'SYYYY-MM-DD HH24:MI:SS'), NULL, NULL, 'tomorrow-night-blue');
INSERT INTO "SYS_DICT_DATA" VALUES ('118', '86', 'tomorrow-night-bright', 'tomorrow-night-bright', 'preview_theme', '', 'default', 'N', 'Y', '0', 'admin', TO_DATE('2020-11-11 17:46:45', 'SYYYY-MM-DD HH24:MI:SS'), NULL, NULL, 'tomorrow-night-bright');
INSERT INTO "SYS_DICT_DATA" VALUES ('119', '87', 'tomorrow-night-eighties', 'tomorrow-night-eighties', 'preview_theme', '', 'default', 'N', 'Y', '0', 'admin', TO_DATE('2020-11-11 17:46:45', 'SYYYY-MM-DD HH24:MI:SS'), NULL, NULL, 'tomorrow-night-eighties');
INSERT INTO "SYS_DICT_DATA" VALUES ('120', '88', 'tomorrow-night', 'tomorrow-night', 'preview_theme', '', 'default', 'N', 'Y', '0', 'admin', TO_DATE('2020-11-11 17:46:45', 'SYYYY-MM-DD HH24:MI:SS'), NULL, NULL, 'tomorrow-night');
INSERT INTO "SYS_DICT_DATA" VALUES ('121', '89', 'tomorrow', 'tomorrow', 'preview_theme', '', 'default', 'N', 'Y', '0', 'admin', TO_DATE('2020-11-11 17:46:45', 'SYYYY-MM-DD HH24:MI:SS'), NULL, NULL, 'tomorrow');
INSERT INTO "SYS_DICT_DATA" VALUES ('122', '90', 'vs', 'vs', 'preview_theme', '', 'default', 'N', 'Y', '0', 'admin', TO_DATE('2020-11-11 17:46:45', 'SYYYY-MM-DD HH24:MI:SS'), NULL, NULL, 'vs');
INSERT INTO "SYS_DICT_DATA" VALUES ('123', '91', 'vs2015', 'vs2015', 'preview_theme', '', 'default', 'N', 'Y', '0', 'admin', TO_DATE('2020-11-11 17:46:45', 'SYYYY-MM-DD HH24:MI:SS'), NULL, NULL, 'vs2015');
INSERT INTO "SYS_DICT_DATA" VALUES ('124', '92', 'xcode', 'xcode', 'preview_theme', '', 'default', 'N', 'Y', '0', 'admin', TO_DATE('2020-11-11 17:46:45', 'SYYYY-MM-DD HH24:MI:SS'), NULL, NULL, 'xcode');
INSERT INTO "SYS_DICT_DATA" VALUES ('125', '93', 'xt256', 'xt256', 'preview_theme', '', 'default', 'N', 'Y', '0', 'admin', TO_DATE('2020-11-11 17:46:45', 'SYYYY-MM-DD HH24:MI:SS'), NULL, NULL, 'xt256');
INSERT INTO "SYS_DICT_DATA" VALUES ('126', '94', 'zenburn', 'zenburn', 'preview_theme', '', 'default', 'N', 'Y', '0', 'admin', TO_DATE('2020-11-11 17:46:45', 'SYYYY-MM-DD HH24:MI:SS'), NULL, NULL, 'zenburn');
INSERT INTO "SYS_DICT_DATA" VALUES ('127', '2', 'mybatis-plus', 'mybatis-plus', 'template_type', '', 'default', 'Y', 'Y', '0', 'admin', TO_DATE('2020-11-12 21:40:31', 'SYYYY-MM-DD HH24:MI:SS'), NULL, NULL, '标准 mybatis-plus crud 模版');
INSERT INTO "SYS_DICT_DATA" VALUES ('128', '3', 'ruoyi', 'ruoyi', 'template_type', '', 'default', 'N', 'Y', '0', 'admin', TO_DATE('2020-11-15 00:48:35', 'SYYYY-MM-DD HH24:MI:SS'), 'admin', TO_DATE('2020-11-15 00:48:47', 'SYYYY-MM-DD HH24:MI:SS'), 'ruoyi');
INSERT INTO "SYS_DICT_DATA" VALUES ('129', '1', '系统配置', '1', 'config_type', '', 'default', 'Y', 'Y', '0', 'admin', TO_DATE('2020-11-15 09:13:26', 'SYYYY-MM-DD HH24:MI:SS'), NULL, NULL, '系统配置');
INSERT INTO "SYS_DICT_DATA" VALUES ('130', '2', '生成配置', '2', 'config_type', '', 'default', 'N', 'Y', '0', 'admin', TO_DATE('2020-11-15 09:13:43', 'SYYYY-MM-DD HH24:MI:SS'), NULL, NULL, '生成配置');
INSERT INTO "SYS_DICT_DATA" VALUES ('131', '3', '下拉框', '2', 'control_type', '', 'default', 'N', 'Y', '0', 'admin', TO_DATE('2020-11-19 09:29:50', 'SYYYY-MM-DD HH24:MI:SS'), 'admin', TO_DATE('2020-11-25 12:07:56', 'SYYYY-MM-DD HH24:MI:SS'), '系统下拉框控件');
INSERT INTO "SYS_DICT_DATA" VALUES ('132', '2', '单选框', '1', 'control_type', '', 'default', 'N', 'Y', '0', 'admin', TO_DATE('2020-11-19 09:30:42', 'SYYYY-MM-DD HH24:MI:SS'), 'admin', TO_DATE('2020-11-25 12:07:20', 'SYYYY-MM-DD HH24:MI:SS'), '系统单选框组控件');
INSERT INTO "SYS_DICT_DATA" VALUES ('133', '1', '文本框', '0', 'control_type', '', 'default', 'Y', 'Y', '0', 'admin', TO_DATE('2020-11-19 09:31:30', 'SYYYY-MM-DD HH24:MI:SS'), 'admin', TO_DATE('2020-11-25 12:07:08', 'SYYYY-MM-DD HH24:MI:SS'), '系统默认输入框组件');
COMMIT;

-- ----------------------------
-- Table structure for SYS_DICT_TYPE
-- ----------------------------
CREATE TABLE "SYS_DICT_TYPE" (
  "DICT_ID" NUMBER(11) NOT NULL ,
  "DICT_NAME" NVARCHAR2(100) ,
  "DICT_TYPE" NVARCHAR2(100) ,
  "STATUS" NCHAR(1) ,
  "CREATE_BY" NVARCHAR2(64) ,
  "CREATE_TIME" DATE ,
  "UPDATE_BY" NVARCHAR2(64) ,
  "UPDATE_TIME" DATE ,
  "REMARK" NVARCHAR2(500) 
)
;
COMMENT ON COLUMN "SYS_DICT_TYPE"."DICT_ID" IS '字典主键';
COMMENT ON COLUMN "SYS_DICT_TYPE"."DICT_NAME" IS '字典名称';
COMMENT ON COLUMN "SYS_DICT_TYPE"."DICT_TYPE" IS '字典类型';
COMMENT ON COLUMN "SYS_DICT_TYPE"."STATUS" IS '状态（0正常 1停用）';
COMMENT ON COLUMN "SYS_DICT_TYPE"."CREATE_BY" IS '创建人';
COMMENT ON COLUMN "SYS_DICT_TYPE"."CREATE_TIME" IS '创建时间';
COMMENT ON COLUMN "SYS_DICT_TYPE"."UPDATE_BY" IS '修改人';
COMMENT ON COLUMN "SYS_DICT_TYPE"."UPDATE_TIME" IS '修改时间';
COMMENT ON COLUMN "SYS_DICT_TYPE"."REMARK" IS '备注信息';
COMMENT ON TABLE "SYS_DICT_TYPE" IS '字典类型';

-- ----------------------------
-- Records of "SYS_DICT_TYPE"
-- ----------------------------
INSERT INTO "SYS_DICT_TYPE" VALUES ('1', '用户性别', 'sys_user_sex', '0', 'admin', TO_DATE('2020-11-10 22:04:40', 'SYYYY-MM-DD HH24:MI:SS'), 'admin', TO_DATE('2020-11-10 22:04:40', 'SYYYY-MM-DD HH24:MI:SS'), '用户性别');
INSERT INTO "SYS_DICT_TYPE" VALUES ('2', '菜单状态', 'sys_show_hide', '0', 'admin', TO_DATE('2020-11-10 22:04:46', 'SYYYY-MM-DD HH24:MI:SS'), 'admin', TO_DATE('2020-11-10 22:04:46', 'SYYYY-MM-DD HH24:MI:SS'), '菜单状态');
INSERT INTO "SYS_DICT_TYPE" VALUES ('3', '系统开关', 'sys_normal_disable', '0', 'admin', TO_DATE('2020-11-10 22:04:46', 'SYYYY-MM-DD HH24:MI:SS'), 'admin', TO_DATE('2020-11-10 22:04:46', 'SYYYY-MM-DD HH24:MI:SS'), '系统开关');
INSERT INTO "SYS_DICT_TYPE" VALUES ('4', '任务状态', 'sys_job_status', '0', 'admin', TO_DATE('2020-11-10 22:04:46', 'SYYYY-MM-DD HH24:MI:SS'), 'admin', TO_DATE('2020-11-10 22:04:46', 'SYYYY-MM-DD HH24:MI:SS'), '任务状态');
INSERT INTO "SYS_DICT_TYPE" VALUES ('5', '任务分组', 'sys_job_group', '0', 'admin', TO_DATE('2020-11-10 22:04:46', 'SYYYY-MM-DD HH24:MI:SS'), 'admin', TO_DATE('2020-11-10 22:04:46', 'SYYYY-MM-DD HH24:MI:SS'), '任务分组');
INSERT INTO "SYS_DICT_TYPE" VALUES ('6', '系统是否', 'sys_yes_no', '0', 'admin', TO_DATE('2020-11-10 22:04:46', 'SYYYY-MM-DD HH24:MI:SS'), 'admin', TO_DATE('2020-11-10 22:04:46', 'SYYYY-MM-DD HH24:MI:SS'), '系统是否');
INSERT INTO "SYS_DICT_TYPE" VALUES ('7', '模版类型', 'template_type', '0', 'admin', TO_DATE('2020-11-10 22:04:46', 'SYYYY-MM-DD HH24:MI:SS'), 'admin', TO_DATE('2020-11-12 21:38:09', 'SYYYY-MM-DD HH24:MI:SS'), '代码生成模版类型');
INSERT INTO "SYS_DICT_TYPE" VALUES ('8', '通知状态', 'sys_notice_status', '0', 'admin', TO_DATE('2020-11-10 22:04:46', 'SYYYY-MM-DD HH24:MI:SS'), 'admin', TO_DATE('2020-11-10 22:04:46', 'SYYYY-MM-DD HH24:MI:SS'), '通知状态');
INSERT INTO "SYS_DICT_TYPE" VALUES ('9', '操作类型', 'sys_oper_type', '0', 'admin', TO_DATE('2020-11-10 22:04:46', 'SYYYY-MM-DD HH24:MI:SS'), 'admin', TO_DATE('2020-11-10 22:04:46', 'SYYYY-MM-DD HH24:MI:SS'), '操作类型');
INSERT INTO "SYS_DICT_TYPE" VALUES ('10', '系统状态', 'sys_common_status', '0', 'admin', TO_DATE('2020-11-10 22:04:46', 'SYYYY-MM-DD HH24:MI:SS'), 'admin', TO_DATE('2020-11-10 22:04:46', 'SYYYY-MM-DD HH24:MI:SS'), '系统状态');
INSERT INTO "SYS_DICT_TYPE" VALUES ('11', '数据库类型', 'sys_db_type', '0', 'admin', TO_DATE('2020-11-10 22:04:46', 'SYYYY-MM-DD HH24:MI:SS'), 'admin', TO_DATE('2020-11-10 22:04:46', 'SYYYY-MM-DD HH24:MI:SS'), '数据库类型');
INSERT INTO "SYS_DICT_TYPE" VALUES ('12', 'Oracle连接方式', 'sys_oracle_mode', '0', 'admin', TO_DATE('2020-11-10 22:04:46', 'SYYYY-MM-DD HH24:MI:SS'), 'admin', TO_DATE('2020-11-10 22:04:46', 'SYYYY-MM-DD HH24:MI:SS'), 'Oracle连接方式');
INSERT INTO "SYS_DICT_TYPE" VALUES ('13', '预览主题', 'preview_theme', '0', 'admin', TO_DATE('2020-11-11 13:43:36', 'SYYYY-MM-DD HH24:MI:SS'), 'admin', TO_DATE('2020-11-11 13:43:45', 'SYYYY-MM-DD HH24:MI:SS'), '代码生成预览主题');
INSERT INTO "SYS_DICT_TYPE" VALUES ('14', '回显样式', 'dict_list_class', '0', 'admin', TO_DATE('2020-11-11 17:14:41', 'SYYYY-MM-DD HH24:MI:SS'), NULL, NULL, '字典数据表格回显样式');
INSERT INTO "SYS_DICT_TYPE" VALUES ('15', '配置类型', 'config_type', '0', 'admin', TO_DATE('2020-11-13 14:52:07', 'SYYYY-MM-DD HH24:MI:SS'), 'admin', TO_DATE('2020-11-15 09:15:01', 'SYYYY-MM-DD HH24:MI:SS'), '配置类型');
INSERT INTO "SYS_DICT_TYPE" VALUES ('16', '控件类型', 'control_type', '0', 'admin', TO_DATE('2020-11-19 09:29:12', 'SYYYY-MM-DD HH24:MI:SS'), NULL, NULL, '管理控件的类型（select,radio,input）');
COMMIT;

-- ----------------------------
-- Primary Key structure for table gen_table
-- ----------------------------
ALTER TABLE "GEN_TABLE" ADD PRIMARY KEY ("TABLE_ID");

-- ----------------------------
-- Primary Key structure for table GEN_TABLE_COLUMN
-- ----------------------------
ALTER TABLE "GEN_TABLE_COLUMN" ADD PRIMARY KEY ("COLUMN_ID");

-- ----------------------------
-- Primary Key structure for table SYS_CONFIG
-- ----------------------------
ALTER TABLE "SYS_CONFIG" ADD PRIMARY KEY ("CONFIG_ID");

-- ----------------------------
-- Primary Key structure for table SYS_DATA_SOURCE
-- ----------------------------
ALTER TABLE "SYS_DATA_SOURCE" ADD PRIMARY KEY ("ID");

-- ----------------------------
-- Primary Key structure for table SYS_DICT_DATA
-- ----------------------------
ALTER TABLE "SYS_DICT_DATA" ADD PRIMARY KEY ("DICT_CODE");

-- ----------------------------
-- Primary Key structure for table SYS_DICT_TYPE
-- ----------------------------
ALTER TABLE "SYS_DICT_TYPE" ADD PRIMARY KEY ("DICT_ID");

-- ----------------------------
-- Indexes structure for table SYS_DICT_TYPE
-- ----------------------------
CREATE UNIQUE INDEX "DICT_TYPE"
  ON "SYS_DICT_TYPE" ("DICT_TYPE");
COMMIT;
