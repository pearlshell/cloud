package com.wangming.generator.support.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.wangming.generator.support.domain.SysConfig;

/**
 * 系统配置信息 BaseMapper
 *
 * @author wangming
 */
public interface SysConfigMapper extends BaseMapper<SysConfig> {

}
