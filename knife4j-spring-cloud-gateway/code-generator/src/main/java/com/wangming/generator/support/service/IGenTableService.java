package com.wangming.generator.support.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.wangming.generator.support.domain.GenTable;
import com.wangming.generator.support.query.GenTableQuery;

import java.util.List;
import java.util.Map;

/**
 * 业务 服务层
 *
 * @author wangming
 */
public interface IGenTableService extends IService<GenTable> {

    /**
     * 查询据库列表
     *
     * @param tableQuery 业务信息
     * @return 数据库表集合
     */
    List<GenTable> selectDbTableList(GenTableQuery tableQuery);

    /**
     * 查询据库列表
     *
     * @param tableNames   表名称组
     * @param dataSourceId 数据源主键
     * @return 数据库表集合
     */
    List<GenTable> selectDbTableListByNames(String[] tableNames, Long dataSourceId);

    /**
     * 查询所有表信息
     *
     * @param dataSourceId 数据源主键
     * @return 表信息集合
     */
    List<GenTable> selectGenTableAll(Long dataSourceId);

    /**
     * 查询业务信息
     *
     * @param id 业务ID
     * @return 业务信息
     */
    GenTable selectGenTableById(Long id);

    /**
     * 根据id修改业务表数据
     *
     * @param genTable 业务信息
     */
    void updateGenTable(GenTable genTable);

    /**
     * 根据id删除业务表数据
     *
     * @param ids 需要删除的数据ID
     */
    void deleteGenTableByIds(List<Long> ids);

    /**
     * 导入表结构
     *
     * @param tableList    导入表列表
     * @param dataSourceId 数据源主键
     */
    void importGenTable(List<GenTable> tableList, Long dataSourceId);

    /**
     * 预览代码
     *
     * @param tableId 表编号
     * @return 预览数据列表
     */
    Map<String, String> previewCode(Long tableId);

    /**
     * 生成代码（下载方式）
     *
     * @param tableName 表名称
     * @return 数据
     */
    byte[] downloadCode(String tableName);

    /**
     * 生成代码（自定义路径）
     *
     * @param tableName 表名称
     */
    void generatorCode(String tableName);

    /**
     * 从数据库同步更新表结构
     *
     * @param tableName 表名称
     */
    void syncDb(String tableName);

    /**
     * 批量生成代码（下载方式）
     *
     * @param tableNames 表数组
     * @return 数据
     */
    byte[] downloadCode(String[] tableNames);

    /**
     * 修改保存参数校验
     *
     * @param genTable 业务信息
     */
    void validateEdit(GenTable genTable);

}
