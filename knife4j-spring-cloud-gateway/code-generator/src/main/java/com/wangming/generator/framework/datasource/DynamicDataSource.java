package com.wangming.generator.framework.datasource;

import com.wangming.generator.common.config.datasource.DynamicDataSourceContextHolder;
import org.springframework.jdbc.datasource.lookup.AbstractRoutingDataSource;

import javax.sql.DataSource;
import java.util.Map;

/**
 * 动态数据源
 *
 * @author wangming
 */
public class DynamicDataSource extends AbstractRoutingDataSource {

    /**
     * 设置数据源
     *
     * @param defaultTargetDataSource 默认数据源（主数据源）
     * @param targetDataSources       备选数据源集合 （从数据源）
     */
    public DynamicDataSource(DataSource defaultTargetDataSource, Map<Object, Object> targetDataSources) {
        super.setDefaultTargetDataSource(defaultTargetDataSource);
        super.setTargetDataSources(targetDataSources);
        super.afterPropertiesSet();
    }

    /**
     * 确定当前查找关键字
     *
     * @return 数据源变量
     */
    @Override
    protected Object determineCurrentLookupKey() {
        return DynamicDataSourceContextHolder.getDataSourceType();
    }

}