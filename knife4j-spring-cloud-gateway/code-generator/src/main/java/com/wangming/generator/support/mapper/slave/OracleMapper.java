package com.wangming.generator.support.mapper.slave;

import com.wangming.generator.support.service.BaseService;

/**
 * Oracle数据源 数据层
 *
 * @author wangming
 */
public interface OracleMapper extends BaseService {

}