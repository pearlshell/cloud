package com.wangming.generator.support.controller;

import com.wangming.generator.common.constant.ConfigConstants;
import com.wangming.generator.common.constant.Constants;
import com.wangming.generator.common.core.controller.BaseController;
import com.wangming.generator.common.core.domain.AjaxResult;
import com.wangming.generator.common.core.page.TableDataInfo;
import com.wangming.generator.common.utils.MapDataUtil;
import com.wangming.generator.common.utils.StringUtils;
import com.wangming.generator.support.domain.SysConfig;
import com.wangming.generator.support.domain.SysDictData;
import com.wangming.generator.support.query.SysConfigQuery;
import com.wangming.generator.support.service.ISysConfigService;
import com.wangming.generator.support.service.ISysDictTypeService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * 系统配置信息
 *
 * @author wangming
 */
@Controller
@RequestMapping("/system/config")
@Api(value = "系统配置信息", tags = "系统配置信息")
public class SysConfigController extends BaseController {

    private static final String PREFIX = "system/config";

    @Resource
    private ISysConfigService configService;
    @Resource
    private ISysDictTypeService sysDictTypeService;

    /**
     * 配置列表界面
     */
    @GetMapping()
    public String config() {
        return PREFIX + "/config";
    }

    /**
     * 查询参数配置列表
     */
    @ResponseBody
    @PostMapping("/list")
    public TableDataInfo<SysConfig> list(SysConfigQuery query) {
        return toPageResult(configService.list(query.autoPageWrapper()));
    }

    /**
     * 新增参数配置
     */
    @GetMapping("/add")
    public String add(ModelMap modelMap) {
        modelMap.put("dictType", sysDictTypeService.list());
        return PREFIX + "/add";
    }

    /**
     * 新增保存参数配置
     */
    @ResponseBody
    @PostMapping("/add")
    public AjaxResult<Object> addSave(@Validated SysConfig config) {
        if (Constants.CONFIG_KEY_NOT_UNIQUE.equals(configService.checkConfigKeyUnique(config))) {
            return error("新增参数'" + config.getConfigName() + "'失败，参数键名已存在");
        }
        if (ConfigConstants.INPUT_TYPE_TEXT.equals(config.getInputType()) && StringUtils.isBlank(config.getConfigValue())) {
            return error("参数键值不能为空");
        }
        boolean flag = (ConfigConstants.INPUT_TYPE_SELECT.equals(config.getInputType()) || ConfigConstants.INPUT_TYPE_RADIO.equals(config.getInputType()));
        if (flag && StringUtils.isNotBlank(config.getDictType())) {
            List<SysDictData> dictDataList = sysDictTypeService.selectDictDataByType(config.getDictType());
            if (dictDataList.size() > 0) {
                config.setConfigValue(dictDataList.get(0).getDictValue());
            }
        }
        return toAjax(configService.insertConfig(config));
    }

    /**
     * 修改参数配置
     */
    @GetMapping("/edit/{configId}")
    public String edit(@PathVariable("configId") Long configId, ModelMap modelMap) {
        modelMap.put("config", configService.getById(configId));
        return PREFIX + "/edit";
    }

    /**
     * 修改保存参数配置
     */
    @ResponseBody
    @PostMapping("/edit")
    public AjaxResult<Object> editSave(@Validated SysConfig config) {
        if (Constants.CONFIG_KEY_NOT_UNIQUE.equals(configService.checkConfigKeyUnique(config))) {
            return error("修改参数'" + config.getConfigName() + "'失败，参数键名已存在");
        }
        return toAjax(configService.updateConfig(config));
    }

    /**
     * 代码生成快速配置窗口
     */
    @GetMapping("/edit/quick")
    public String quickConfig() {
        return PREFIX + "/quickConfig";
    }

    /**
     * 批量更新系统配置
     *
     * @return 结果
     */
    @ResponseBody
    @PostMapping("/edit/batch")
    @ApiOperation(value = "批量更新系统配置", notes = "批量更新系统配置")
    public AjaxResult<Object> updateConfig(HttpServletRequest request) {
        configService.updateConfig(MapDataUtil.convertDataMap(request));
        return success();
    }

    /**
     * 删除参数配置
     */
    @ResponseBody
    @PostMapping("/remove")
    public AjaxResult<Object> remove(String ids) {
        return toAjax(configService.deleteConfigByIds(ids));
    }

    /**
     * 清空缓存
     */
    @ResponseBody
    @GetMapping("/clearCache")
    public AjaxResult<Object> clearCache() {
        configService.clearCache();
        return success();
    }

    /**
     * 校验参数键名
     */
    @ResponseBody
    @PostMapping("/checkConfigKeyUnique")
    public String checkConfigKeyUnique(SysConfig config) {
        return configService.checkConfigKeyUnique(config);
    }

}
