package com.wangming.generator.support.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.wangming.generator.support.domain.SysConfig;

import java.util.Map;

/**
 * 系统配置 业务层
 *
 * @author wangming
 */
public interface ISysConfigService extends IService<SysConfig> {

    /**
     * 根据键名查询参数配置信息
     *
     * @param configKey 参数键名
     * @return 参数键值
     */
    String selectConfigByKey(String configKey);

    /**
     * 新增参数配置
     *
     * @param config 参数配置信息
     * @return 结果
     */
    int insertConfig(SysConfig config);

    /**
     * 批量更新参数配置
     *
     * @param configMap 配置信息
     */
    void updateConfig(Map<String, Object> configMap);

    /**
     * 修改参数配置
     *
     * @param config 参数配置信息
     * @return 结果
     */
    int updateConfig(SysConfig config);

    /**
     * 批量删除参数配置信息
     *
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    int deleteConfigByIds(String ids);

    /**
     * 清空缓存数据
     */
    void clearCache();

    /**
     * 校验参数键名是否唯一
     *
     * @param config 参数信息
     * @return 结果
     */
    String checkConfigKeyUnique(SysConfig config);

}
