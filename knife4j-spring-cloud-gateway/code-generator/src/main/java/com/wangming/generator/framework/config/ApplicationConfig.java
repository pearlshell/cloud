package com.wangming.generator.framework.config;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.wangming.generator.common.config.JacksonConfig;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.context.annotation.Primary;

/**
 * 程序注解配置
 * MapperScan 指定要扫描的Mapper类的包的路径
 * EnableAspectJAutoProxy 表示通过aop框架暴露该代理对象, AopContext能够访问
 *
 * @author wangming
 */
@Configuration
@EnableAspectJAutoProxy(exposeProxy = true)
public class ApplicationConfig {

    /**
     * 使用自定义的json处理器
     */
    @Bean
    @Primary
    public static ObjectMapper jacksonObjectMapper() {
        return JacksonConfig.jacksonObjectMapper();
    }

}
