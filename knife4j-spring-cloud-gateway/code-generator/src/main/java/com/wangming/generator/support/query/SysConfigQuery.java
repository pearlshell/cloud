package com.wangming.generator.support.query;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.wangming.generator.common.copycat.annotaion.Eq;
import com.wangming.generator.common.copycat.annotaion.Gt;
import com.wangming.generator.common.copycat.annotaion.Like;
import com.wangming.generator.common.copycat.annotaion.Lt;
import com.wangming.generator.common.copycat.query.AbstractQuery;
import com.wangming.generator.support.domain.SysConfig;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.util.Date;

/**
 * 系统配置相关
 *
 * @author wangming
 */
@Data
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "SysConfig", description = "系统配置相关")
public class SysConfigQuery extends AbstractQuery<SysConfig> {

    @Eq
    @ApiModelProperty("参数主键")
    private Long configId;

    @Like
    @ApiModelProperty("参数名称")
    private String configName;

    @Eq
    @ApiModelProperty("参数键名")
    private String configKey;

    @Like
    @ApiModelProperty("参数键值")
    private String configValue;

    @Eq
    @ApiModelProperty("系统内置（Y是 N否）")
    private String configType;

    @Gt(alias = "create_time")
    @ApiModelProperty("索引开始时间段（yyyy-MM-dd）")
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date beginTime;

    @Lt(alias = "create_time")
    @ApiModelProperty("索引结束时间段（yyyy-MM-dd）")
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date endTime;

}
