package com.wangming.generator.framework.web.service;

import com.wangming.generator.support.service.ISysConfigService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * <p>
 * html 调用 thymeleaf 实现参数管理
 * </p>
 *
 * 这是thymeleaf的官方文档中提到了${@myBean.doSomething()}可以访问容器中bean的数据。
 * 被spring管理的
 *
 * @author wangming
 * @date 2020-11-15 21:47
 */
@Service("config")
public class SysConfigService {

    @Resource
    private ISysConfigService configService;

    /**
     * 根据键名查询参数配置信息
     * @param configKey 参数键名
     * @return 参数键值
     */
    public String getKey(String configKey) {
        return configService.selectConfigByKey(configKey);
    }

}
