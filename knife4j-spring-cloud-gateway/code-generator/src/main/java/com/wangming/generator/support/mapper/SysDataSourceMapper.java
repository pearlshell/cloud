package com.wangming.generator.support.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.wangming.generator.support.domain.SysDataSource;

/**
 * 数据源 数据层
 *
 * @author wangming
 */
public interface SysDataSourceMapper extends BaseMapper<SysDataSource> {

}
