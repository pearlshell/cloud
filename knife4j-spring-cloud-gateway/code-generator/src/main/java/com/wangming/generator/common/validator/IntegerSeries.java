package com.wangming.generator.common.validator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @author wangming
 * @date 2020-07-09 13:57
 */
@Target({ElementType.FIELD, ElementType.PARAMETER})
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = IntegerSeriesValidator.class)
public @interface IntegerSeries {

    int[] list();

    String message() default "无效的数据";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};

}
