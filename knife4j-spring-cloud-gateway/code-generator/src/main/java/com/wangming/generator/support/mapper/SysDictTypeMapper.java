package com.wangming.generator.support.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.wangming.generator.support.domain.SysDictType;

/**
 * 字典表 数据层
 *
 * @author wangming
 */
public interface SysDictTypeMapper extends BaseMapper<SysDictType> {

}
