package com.wangming.generator.common.copycat.annotaion;

import com.baomidou.mybatisplus.core.enums.SqlLike;
import com.wangming.generator.common.copycat.enums.ColumnNamingStrategy;

import java.lang.annotation.*;

/**
 * 模糊查询(LIKE) 条件注解
 *
 * @author wangming
 * @date 2020-05-12 00:00:00
 * @see {@link com.wangming.generator.common.copycat.processor.LikeProcessor}
 */
@Documented
@CriteriaQuery
@Target({ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
public @interface Like {

    /**
     * 自定义的属性值
     */
    String alias() default "";

    /**
     * 匹配模式
     */
    SqlLike like() default SqlLike.DEFAULT;

    /**
     * 默认下划线
     *
     * @return ColumnNamingStrategy
     */
    ColumnNamingStrategy naming() default ColumnNamingStrategy.LOWER_CASE_UNDER_LINE;

}
