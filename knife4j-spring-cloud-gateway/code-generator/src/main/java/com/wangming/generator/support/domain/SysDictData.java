package com.wangming.generator.support.domain;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.wangming.generator.common.constant.Constants;
import com.wangming.generator.common.core.domain.BaseEntity;
import com.wangming.generator.common.utils.StringUtils;
import com.wangming.generator.common.validator.StringSeries;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

/**
 * 字典数据
 *
 * @author wangming
 */
@Data
@Accessors(chain = true)
@TableName("sys_dict_data")
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "DictData" , description = "字典数据")
public class SysDictData extends BaseEntity {

    /** 字典编码 */
    @ApiModelProperty("字典编码")
    @TableId(type = IdType.AUTO)
    private Long dictCode;

    /** 字典排序 */
    @ApiModelProperty("字典排序")
    private Long dictSort;

    /** 字典标签 */
    @ApiModelProperty("字典标签")
    @NotBlank(message = "字典标签不能为空")
    @Size(max = 100, message = "字典标签长度不能超过100个字符")
    private String dictLabel;

    /** 字典键值 */
    @ApiModelProperty("字典键值")
    @NotBlank(message = "字典键值不能为空")
    @Size(max = 100, message = "字典键值长度不能超过100个字符")
    private String dictValue;

    /** 字典类型 */
    @ApiModelProperty("字典类型")
    @NotBlank(message = "字典类型不能为空")
    @Size(max = 100, message = "字典类型长度不能超过100个字符")
    private String dictType;

    /** 样式属性（其他样式扩展） */
    @ApiModelProperty("样式属性（其他样式扩展）")
    @Size(max = 100, message = "样式属性长度不能超过100个字符")
    private String cssClass;

    /** 表格字典样式 */
    @ApiModelProperty("表格字典样式")
    private String listClass;

    /** 系統默认（Y是 N否） */
    @StringSeries(list = { "Y", "N" })
    @ApiModelProperty("系统默认（Y是 N否）")
    private String isDefault;

    /** 系统内置（Y是 N否） */
    @StringSeries(list = { "Y", "N" })
    @ApiModelProperty("系统内置（Y是 N否）")
    private String isBuiltIn;

    /** 状态（0正常 1停用） */
    @StringSeries(list = { "0", "1" })
    @ApiModelProperty("状态（0正常 1停用）")
    private String status;

    public boolean getDefault() {
        return Constants.YES.equals(this.isDefault);
    }

    public boolean isBuiltIn() {
        return isBuiltIn(this.isBuiltIn);
    }

    public boolean isBuiltIn(String isBuiltIn) {
        return isBuiltIn != null && StringUtils.equals(Constants.YES, isBuiltIn);
    }

}
