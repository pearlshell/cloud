package com.wangming.generator.framework.config;

import com.wangming.generator.common.exception.BusinessException;
import net.sf.ehcache.CacheManager;
import org.apache.commons.io.IOUtils;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;

/**
 * <p>
 * EhCache配置, 启用EhCache缓存
 * </p>
 *
 * @author wangming
 * @date 2020-11-14 11:01
 */
@Configuration
public class EhCacheConfig {

    /**
     * 缓存管理器 使用Ehcache实现
     */
    @Bean
    public CacheManager ehCacheManager() {
        return new CacheManager(getCacheManagerConfigFileInputStream());
    }

    /**
     * 返回配置文件流 避免ehcache配置文件一直被占用，无法完全销毁项目重新部署
     */
    protected InputStream getCacheManagerConfigFileInputStream() {
        InputStream inputStream = null;
        try {
            inputStream = this.getClass().getResourceAsStream("/ehcache.xml");
            byte[] b = IOUtils.toByteArray(inputStream);
            return new ByteArrayInputStream(b);
        } catch (IOException e) {
            throw new BusinessException("Unable to obtain input stream for cacheManagerConfigFile [ ehcache.xml ]", e);
        } finally {
            IOUtils.closeQuietly(inputStream);
        }
    }

}
