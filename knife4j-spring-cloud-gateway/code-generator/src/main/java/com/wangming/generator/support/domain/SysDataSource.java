package com.wangming.generator.support.domain;

import com.baomidou.mybatisplus.annotation.IdType;
import com.wangming.generator.common.validator.StringSeries;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.wangming.generator.common.core.domain.BaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * 系统数据源配置
 *
 * @author wangming
 */
@Data
@Accessors(chain = true)
@TableName("sys_data_source")
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "SysDataSource", description = "系统数据源配置")
public class SysDataSource extends BaseEntity {

    /** 数据源主键 */
    @TableId(type = IdType.AUTO)
    @ApiModelProperty("数据源主键")
    private Long id;
    /** 数据库名称 */
    @ApiModelProperty("数据库名称")
    private String name;
    /** 数据库类型 */
    @ApiModelProperty("数据库类型")
    private String dbType;
    /** oracle连接方式 */
    @ApiModelProperty("oracle连接方式")
    private String oracleConnMode;
    /** oracle连接服务名或SID */
    @ApiModelProperty("oracle连接服务名或SID")
    private String serviceNameOrSid;
    /** 主机地址 */
    @ApiModelProperty("主机地址")
    private String host;
    /** 端口号 */
    @ApiModelProperty("端口号")
    private Integer port;
    /** 用户名 */
    @ApiModelProperty("用户名")
    private String username;
    /** 连接密码 */
    @ApiModelProperty("连接密码")
    private String password;
    /** 状态（0正常 1停用） */
    @StringSeries(list = { "0", "1" })
    @ApiModelProperty("状态（0正常 1停用）")
    private String status;

}
