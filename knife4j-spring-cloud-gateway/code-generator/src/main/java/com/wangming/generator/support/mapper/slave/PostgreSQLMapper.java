package com.wangming.generator.support.mapper.slave;

import com.wangming.generator.support.service.BaseService;

/**
 * PostgreSQL数据源 数据层
 *
 * @author wangming
 */
public interface PostgreSQLMapper extends BaseService {

}