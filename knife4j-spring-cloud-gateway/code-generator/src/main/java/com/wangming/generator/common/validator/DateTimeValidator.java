package com.wangming.generator.common.validator;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.text.ParseException;
import java.text.SimpleDateFormat;

/**
 * @author wangming
 * @date 2020-11-25 13:58
 */
public class DateTimeValidator implements ConstraintValidator<DateTime, String> {

    private DateTime dateTime;

    /**
     * 初始化 可以获得当前注解的所有属性
     */
    @Override
    public void initialize(DateTime dateTime) {
        this.dateTime = dateTime;
    }

    /**
     * 进行约束验证的主体方法
     *
     * @param s       标识验证参数的具体实例
     * @param context 约束执行的上下文环境
     * @return
     */
    @Override
    public boolean isValid(String s, ConstraintValidatorContext context) {
        if (s == null) {
            return true;
        }
        // 获取定义的格式
        String format = dateTime.format();

        if (s.length() != format.length()) {
            return false;
        }

        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(format);
        try {
            simpleDateFormat.parse(s);
        } catch (ParseException e) {
            e.printStackTrace();
            return false;
        }
        return true;

    }

}
