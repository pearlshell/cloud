package com.wangming.generator.support.domain;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.wangming.generator.common.constant.Constants;
import com.wangming.generator.common.core.domain.BaseEntity;
import com.wangming.generator.common.utils.StringUtils;
import com.wangming.generator.common.validator.StringSeries;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

/**
 * 系统配置相关
 *
 * @author wangming
 */
@Data
@Accessors(chain = true)
@TableName("sys_config")
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "SysConfig", description = "系统配置相关")
public class SysConfig extends BaseEntity {

    /** 参数主键 */
    @TableId(type = IdType.AUTO)
    @ApiModelProperty("参数主键")
    private Long configId;

    /** 参数名称 */
    @NotBlank(message = "参数名称不能为空")
    @Size(max = 100, message = "参数名称不能超过100个字符")
    @ApiModelProperty("参数名称")
    private String configName;

    /** 参数键名 */
    @NotBlank(message = "参数键名长度不能为空")
    @Size(max = 100, message = "参数键名长度不能超过100个字符")
    @ApiModelProperty("参数键名")
    private String configKey;

    /** 参数键值 */
    @Size(max = 500, message = "参数键值长度不能超过500个字符")
    @ApiModelProperty("参数键值")
    private String configValue;

    /** 系统内置（Y是 N否） */
    @StringSeries(list = { "Y", "N" })
    @ApiModelProperty("系统内置（Y是 N否）")
    private String isBuiltIn;

    /** 关联的字典类型值 */
    @ApiModelProperty("关联的字典类型值")
    private String dictType;

    /** 输入的类型 */
    @StringSeries(list = { "0", "1", "2" })
    @ApiModelProperty("输入的类型")
    private String inputType;

    public boolean isBuiltIn() {
        return isBuiltIn(this.isBuiltIn);
    }

    public boolean isBuiltIn(String isBuiltIn) {
        return isBuiltIn != null && StringUtils.equals(Constants.YES, isBuiltIn);
    }

}
