package com.wangming.generator.common.constant;

/**
 * <p>
 * 参数配置常量
 * </p>
 *
 * @author wangming
 * @date 2020-11-25 11:51
 */
public class ConfigConstants {

    /** 文本 */
    public static final String INPUT_TYPE_TEXT = "0";
    /** 单选 */
    public static final String INPUT_TYPE_RADIO = "1";
    /** 下拉框 */
    public static final String INPUT_TYPE_SELECT = "2";

}
