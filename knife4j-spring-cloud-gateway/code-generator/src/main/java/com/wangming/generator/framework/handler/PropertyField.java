package com.wangming.generator.framework.handler;

/**
 * <p>
 * 公共属性字段
 * </p>
 *
 * @author wangming
 * @date 2020-11-04 14:33
 */
public class PropertyField {

    public static final String CREATE_BY = "createBy";
    public static final String CREATE_TIME = "createTime";
    public static final String UPDATE_BY = "updateBy";
    public static final String UPDATE_TIME = "updateTime";

}
