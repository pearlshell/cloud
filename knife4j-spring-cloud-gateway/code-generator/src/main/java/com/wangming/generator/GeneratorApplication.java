package com.wangming.generator;

import org.mybatis.spring.annotation.MapperScan;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;

/**
 * 启动程序
 *
 * @author wangming
 */
@SpringBootApplication(exclude = {DataSourceAutoConfiguration.class})
@MapperScan(basePackages = {"com.wangming.generator.support.mapper", "com.wangming.generator.support.mapper.slave"})
public class GeneratorApplication {

    private static final Logger log = LoggerFactory.getLogger(GeneratorApplication.class);

    public static void main(String[] args) {
        SpringApplication.run(GeneratorApplication.class, args);
        log.info("代码生成器启动成功!");
    }

}