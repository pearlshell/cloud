package com.wangming.generator.support.query;

import com.baomidou.mybatisplus.annotation.TableId;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.wangming.generator.common.copycat.annotaion.Eq;
import com.wangming.generator.common.copycat.annotaion.Gt;
import com.wangming.generator.common.copycat.annotaion.Like;
import com.wangming.generator.common.copycat.annotaion.Lt;
import com.wangming.generator.common.copycat.query.AbstractQuery;
import com.wangming.generator.support.domain.SysDataSource;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.Date;

/**
 * <p>
 * 系统数据源查询参数
 * </p>
 *
 * @author wangming
 * @date 2020-11-07 16:48
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "SysDataSourceQuery", description = "系统数据源查询参数")
public class SysDataSourceQuery extends AbstractQuery<SysDataSource> {

    @Eq
    @ApiModelProperty("数据源主键")
    private Long id;

    @Like
    @ApiModelProperty("数据库名称")
    private String name;

    @Eq
    @ApiModelProperty("数据库类型")
    private String dbType;

    @Eq
    @ApiModelProperty("状态（0正常 1停用）")
    private String status;

    @Gt(alias = "create_time")
    @ApiModelProperty("开始时间段（yyyy-MM-dd）")
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date beginTime;

    @Lt(alias = "create_time")
    @ApiModelProperty("结束时间段（yyyy-MM-dd）")
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date endTime;

}
