package com.wangming.generator.support.controller;

import com.wangming.generator.common.constant.Constants;
import com.wangming.generator.common.core.controller.BaseController;
import com.wangming.generator.common.core.domain.AjaxResult;
import com.wangming.generator.common.core.domain.Ztree;
import com.wangming.generator.common.core.page.TableDataInfo;
import com.wangming.generator.common.exception.DemoModeException;
import com.wangming.generator.common.utils.text.Convert;
import com.wangming.generator.support.domain.SysDictType;
import com.wangming.generator.support.query.DictTypeQuery;
import com.wangming.generator.support.service.ISysDictTypeService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.Arrays;
import java.util.List;

/**
 * 字典类型信息
 *
 * @author wangming
 */
@Controller
@RequestMapping("/system/dict")
@Api(value = "字典类型信息", tags = "字典类型信息")
public class DictTypeController extends BaseController {

    private static final String PREFIX = "system/dict/type";

    @Resource
    private ISysDictTypeService dictTypeService;

    @GetMapping()
    public String dictType() {
        return PREFIX + "/type";
    }

    /**
     * 分页条件查询获取字典类型列表
     *
     * @param typeQuery 查询条件
     * @return 分页数据
     */
    @ResponseBody
    @PostMapping("/list")
    @ApiOperation(value = "分页条件查询获取字典类型列表", notes = "分页条件查询获取字典类型列表")
    public TableDataInfo<SysDictType> list(DictTypeQuery typeQuery) {
        return toPageResult(dictTypeService.selectDictTypeList(typeQuery));
    }

    /**
     * 新增字典类型
     */
    @GetMapping("/add")
    public String add() {
        return PREFIX + "/add";
    }

    /**
     * 新增保存字典类型
     */
    @ResponseBody
    @PostMapping("/add")
    @ApiOperation(value = "新增保存字典类型", notes = "新增保存字典类型")
    public AjaxResult<Object> addSave(@Validated SysDictType dictType) {
        if (Constants.DICT_TYPE_NOT_UNIQUE.equals(dictTypeService.checkDictTypeUnique(dictType))) {
            return error("新增字典'" + dictType.getDictName() + "'失败，字典类型已存在");
        }
        return toAjax(dictTypeService.insertDictType(dictType));
    }

    /**
     * 修改字典类型
     */
    @GetMapping("/edit/{dictId}")
    public String edit(@PathVariable("dictId") Long dictId, ModelMap modelMap) {
        modelMap.put("dict", dictTypeService.getById(dictId));
        return PREFIX + "/edit";
    }

    /**
     * 修改保存字典类型
     */
    @ResponseBody
    @PostMapping("/edit")
    @ApiOperation(value = "修改保存字典类型", notes = "修改保存字典类型")
    public AjaxResult<Object> editSave(@Validated SysDictType dictType) {
        if (Constants.DICT_TYPE_NOT_UNIQUE.equals(dictTypeService.checkDictTypeUnique(dictType))) {
            return error("修改字典'" + dictType.getDictName() + "'失败，字典类型已存在");
        }
        return toAjax(dictTypeService.updateDictType(dictType));
    }

    /**
     * 清空缓存
     */
    @ResponseBody
    @GetMapping("/clearCache")
    public AjaxResult<Object> clearCache() {
        dictTypeService.clearCache();
        return success();
    }

    /**
     * 查询字典详细
     */
    @GetMapping("/detail/{dictId}")
    public String detail(@PathVariable("dictId") Long dictId, ModelMap modelMap) {
        modelMap.put("dict", dictTypeService.getById(dictId));
        modelMap.put("dictList", dictTypeService.selectDictTypeAll());
        return "system/dict/data/data";
    }

    /**
     * 校验字典类型
     */
    @ResponseBody
    @PostMapping("/checkDictTypeUnique")
    @ApiOperation(value = "校验字典类型", notes = "校验字典类型")
    public String checkDictTypeUnique(SysDictType dictType) {
        return dictTypeService.checkDictTypeUnique(dictType);
    }

    /**
     * 选择字典树
     */
    @GetMapping("/selectDictTree/{columnId}/{dictType}")
    public String selectDeptTree(@PathVariable("columnId") Long columnId, @PathVariable String dictType, ModelMap modelMap) {
        modelMap.put("columnId", columnId);
        modelMap.put("dict", dictTypeService.selectDictTypeByType(dictType));
        return PREFIX + "/tree";
    }

    /**
     * 加载字典列表树
     */
    @ResponseBody
    @GetMapping("/treeData")
    @ApiOperation(value = "加载字典列表树", notes = "加载字典列表树")
    public List<Ztree> treeData() {
        return dictTypeService.selectDictTree(new DictTypeQuery());
    }

    /**
     * 删除字典类型（多个用逗号分隔）
     *
     * @param ids 数据id
     * @return 结果
     */
    @ResponseBody
    @PostMapping("/remove")
    @ApiOperation(value = "删除字典类型（多个用逗号分隔）", notes = "删除字典类型（多个用逗号分隔）")
    public AjaxResult<Object> remove(String ids) {
        List<Long> list = Arrays.asList(Convert.toLongArray(ids));
        return toAjax(dictTypeService.deleteDictTypeByIds(list));
    }

}
