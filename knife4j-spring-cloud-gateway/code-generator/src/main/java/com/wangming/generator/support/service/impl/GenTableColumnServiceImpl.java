package com.wangming.generator.support.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.wangming.generator.support.domain.GenTableColumn;
import com.wangming.generator.support.mapper.GenTableColumnMapper;
import com.wangming.generator.support.service.IGenTableColumnService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * 业务字段 服务层实现
 *
 * @author wangming
 */
@Service
public class GenTableColumnServiceImpl extends ServiceImpl<GenTableColumnMapper, GenTableColumn> implements IGenTableColumnService {

    /**
     * 根据业务主键获取业务字段列表
     *
     * @param tableId 业务表主键
     * @return 业务字段集合
     */
    @Override
    public List<GenTableColumn> selectGenTableColumnListByTableId(long tableId) {
        LambdaQueryWrapper<GenTableColumn> queryWrapper = Wrappers.lambdaQuery();
        queryWrapper.eq(GenTableColumn::getTableId, tableId)
                .orderByAsc(GenTableColumn::getSort);
        return this.baseMapper.selectList(queryWrapper);
    }

    /**
     * 根据业务主键批量删除业务字段
     *
     * @param tableIds 业务表主键
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public void deleteGenTableColumnByTableId(List<Long> tableIds) {
        for (Long tableId : tableIds) {
            LambdaQueryWrapper<GenTableColumn> queryWrapper = Wrappers.lambdaQuery();
            queryWrapper.eq(GenTableColumn::getTableId, tableId);
            this.baseMapper.delete(queryWrapper);
        }
    }

}