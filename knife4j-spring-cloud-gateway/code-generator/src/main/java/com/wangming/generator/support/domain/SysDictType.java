package com.wangming.generator.support.domain;

import com.baomidou.mybatisplus.annotation.IdType;
import com.wangming.generator.common.validator.StringSeries;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.wangming.generator.common.core.domain.BaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

/**
 * 字典类型
 *
 * @author wangming
 */
@Data
@Accessors(chain = true)
@TableName("sys_dict_type")
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "DictType", description = "字典类型")
public class SysDictType extends BaseEntity {

    /** 字典主键 */
    @TableId(type = IdType.AUTO)
    @ApiModelProperty("字典主键")
    private Long dictId;

    /** 字典名称 */
    @ApiModelProperty("字典名称")
    @NotBlank(message = "字典名称不能为空")
    @Size(max = 100, message = "字典类型名称长度不能超过100个字符")
    private String dictName;

    /** 字典类型 */
    @ApiModelProperty("字典类型")
    @NotBlank(message = "字典类型不能为空")
    @Size(max = 100, message = "字典类型类型长度不能超过100个字符")
    private String dictType;

    /** 状态（0正常 1停用） */
    @StringSeries(list = { "0", "1" })
    @ApiModelProperty("状态（0正常 1停用）")
    private String status;

}
