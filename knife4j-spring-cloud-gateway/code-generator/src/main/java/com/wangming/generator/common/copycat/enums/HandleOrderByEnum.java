package com.wangming.generator.common.copycat.enums;

/**
 * 排序类型
 *
 * @author wangming
 * @date 2020/09/119
 * @since 1.1.0
 */
public enum HandleOrderByEnum {

    /**
     * 静态
     */
    STATIC,
    /**
     * 动态
     */
    DYNAMIC

}
