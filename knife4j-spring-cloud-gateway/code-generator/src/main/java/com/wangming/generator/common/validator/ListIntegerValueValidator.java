package com.wangming.generator.common.validator;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.List;

/**
 * @author wangming
 * @date 2020-11-25 13:58
 */
public class ListIntegerValueValidator implements ConstraintValidator<ListIntegerValue, List<Integer>> {

    private ListIntegerValue listIntegerValue;

    /**
     * 初始化 可以获得当前注解的所有属性
     */
    @Override
    public void initialize(ListIntegerValue listIntegerValue) {
        this.listIntegerValue = listIntegerValue;
    }

    /**
     * 进行约束验证的主体方法
     *
     * @param integerList 标识验证参数的具体实例
     * @param context     约束执行的上下文环境
     * @return
     */
    @Override
    public boolean isValid(List<Integer> integerList, ConstraintValidatorContext context) {
        if (integerList == null) {
            return true;
        }

        try {
            for (Integer i : integerList) {
                if ((i >= listIntegerValue.min() && i <= listIntegerValue.max()) || i == listIntegerValue.regular()) {
                    continue;
                }else{
                    return false;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }

        return true;
    }

}
