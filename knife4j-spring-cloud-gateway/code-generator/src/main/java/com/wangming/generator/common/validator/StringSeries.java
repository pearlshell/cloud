package com.wangming.generator.common.validator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @author wangming
 * @date 2020-11-25 13:57
 */
@Target({ElementType.FIELD, ElementType.PARAMETER})
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = StringSeriesValidator.class)
public @interface StringSeries {

    String[] list();

    String message() default "无效的数据";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};

}
