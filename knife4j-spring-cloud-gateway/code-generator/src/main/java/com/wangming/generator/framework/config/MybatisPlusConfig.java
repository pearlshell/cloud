package com.wangming.generator.framework.config;

import com.github.pagehelper.PageInterceptor;
import com.baomidou.mybatisplus.autoconfigure.ConfigurationCustomizer;
import com.baomidou.mybatisplus.core.injector.DefaultSqlInjector;
import com.baomidou.mybatisplus.core.injector.ISqlInjector;
import com.baomidou.mybatisplus.extension.plugins.PaginationInterceptor;
import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.Properties;

/**
 * <p>
 * mybatis-plus 配置类
 * </p>
 *
 * @author wangming
 * @date 2020-09-28 11:55
 */
@Data
@Configuration
@ConfigurationProperties(prefix = "pagehelper")
public class MybatisPlusConfig {

    private String helperDialect;
    private String reasonable;
    private String supportMethodsArguments;
    private String params;
    private String autoRuntimeDialect;

    /**
     * mybatis-plus sql 注入器
     */
    @Bean
    public ISqlInjector sqlInjector() {
        return new DefaultSqlInjector();
    }

    /**
     * mybatis-plus默认分页与PageHelper分页支持
     */
    @Bean
    public ConfigurationCustomizer mybatisConfigurationCustomizer() {
        PageInterceptor pageInterceptor = new PageInterceptor();
        Properties properties = new Properties();
        // 默认数据库方言
        properties.setProperty("helperDialect", helperDialect);
        // 页码<=0 查询第一页，页码>=总页数查询最后一页
        properties.setProperty("reasonable", reasonable);
        // 支持通过 Mapper 接口参数来传递分页参数
        properties.setProperty("supportMethodsArguments", supportMethodsArguments);
        // 对象或者Map中发现了countSql属性，就会作为count参数使用
        properties.setProperty("params", params);
        // 多数据源自动识别对应方言的分页 ，默认为false
        properties.setProperty("autoRuntimeDialect", autoRuntimeDialect);
        pageInterceptor.setProperties(properties);
        return configuration -> {
            configuration.addInterceptor(pageInterceptor);
            // configuration.addInterceptor(new PaginationInterceptor());
        };
    }

}
