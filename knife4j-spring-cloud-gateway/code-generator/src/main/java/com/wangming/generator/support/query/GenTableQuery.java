package com.wangming.generator.support.query;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.wangming.generator.common.copycat.annotaion.Eq;
import com.wangming.generator.common.copycat.annotaion.Gt;
import com.wangming.generator.common.copycat.annotaion.Like;
import com.wangming.generator.common.copycat.annotaion.Lt;
import com.wangming.generator.common.copycat.query.AbstractQuery;
import com.wangming.generator.support.domain.GenTable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.Date;

/**
 * <p>
 * 代码生成业务表查询参数
 * </p>
 *
 * @author wangming
 * @date 2020-11-07 18:07
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "GenTableQuery", description = "代码生成业务表查询参数")
public class GenTableQuery extends AbstractQuery<GenTable> {

    @Eq
    @ApiModelProperty("主键")
    private Long tableId;

    @Eq
    @ApiModelProperty("数据源主键")
    private Long dataSourceId;

    @Eq
    @ApiModelProperty("数据源类型")
    private String dbType;

    @Like
    @ApiModelProperty("表名称")
    private String tableName;

    @Like
    @ApiModelProperty("表描述")
    private String tableComment;


    @Gt(alias = "create_time")
    @ApiModelProperty("开始时间段（yyyy-MM-dd）")
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date beginTime;

    @Lt(alias = "create_time")
    @ApiModelProperty("结束时间段（yyyy-MM-dd）")
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date endTime;

}
