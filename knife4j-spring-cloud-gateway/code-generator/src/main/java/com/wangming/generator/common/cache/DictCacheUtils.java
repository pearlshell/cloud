package com.wangming.generator.common.cache;

import com.google.common.collect.Lists;
import com.wangming.generator.common.utils.StringUtils;
import com.wangming.generator.support.domain.SysDictData;

import java.util.List;

/**
 * <p>
 * 字典缓存工具类
 * </p>
 *
 * @author wangming
 * @date 2020-11-15 21:22
 */
public class DictCacheUtils {

    /** 字典管理 cache name */
    public static final String SYS_DICT_CACHE = "sys-dict";
    /** 字典管理 cache key */
    public static final String SYS_DICT_KEY = "sys_dict:";
    /** 分隔符 */
    public static final String SEPARATOR = ",";

    /**
     * 设置字典缓存
     *
     * @param key       参数键
     * @param dictDataList 字典数据列表
     */
    public static void setDictCache(String key, List<SysDictData> dictDataList) {
        CacheUtils.put(getCacheName(), getCacheKey(key), dictDataList);
    }

    /**
     * 获取字典缓存
     *
     * @param key 参数键
     * @return dictDataList 字典数据列表
     */
    public static List<SysDictData> getDictCache(String key) {
        Object cacheObj = CacheUtils.get(getCacheName(), getCacheKey(key));
        if (StringUtils.isNotNull(cacheObj)) {
            return StringUtils.cast(cacheObj);
        } else {
            return Lists.newArrayList();
        }
    }

    /**
     * 根据字典类型和字典值获取字典标签
     *
     * @param dictType  字典类型
     * @param dictValue 字典值
     * @return 字典标签
     */
    public static String getDictLabel(String dictType, String dictValue) {
        return getDictLabel(dictType, dictValue, SEPARATOR);
    }

    /**
     * 根据字典类型和字典标签获取字典值
     *
     * @param dictType  字典类型
     * @param dictLabel 字典标签
     * @return 字典值
     */
    public static String getDictValue(String dictType, String dictLabel) {
        return getDictValue(dictType, dictLabel, SEPARATOR);
    }

    /**
     * 根据字典类型和字典值获取字典标签
     *
     * @param dictType  字典类型
     * @param dictValue 字典值
     * @param separator 分隔符
     * @return 字典标签
     */
    public static String getDictLabel(String dictType, String dictValue, String separator) {
        StringBuilder propertyString = new StringBuilder();
        List<SysDictData> dataList = getDictCache(dictType);
        if (StringUtils.containsAny(separator, dictValue) && StringUtils.isNotEmpty(dataList)) {
            for (SysDictData dict : dataList) {
                for (String value : dictValue.split(separator)) {
                    if (value.equals(dict.getDictValue())) {
                        propertyString.append(dict.getDictLabel()).append(separator);
                        break;
                    }
                }
            }
        } else {
            for (SysDictData dict : dataList) {
                if (dictValue.equals(dict.getDictValue())) {
                    return dict.getDictLabel();
                }
            }
        }
        return StringUtils.stripEnd(propertyString.toString(), separator);
    }

    /**
     * 根据字典类型和字典标签获取字典值
     *
     * @param dictType  字典类型
     * @param dictLabel 字典标签
     * @param separator 分隔符
     * @return 字典值
     */
    public static String getDictValue(String dictType, String dictLabel, String separator) {
        StringBuilder propertyString = new StringBuilder();
        List<SysDictData> dataList = getDictCache(dictType);
        if (StringUtils.containsAny(separator, dictLabel) && StringUtils.isNotEmpty(dataList)) {
            for (SysDictData dict : dataList) {
                for (String label : dictLabel.split(separator)) {
                    if (label.equals(dict.getDictLabel())) {
                        propertyString.append(dict.getDictValue()).append(separator);
                        break;
                    }
                }
            }
        } else {
            for (SysDictData dict : dataList) {
                if (dictLabel.equals(dict.getDictLabel())) {
                    return dict.getDictValue();
                }
            }
        }
        return StringUtils.stripEnd(propertyString.toString(), separator);
    }

    /**
     * 清空字典缓存
     */
    public static void clearDictCache() {
        CacheUtils.removeAll(getCacheName());
    }

    /**
     * 获取 cache name
     *
     * @return 缓存名
     */
    public static String getCacheName() {
        return SYS_DICT_CACHE;
    }

    /**
     * 设置 cache key
     *
     * @param configKey 参数键
     * @return 缓存键key
     */
    public static String getCacheKey(String configKey) {
        return SYS_DICT_KEY + configKey;
    }

}
