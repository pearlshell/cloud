package com.wangming.generator.support.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;

/**
 * @author wangming
 */
public class WindowsRegistry {

    private static Logger logger = LoggerFactory.getLogger(WindowsRegistry.class);

    /**
     * @param location path in the registry
     * @param key      registry key
     * @return registry value or null if not found
     */
    public static String readRegistry(String location, String key) {
        try {
            // Run reg query, then read output with StreamReader (internal class)
            Process process = Runtime.getRuntime().exec("reg query " +
                    '"' + location + "\" " + key);
            StreamReader reader = new StreamReader(process.getInputStream());
            reader.start();
            process.waitFor();
            reader.join();
            String output = reader.getResult();

            // Output has the following format:
            // \n<Version information>\n\n<key>\t<registry type>\t<value>
            if (output.contains("\r\n")) {
                String[] parsed = output.split("\r\n");
                return parsed[parsed.length - 1];
            }
            return output.replaceAll("\r|\n", "");
        } catch (Exception e) {
            return null;
        }

    }

    static class StreamReader extends Thread {
        private final InputStream is;
        private final StringWriter sw = new StringWriter();

        public StreamReader(InputStream is) {
            this.is = is;
        }

        @Override
        public void run() {
            try {
                int c;
                while ((c = is.read()) != -1) {
                    sw.write(c);
                }
            } catch (IOException ignored) {
            }
        }

        public String getResult() {
            return sw.toString();
        }
    }

    public static void main(String[] args) {
        logger.info(readRegistry("HKEY_CURRENT_USER\\Software\\Microsoft\\Windows\\CurrentVersion\\App Paths\\chrome.exe", "/ve"));
    }

}
