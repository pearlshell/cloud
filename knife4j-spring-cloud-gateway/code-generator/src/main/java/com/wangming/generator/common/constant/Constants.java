package com.wangming.generator.common.constant;

/**
 * 通用常量信息
 *
 * @author wangming
 */
public class Constants {

    /** UTF-8 字符集 */
    public static final String UTF8 = "UTF-8";

    /** 是 */
    public static final String YES = "Y";
    /** 否 */
    public static final String NO = "N";

    /** 默认用户 */
    public static final String DEFAULT_USER = "admin";
    /** 演示标识符 */
    public static final Long DEMO_TAG = 0L;

    /** 通用成功标识 */
    public static final String SUCCESS = "0";
    /** 通用失败标识 */
    public static final String FAIL = "1";

    /** 登录成功 */
    public static final String LOGIN_SUCCESS = "Success";
    /** 注销 */
    public static final String LOGOUT = "Logout";
    /** 登录失败 */
    public static final String LOGIN_FAIL = "Error";

    /** 字典类型是否唯一的返回结果码 */
    public static final String DICT_TYPE_UNIQUE = "0";
    public static final String DICT_TYPE_NOT_UNIQUE = "1";

    /** 参数键名是否唯一的返回结果码 */
    public static final String CONFIG_KEY_UNIQUE = "0";
    public static final String CONFIG_KEY_NOT_UNIQUE = "1";

    /** 字典正常状态 */
    public static final String DICT_NORMAL = "0";

    /** 数据库类型-MySQL */
    public static final String DATABASE_TYPE_MYSQL = "mysql";
    /** 数据库类型-Oracle */
    public static final String DATABASE_TYPE_ORACLE = "oracle";
    /** 数据库类型-PostgreSQL */
    public static final String DATABASE_TYPE_POSTGRESQL = "pgsql";
    /** 数据库类型-DB2 */
    public static final String DATABASE_TYPE_DB2 = "db2";
    /** 数据库类型-SQL Server */
    public static final String DATABASE_TYPE_SQLSERVER = "sqlserver";

    /** Oracle数据库连接类型-SID */
    public static final String ORACLE_CONN_TYPE_SID = "sid";
    /** Oracle数据库连接类型-服务名 */
    public static final String ORACLE_CONN_TYPE_SERVICE_NAME = "service_name";

}
