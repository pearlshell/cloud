package com.wangming.generator.framework.datasource;

import com.alibaba.druid.pool.DruidDataSource;
import com.wangming.generator.common.utils.spring.SpringUtils;

import java.util.HashMap;
import java.util.Map;

/**
 * 动态数据源工具
 *
 * @author wangming
 */
public class DynamicDataSourceUtil {

    /**
     * 其他数据源
     */
    protected static Map<Object, Object> dataSourceMap = new HashMap<>();

    public static void flushDataSource() {
        // 获取spring管理的dynamicDataSource
        DynamicDataSource dynamicDataSource = SpringUtils.getBean("dynamicDataSource");
        // 将数据源设置到 targetDataSources
        dynamicDataSource.setTargetDataSources(dataSourceMap);
        // 将 targetDataSources 中的连接信息放入 resolvedDataSources 管理
        dynamicDataSource.afterPropertiesSet();
    }

    /**
     * 删除数据源
     */
    public static void deleteTargetDataSource(Object key) {
        dataSourceMap.remove(key);
    }

    /**
     * 替换数据源
     */
    public static void replaceTargetDataSource(Object key, DruidDataSource dataSource) {
        dataSourceMap.replace(key, dataSource);
    }

    /**
     * 添加数据源
     */
    public static void addTargetDataSource(Object key, DruidDataSource dataSource) {
        dataSourceMap.put(key, dataSource);
    }

    /**
     * 设置数据源
     */
    public static void setTargetDataSources(Map<Object, Object> targetDataSources) {
        dataSourceMap = targetDataSources;
    }

}