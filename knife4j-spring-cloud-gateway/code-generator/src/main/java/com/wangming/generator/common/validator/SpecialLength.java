package com.wangming.generator.common.validator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * SpecialLength
 *
 * @author wangming
 * @date 2020-11-25 14:41:21
 */
@Target({ElementType.FIELD, ElementType.PARAMETER})
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = SpecialLengthValidator.class)
public @interface SpecialLength {

    int len();

    String message() default "数据长度超出限制";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};

}
