package com.wangming.generator.support.controller;

import com.wangming.generator.common.core.controller.BaseController;
import com.wangming.generator.common.core.domain.AjaxResult;
import com.wangming.generator.common.core.page.TableDataInfo;
import com.wangming.generator.common.exception.DemoModeException;
import com.wangming.generator.common.utils.text.Convert;
import com.wangming.generator.support.domain.SysDictData;
import com.wangming.generator.support.query.DictDataQuery;
import com.wangming.generator.support.service.ISysDictDataService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.Arrays;
import java.util.List;

/**
 * 数据字典信息
 *
 * @author wangming
 */
@Controller
@RequestMapping("/system/dict/data")
@Api(value = "数据字典信息", tags = "数据字典信息")
public class DictDataController extends BaseController {

    private static final String PREFIX = "system/dict/data";

    @Resource
    private ISysDictDataService dictDataService;

    @GetMapping()
    public String dictData() {
        return PREFIX + "/data";
    }

    /**
     * 分页条件查询获取字典数据列表
     *
     * @param dataQuery 查询条件
     * @return 分页数据
     */
    @ResponseBody
    @PostMapping("/list")
    @ApiOperation(value = "分页条件查询获取字典数据列表", notes = "分页条件查询获取字典数据列表")
    public TableDataInfo<SysDictData> list(DictDataQuery dataQuery) {
        return toPageResult(dictDataService.selectDictDataList(dataQuery));
    }

    /**
     * 新增字典数据
     */
    @GetMapping("/add/{dictType}")
    public String add(@PathVariable("dictType") String dictType, ModelMap modelMap) {
        modelMap.put("dictType", dictType);
        return PREFIX + "/add";
    }

    /**
     * 新增保存字典数据
     */
    @ResponseBody
    @PostMapping("/add")
    @ApiOperation(value = "新增保存字典数据", notes = "新增保存字典数据")
    public AjaxResult<Object> addSave(@Validated SysDictData dictData) {
        return toAjax(dictDataService.insertDictData(dictData));
    }

    /**
     * 修改字典数据
     */
    @GetMapping("/edit/{dictCode}")
    public String edit(@PathVariable("dictCode") Long dictCode, ModelMap modelMap) {
        modelMap.put("dict", dictDataService.getById(dictCode));
        return PREFIX + "/edit";
    }

    /**
     * 修改保存字典数据
     */
    @ResponseBody
    @PostMapping("/edit")
    @ApiOperation(value = "修改保存字典数据", notes = "修改保存字典数据")
    public AjaxResult<Object> editSave(@Validated SysDictData dictData) {
        return toAjax(dictDataService.updateDictData(dictData));
    }

    /**
     * 删除字典数据（多个用逗号分隔）
     *
     * @param ids 数据id
     * @return 结果
     */
    @ResponseBody
    @PostMapping("/remove")
    @ApiOperation(value = "删除字典数据（多个用逗号分隔）", notes = "删除字典数据（多个用逗号分隔）")
    public AjaxResult<Object> remove(String ids) {
        List<Long> list = Arrays.asList(Convert.toLongArray(ids));
        return toAjax(dictDataService.deleteDictDataByIds(list));
    }

}