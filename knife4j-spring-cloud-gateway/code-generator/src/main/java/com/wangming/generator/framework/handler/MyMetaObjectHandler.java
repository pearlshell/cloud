package com.wangming.generator.framework.handler;

import com.baomidou.mybatisplus.core.handlers.MetaObjectHandler;
import com.wangming.generator.common.constant.Constants;
import org.apache.ibatis.reflection.MetaObject;
import org.springframework.stereotype.Component;

import java.util.Date;

/**
 * <p>
 * mybatis-puls 自定义自动填充功能实现类
 * </p>
 *
 * @author wangming
 * @date 2020-10-28 15:52
 */
@Component
public class MyMetaObjectHandler implements MetaObjectHandler {

    /**
     * 插入元对象字段填充（用于插入时对公共字段的填充）
     *
     * @param metaObject 元对象
     */
    @Override
    public void insertFill(MetaObject metaObject) {
        this.setFieldValByName(PropertyField.CREATE_BY, Constants.DEFAULT_USER, metaObject);
        this.setFieldValByName(PropertyField.CREATE_TIME, new Date(), metaObject);
    }

    /**
     * 更新元对象字段填充（用于更新时对公共字段的填充）
     *
     * @param metaObject 元对象
     */
    @Override
    public void updateFill(MetaObject metaObject) {
        this.setFieldValByName(PropertyField.UPDATE_BY, Constants.DEFAULT_USER, metaObject);
        this.setFieldValByName(PropertyField.UPDATE_TIME, new Date(), metaObject);
    }

}
