package com.wangming.generator.common.validator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @author wangming
 * @date 2020-11-25 13:57
 */
@Target({ElementType.FIELD, ElementType.PARAMETER})
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = ListStringValueValidator.class)
public @interface ListStringValue {

    String[] list();

    String message() default "不在要求范围之内";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};

}
