package com.wangming.generator.common.copycat.time;

import com.wangming.generator.common.copycat.util.TimeUtils;

import java.time.LocalDateTime;

/**
 * 默认的时间处理器-处理为 {@link LocalDateTime} 对象
 *
 * @author wangming
 * @date 2020-09-20 00:00:00
 * @since 1.2.0
 */
public class LocalDateTimeProcessor implements TimeProcessor<LocalDateTime> {

    @Override
    public boolean supports(Class<?> clazz) {
        return LocalDateTime.class.equals(clazz);
    }

    @Override
    public LocalDateTime handleTime(Long timeStamp, Class<?> clazz) {
        return TimeUtils.timestampToLocalDateTime(timeStamp);
    }

}
