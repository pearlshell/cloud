package com.wangming.generator.support.query;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.wangming.generator.common.copycat.annotaion.Eq;
import com.wangming.generator.common.copycat.annotaion.Gt;
import com.wangming.generator.common.copycat.annotaion.Like;
import com.wangming.generator.common.copycat.annotaion.Lt;
import com.wangming.generator.common.copycat.query.AbstractQuery;
import com.wangming.generator.support.domain.SysDictType;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.Date;

/**
 * <p>
 * 字典类型查询参数
 * </p>
 *
 * @author wangming
 * @date 2020-11-07 13:15
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "DictTypeQuery", description = "字典类型查询参数")
public class DictTypeQuery extends AbstractQuery<SysDictType> {

    @Eq
    @ApiModelProperty("字典主键")
    private Long dictId;

    @Like
    @ApiModelProperty("字典名称")
    private String dictName;

    @Eq
    @ApiModelProperty("字典类型")
    private String dictType;

    @Eq
    @ApiModelProperty("状态（0正常 1停用）")
    private String status;

    @Gt(alias = "create_time")
    @ApiModelProperty("开始时间段（yyyy-MM-dd）")
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date beginTime;

    @Lt(alias = "create_time")
    @ApiModelProperty("结束时间段（yyyy-MM-dd）")
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date endTime;

}
