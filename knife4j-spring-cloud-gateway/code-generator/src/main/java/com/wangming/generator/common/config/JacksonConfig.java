package com.wangming.generator.common.config;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.*;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import org.apache.commons.lang3.StringUtils;

import java.io.IOException;
import java.sql.Timestamp;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;

/**
 * <p>
 * Jackson配置
 * </p>
 *
 * @author wangming
 * @date 2020-11-08 19:40
 */
public class JacksonConfig {

    /**
     * ObjectMapper相关配置
     */
    public static ObjectMapper jacksonObjectMapper() {

        // 初始化全局Jackson 序列化工具
        ObjectMapper objectMapper = new ObjectMapper();

        // 忽略未知属性
        objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);

        // 忽略无法序列化的属性
        objectMapper.configure(SerializationFeature.FAIL_ON_EMPTY_BEANS, true);

        // 序列化时忽略空值字段
        objectMapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);

        return objectMapper;
    }

}
