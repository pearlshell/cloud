package com.wangming.generator.support.mapper.slave;

import com.wangming.generator.support.service.BaseService;

/**
 * MySQL数据源 数据层
 *
 * @author wangming
 */
public interface MySQLMapper extends BaseService {

}