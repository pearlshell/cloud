package com.wangming.generator.common.core.domain;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.google.common.collect.Maps;
import com.wangming.generator.common.utils.bean.BeanUtils;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;
import java.util.Map;

/**
 * Entity基类
 *
 * @author wangming
 */
@Data
public class BaseEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty("创建人")
    @TableField(value = "create_by", fill = FieldFill.INSERT)
    private String createBy;

    @ApiModelProperty("创建时间")
    @TableField(value = "create_time", fill = FieldFill.INSERT)
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date createTime;

    @ApiModelProperty("修改人")
    @TableField(value = "update_by", fill = FieldFill.UPDATE)
    private String updateBy;

    @ApiModelProperty("修改时间")
    @TableField(value = "update_time", fill = FieldFill.UPDATE)
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date updateTime;

    @ApiModelProperty("备注")
    private String remark;

    @ApiModelProperty("请求参数")
    @TableField(exist = false)
    private Map<String, Object> params;

    /**
     * 默认
     */
    public Map<String, Object> getParams() {
        return params != null ? params : Maps.newHashMap();
    }

    /**
     * 根据Class泛型对象生成一个目标对象，并将源自己的属性值复制到目标对象中
     *
     * @param target 目标对象class
     * @param <T>    目标对象泛型
     * @return 复制属性值后的目标对象
     */
    public <T> T copyTo(Class<T> target) {
        return BeanUtils.copyTo(this, target);
    }

}
