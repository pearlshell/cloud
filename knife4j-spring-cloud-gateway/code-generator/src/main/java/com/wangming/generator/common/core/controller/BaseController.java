package com.wangming.generator.common.core.controller;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.wangming.generator.common.utils.DateUtils;
import com.wangming.generator.common.utils.StringUtils;
import com.wangming.generator.common.utils.sql.SqlUtil;
import com.wangming.generator.common.core.domain.AjaxResult;
import com.wangming.generator.common.core.domain.AjaxResult.Type;
import com.wangming.generator.common.core.page.PageDomain;
import com.wangming.generator.common.core.page.TableDataInfo;
import com.wangming.generator.common.core.page.TableSupport;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;

import java.beans.PropertyEditorSupport;
import java.util.Date;
import java.util.List;

/**
 * web层通用数据处理
 *
 * @author wangming
 */
public class BaseController {

    /**
     * 将前台传递过来的日期格式的字符串，自动转化为Date类型
     */
    @InitBinder
    public void initBinder(WebDataBinder binder) {
        // Date 类型转换
        binder.registerCustomEditor(Date.class, new PropertyEditorSupport() {
            @Override
            public void setAsText(String text) {
                setValue(DateUtils.parseDate(text));
            }
        });
    }

    /**
     * 设置请求分页数据
     */
    protected void startPage() {
        PageDomain pageDomain = TableSupport.buildPageRequest();
        Integer pageNum = pageDomain.getPageNum();
        Integer pageSize = pageDomain.getPageSize();
        if (StringUtils.isNotNull(pageNum) && StringUtils.isNotNull(pageSize)) {
            String orderBy = SqlUtil.escapeOrderBySql(pageDomain.getOrderBy());
            PageHelper.startPage(pageNum, pageSize, orderBy);
        }
    }

    /**
     * 响应请求分页数据
     */
    @SuppressWarnings({"rawtypes", "unchecked"})
    protected TableDataInfo getDataTable(List<?> list) {
        TableDataInfo rspData = new TableDataInfo();
        rspData.setCode(0);
        rspData.setRows(list);
        rspData.setTotal(new PageInfo(list).getTotal());
        return rspData;
    }

    /**
     * 处理返回分页数据
     */
    protected <T> TableDataInfo<T> toPageResult(List<T> records) {
        TableDataInfo<T> tableInfo = new TableDataInfo<>();
        tableInfo.setCode(0);
        PageInfo<T> pageInfo = new PageInfo<>(records);
        tableInfo.setRows(pageInfo.getList());
        tableInfo.setPageNum(pageInfo.getPageNum());
        tableInfo.setPageSize(pageInfo.getPageSize());
        tableInfo.setTotal(pageInfo.getTotal());
        tableInfo.setTotalPage(pageInfo.getPages());
        return tableInfo;
    }

    /**
     * 响应返回结果
     *
     * @param rows 影响行数
     * @return 操作结果
     */
    protected <T> AjaxResult<T> toAjax(int rows) {
        return rows > 0 ? success() : error();
    }

    /**
     * 响应返回结果
     *
     * @param result 结果
     * @return 操作结果
     */
    protected <T> AjaxResult<T> toAjax(boolean result) {
        return result ? success() : error();
    }

    /**
     * 返回成功
     */
    public <T> AjaxResult<T> success() {
        return AjaxResult.success();
    }

    /**
     * 返回失败消息
     */
    public <T> AjaxResult<T> error() {
        return AjaxResult.error();
    }

    /**
     * 返回成功消息
     */
    public <T> AjaxResult<T> success(String message) {
        return AjaxResult.success(message);
    }

    /**
     * 返回失败消息
     */
    public <T> AjaxResult<T> error(String message) {
        return AjaxResult.error(message);
    }

    /**
     * 返回错误码消息
     */
    public <T> AjaxResult<T> error(Type type, String message) {
        return new AjaxResult<>(type, message);
    }

    /**
     * 页面跳转
     */
    public String redirect(String url) {
        return StringUtils.format("redirect:{}", url);
    }

}
