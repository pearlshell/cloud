package com.wangming.generator.support.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.wangming.generator.support.domain.GenTable;

import java.util.List;

/**
 * 业务 数据层
 *
 * @author wangming
 */
public interface GenTableMapper extends BaseMapper<GenTable> {

    /**
     * 根据表名称获取业务表结构（包含列）
     *
     * @param tableName 表名称
     * @return 业务信息
     */
    GenTable selectGenTableByName(String tableName);

    /**
     * 根据业务id获取业务表结构（包含列）
     *
     * @param id 业务ID
     * @return 业务信息
     */
    GenTable selectGenTableById(Long id);

    /**
     * 获取指定数据源所有表结构（包含列）
     *
     * @param dataSourceId 数据源主键
     * @return 表信息集合
     */
    List<GenTable> selectGenTableAll(Long dataSourceId);

}