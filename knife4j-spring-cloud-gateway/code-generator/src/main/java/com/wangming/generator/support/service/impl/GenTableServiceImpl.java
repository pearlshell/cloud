package com.wangming.generator.support.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.wangming.generator.common.cache.GenCacheUtils;
import com.wangming.generator.common.config.datasource.DynamicDataSourceContextHolder;
import com.wangming.generator.common.constant.Constants;
import com.wangming.generator.common.constant.GenConstants;
import com.wangming.generator.common.exception.BusinessException;
import com.wangming.generator.common.utils.StringUtils;
import com.wangming.generator.common.utils.file.FileUtils;
import com.wangming.generator.common.utils.text.CharsetKit;
import com.wangming.generator.common.utils.text.Convert;
import com.wangming.generator.framework.aspectj.lang.enums.DataSourceType;
import com.wangming.generator.support.domain.GenTable;
import com.wangming.generator.support.domain.GenTableColumn;
import com.wangming.generator.support.domain.SysDataSource;
import com.wangming.generator.support.mapper.GenTableMapper;
import com.wangming.generator.support.mapper.slave.MySQLMapper;
import com.wangming.generator.support.mapper.slave.OracleMapper;
import com.wangming.generator.support.mapper.slave.PostgreSQLMapper;
import com.wangming.generator.support.mapper.slave.SQLServerMapper;
import com.wangming.generator.support.query.GenTableQuery;
import com.wangming.generator.support.service.BaseService;
import com.wangming.generator.support.service.IGenTableColumnService;
import com.wangming.generator.support.service.IGenTableService;
import com.wangming.generator.support.service.ISysDataSourceService;
import com.wangming.generator.support.util.GenUtils;
import com.wangming.generator.support.util.VelocityInitializer;
import com.wangming.generator.support.util.VelocityUtils;
import org.apache.commons.io.IOUtils;
import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.Velocity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionAspectSupport;

import javax.annotation.Resource;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.StringWriter;
import java.nio.charset.StandardCharsets;
import java.util.*;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

/**
 * 业务 服务层实现
 *
 * @author wangming
 */
@Service
public class GenTableServiceImpl extends ServiceImpl<GenTableMapper, GenTable> implements IGenTableService {

    @Resource
    private MySQLMapper mysqlMapper;
    @Resource
    private OracleMapper oracleMapper;
    @Resource
    private PostgreSQLMapper postgresqlMapper;
    @Resource
    private SQLServerMapper sqlserverMapper;
    @Resource
    private ISysDataSourceService dataSourceService;
    @Resource
    private IGenTableColumnService tableColumnService;

    /**
     * 根据业务id获取业务表结构（包含列）
     *
     * @param id 业务ID
     * @return 业务信息
     */
    @Override
    public GenTable selectGenTableById(Long id) {
        GenTable genTable = this.baseMapper.selectGenTableById(id);
        setTableFromOptions(genTable);
        return genTable;
    }

    /**
     * 根据数据库类型获取对应的mapper
     *
     * @param dbType 数据库类型
     */
    private BaseService getSlaveMapper(String dbType) {
        if (Constants.DATABASE_TYPE_MYSQL.equals(dbType)) {
            return mysqlMapper;
        } else if (Constants.DATABASE_TYPE_ORACLE.equals(dbType)) {
            return oracleMapper;
        } else if (Constants.DATABASE_TYPE_SQLSERVER.equals(dbType)) {
            return sqlserverMapper;
        } else if (Constants.DATABASE_TYPE_POSTGRESQL.equals(dbType)) {
            return postgresqlMapper;
        }
        return mysqlMapper;
    }

    /**
     * 根据表名获取列
     *
     * @param dataSourceId 数据源主键
     * @param tableName    表名
     */
    private List<GenTableColumn> selectDbTableColumnsByName(Long dataSourceId, String tableName) {
        SysDataSource dataSource = dataSourceService.getById(dataSourceId);
        try {
            // 切换数据源
            DynamicDataSourceContextHolder.setDataSourceType(DataSourceType.SLAVE.name() + Convert.toStr(dataSource.getId()));
            return getSlaveMapper(dataSource.getDbType()).selectDbTableColumnsByName(tableName);
        } catch (Exception e) {
            // 有异常不抛出，返回空集合，防止无法回滚
            return new ArrayList<>();
        } finally {
            DynamicDataSourceContextHolder.clearDataSourceType();
        }
    }

    /**
     * 查询据库列表
     *
     * @param tableQuery 业务信息
     * @return 数据库表集合
     */
    @Override
    public List<GenTable> selectDbTableList(GenTableQuery tableQuery) {
        try {
            SysDataSource dataSource = dataSourceService.getById(tableQuery.getDataSourceId());

            //切换数据源
            DynamicDataSourceContextHolder.setDataSourceType(DataSourceType.SLAVE.name() + Convert.toStr(dataSource.getId()));
            return getSlaveMapper(dataSource.getDbType()).selectDbTableList(tableQuery);
        } finally {
            DynamicDataSourceContextHolder.clearDataSourceType();
        }
    }

    /**
     * 查询据库列表
     *
     * @param tableNames   表名称组
     * @param dataSourceId 数据源主键
     * @return 数据库表集合
     */
    @Override
    public List<GenTable> selectDbTableListByNames(String[] tableNames, Long dataSourceId) {
        SysDataSource dataSource = dataSourceService.getById(dataSourceId);
        try {
            //切换数据源
            DynamicDataSourceContextHolder.setDataSourceType(DataSourceType.SLAVE.name() + Convert.toStr(dataSource.getId()));
            return getSlaveMapper(dataSource.getDbType()).selectDbTableListByNames(tableNames);
        } finally {
            DynamicDataSourceContextHolder.clearDataSourceType();
        }
    }

    /**
     * 获取指定数据源所有表结构（包含列）
     *
     * @param dataSourceId 数据源主键
     * @return 表信息集合
     */
    @Override
    public List<GenTable> selectGenTableAll(Long dataSourceId) {
        return this.baseMapper.selectGenTableAll(dataSourceId);
    }

    /**
     * 修改业务
     *
     * @param genTable 业务信息
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public void updateGenTable(GenTable genTable) {
        String options = JSON.toJSONString(genTable.getParams());
        genTable.setOptions(options);
        int row = this.baseMapper.updateById(genTable);
        if (row > 0) {
            tableColumnService.updateBatchById(genTable.getColumns());
        }
    }

    /**
     * 删除业务对象
     *
     * @param ids 需要删除的数据ID
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public void deleteGenTableByIds(List<Long> ids) {
        this.baseMapper.deleteBatchIds(ids);
        tableColumnService.deleteGenTableColumnByTableId(ids);
    }

    /**
     * 导入表结构
     *
     * @param tableList    导入表列表
     * @param dataSourceId 数据源主键
     */
    @Override
    public void importGenTable(List<GenTable> tableList, Long dataSourceId) {
        SysDataSource dataSource = dataSourceService.getById(dataSourceId);
        for (GenTable table : tableList) {
            try {
                String tableName = table.getTableName();
                GenUtils.initTable(table);
                table.setDataSourceId(dataSourceId);
                int row = this.baseMapper.insert(table);
                if (row > 0) {
                    List<GenTableColumn> dbTableColumns = selectDbTableColumnsByName(dataSource.getId(), tableName);
                    for (GenTableColumn column : dbTableColumns) {
                        GenUtils.initColumnField(column, table);
                    }
                    tableColumnService.saveBatch(dbTableColumns);
                }
            } catch (Exception e) {
                log.error("表名 " + table.getTableName() + " 导入失败：", e);
            }
        }
    }

    /**
     * 预览代码
     *
     * @param tableId 表编号
     * @return 预览数据列表
     */
    @Override
    public Map<String, String> previewCode(Long tableId) {
        Map<String, String> dataMap = new LinkedHashMap<>();
        // 查询表信息
        GenTable table = this.baseMapper.selectGenTableById(tableId);
        // 获取模板列表
        List<String> templates = getTemplateList(table);
        VelocityContext context = VelocityUtils.prepareContext(table);
        for (String template : templates) {
            // 渲染模板
            StringWriter sw = new StringWriter();
            Template tpl = Velocity.getTemplate(template, Constants.UTF8);
            tpl.merge(context, sw);
            // 获取模版标题名
            String templateName = template.substring(template.lastIndexOf("/") + 1).replace(".vm", "");
            dataMap.put(templateName, sw.toString());
        }
        return dataMap;
    }

    /**
     * 生成代码（下载方式）
     *
     * @param tableName 表名称
     * @return 数据
     */
    @Override
    public byte[] downloadCode(String tableName) {
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        ZipOutputStream zip = new ZipOutputStream(outputStream);
        generatorCode(tableName, zip);
        IOUtils.closeQuietly(zip);
        return outputStream.toByteArray();
    }

    /**
     * 生成代码（自定义路径）
     *
     * @param tableName 表名称
     */
    @Override
    public void generatorCode(String tableName) {
        // 查询表信息
        GenTable table = this.baseMapper.selectGenTableByName(tableName);
        // 获取模板列表并填充字段信息
        List<String> templates = getTemplateList(table);
        // 给模板导入数据
        VelocityContext context = VelocityUtils.prepareContext(table);
        for (String template : templates) {
            if (!StringUtils.contains(template, "sql.vm")) {
                // 渲染模板
                StringWriter sw = new StringWriter();
                Template tpl = Velocity.getTemplate(template, Constants.UTF8);
                tpl.merge(context, sw);
                try {
                    String path = getGenPath(table, template);
                    FileUtils.writeStringToFile(new File(path), sw.toString(), CharsetKit.UTF_8);
                } catch (IOException e) {
                    throw new BusinessException("渲染模板失败，表名：" + table.getTableName());
                }
            }
        }
    }

    /**
     * 从数据库同步更新表结构
     *
     * @param tableName 表名称
     */
    @Override
    public void syncDb(String tableName) {
        try {
            // 从当前系统获取表业务信息
            LambdaQueryWrapper<GenTable> queryWrapper = Wrappers.lambdaQuery();
            queryWrapper.eq(GenTable::getTableName, tableName);
            GenTable table = this.baseMapper.selectOne(queryWrapper);
            // 移除表字段
            tableColumnService.deleteGenTableColumnByTableId(Collections.singletonList(table.getTableId()));
            // 从数据源获取表业务信息
            SysDataSource dataSource = dataSourceService.getById(table.getDataSourceId());
            List<GenTableColumn> dbTableColumns = selectDbTableColumnsByName(dataSource.getId(), tableName);
            for (GenTableColumn column : dbTableColumns) {
                GenUtils.initColumnField(column, table);
            }
            // 重新添加表字段
            tableColumnService.saveBatch(dbTableColumns);
        } catch (Exception e) {
            // 发生异常手动回滚
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            throw new BusinessException("数据源或表结构获取失败！请检查数据源与表名是否有效！", e);
        }
    }

    /**
     * 批量生成代码
     *
     * @param tableNames 表数组
     * @return 数据
     */
    @Override
    public byte[] downloadCode(String[] tableNames) {
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        ZipOutputStream zip = new ZipOutputStream(outputStream);
        for (String tableName : tableNames) {
            generatorCode(tableName, zip);
        }
        IOUtils.closeQuietly(zip);
        return outputStream.toByteArray();
    }

    /**
     * 查询表信息并生成代码
     */
    private void generatorCode(String tableName, ZipOutputStream zip) {
        // 查询表信息
        GenTable table = this.baseMapper.selectGenTableByName(tableName);
        // 获取模板列表
        List<String> templates = getTemplateList(table);
        //给模板配置数据
        VelocityContext context = VelocityUtils.prepareContext(table);
        for (String template : templates) {
            // 渲染模板
            StringWriter sw = new StringWriter();
            Template tpl = Velocity.getTemplate(template, Constants.UTF8);
            tpl.merge(context, sw);
            try {
                // 添加到zip
                zip.putNextEntry(new ZipEntry(VelocityUtils.getFileName(template, table)));
                IOUtils.write(sw.toString(), zip, StandardCharsets.UTF_8);
                IOUtils.closeQuietly(sw);
                zip.flush();
                zip.closeEntry();
            } catch (IOException e) {
                log.error("渲染模板失败，表名：" + table.getTableName(), e);
            }
        }
    }

    /**
     * 修改保存参数校验
     *
     * @param genTable 业务信息
     */
    @Override
    public void validateEdit(GenTable genTable) {
        if (GenConstants.TPL_TREE.equals(genTable.getTplCategory())) {
            String options = JSON.toJSONString(genTable.getParams());
            JSONObject paramsObj = JSONObject.parseObject(options);
            if (StringUtils.isEmpty(paramsObj.getString(GenConstants.TREE_CODE))) {
                throw new BusinessException("树编码字段不能为空");
            } else if (StringUtils.isEmpty(paramsObj.getString(GenConstants.TREE_PARENT_CODE))) {
                throw new BusinessException("树父编码字段不能为空");
            } else if (StringUtils.isEmpty(paramsObj.getString(GenConstants.TREE_NAME))) {
                throw new BusinessException("树名称字段不能为空");
            }
        } else if (GenConstants.TPL_SUB.equals(genTable.getTplCategory())) {
            if (StringUtils.isEmpty(genTable.getSubTableName())) {
                throw new BusinessException("关联子表的表名不能为空");
            } else if (StringUtils.isEmpty(genTable.getSubTableFkName())) {
                throw new BusinessException("子表关联的外键名不能为空");
            }
        }
    }

    /**
     * 设置主键列信息
     *
     * @param table 业务表信息
     */
    public void setPkColumn(GenTable table) {
        //循环表的字段集合找到主键
        for (GenTableColumn column : table.getColumns()) {
            if (column.isPk()) {
                table.setPkColumn(column);
                break;
            }
        }
        //判断主键是否为空，如果为空就将第一字段放入主键集合
        if (StringUtils.isNull(table.getPkColumn())) {
            table.setPkColumn(table.getColumns().get(0));
        }
        //判断类型是不是主子表（增删改查）
        if (GenConstants.TPL_SUB.equals(table.getTplCategory())) {
            for (GenTableColumn column : table.getSubTable().getColumns()) {
                if (column.isPk()) {
                    table.getSubTable().setPkColumn(column);
                    break;
                }
            }
            if (StringUtils.isNull(table.getSubTable().getPkColumn())) {
                table.getSubTable().setPkColumn(table.getSubTable().getColumns().get(0));
            }
        }
    }

    /**
     * 获取代码生成地址
     *
     * @param table    业务表信息
     * @param template 模板文件路径
     * @return 生成地址
     */
    public static String getGenPath(GenTable table, String template) {
        String genPath = table.getGenPath();
        if (StringUtils.equals(genPath, "/")) {
            return System.getProperty("user.dir") + File.separator + "src" + File.separator + VelocityUtils.getFileName(template, table);
        }
        return genPath + File.separator + VelocityUtils.getFileName(template, table);
    }

    /**
     * 根据业务表获取模版列表
     *
     * @return 模版列表
     */
    private List<String> getTemplateList(GenTable table) {
        // 设置主子表信息
        setSubTable(table);
        // 设置主键列信息
        setPkColumn(table);
        //初始化模板
        VelocityInitializer.initVelocity();
        String genTemplate = GenCacheUtils.getGenTemplate();
        //返回模板集合
        return VelocityUtils.getTemplateList(table.getTplCategory(), genTemplate);
    }

    /**
     * 设置主子表信息
     *
     * @param table 业务表信息
     */
    public void setSubTable(GenTable table) {
        String subTableName = table.getSubTableName();
        if (StringUtils.isNotEmpty(subTableName)) {
            table.setSubTable(this.baseMapper.selectGenTableByName(subTableName));
        }
    }

    /**
     * 设置代码生成其他选项值
     *
     * @param genTable 设置后的生成对象
     */
    public void setTableFromOptions(GenTable genTable) {
        JSONObject paramsObj = JSONObject.parseObject(genTable.getOptions());
        if (StringUtils.isNotNull(paramsObj)) {
            String treeCode = paramsObj.getString(GenConstants.TREE_CODE);
            String treeParentCode = paramsObj.getString(GenConstants.TREE_PARENT_CODE);
            String treeName = paramsObj.getString(GenConstants.TREE_NAME);
            String parentMenuId = paramsObj.getString(GenConstants.PARENT_MENU_ID);
            String parentMenuName = paramsObj.getString(GenConstants.PARENT_MENU_NAME);

            genTable.setTreeCode(treeCode);
            genTable.setTreeParentCode(treeParentCode);
            genTable.setTreeName(treeName);
            genTable.setParentMenuId(parentMenuId);
            genTable.setParentMenuName(parentMenuName);
        }
    }

}