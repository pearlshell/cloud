package com.wangming.generator.support.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.wangming.generator.support.domain.SysDictData;
import com.wangming.generator.support.query.DictDataQuery;

import java.util.List;

/**
 * 字典 业务层
 *
 * @author wangming
 */
public interface ISysDictDataService extends IService<SysDictData> {

    /**
     * 根据条件分页查询字典数据
     *
     * @param dataQuery 字典数据信息
     * @return 字典数据集合信息
     */
    List<SysDictData> selectDictDataList(DictDataQuery dataQuery);

    /**
     * 根据字典类型查询字典数据
     *
     * @param dictType 字典类型
     * @return 字典数据集合信息
     */
    List<SysDictData> selectDictDataByType(String dictType);

    /**
     * 根据字典类型和字典键值查询字典数据信息
     *
     * @param dictType  字典类型
     * @param dictValue 字典键值
     * @return 字典标签
     */
    String selectDictLabel(String dictType, String dictValue);

    /**
     * 根据字典数据ID查询信息
     *
     * @param dictCode 字典数据ID
     * @return 字典数据
     */
    SysDictData selectDictDataById(Long dictCode);

    /**
     * 批量删除字典数据
     *
     * @param ids 需要删除的数据
     * @return 结果
     */
    int deleteDictDataByIds(List<Long> ids);

    /**
     * 新增保存字典数据信息
     *
     * @param dictData 字典数据信息
     * @return 结果
     */
    int insertDictData(SysDictData dictData);

    /**
     * 修改保存字典数据信息
     *
     * @param dictData 字典数据信息
     * @return 结果
     */
    int updateDictData(SysDictData dictData);

    /**
     * 根据字典类型获取字典数据数量
     *
     * @param dictType 字典类型
     * @return 字典数据
     */
    int countDictDataByType(String dictType);

    /**
     * 同步修改字典数据类型
     *
     * @param oldDictType 旧字典类型
     * @param newDictType 新旧字典类型
     * @return 结果
     */
    int updateDictDataType(String oldDictType, String newDictType);

}
