package com.wangming.generator.common.copycat.processor;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.wangming.generator.common.copycat.annotaion.ConditionProcessor;
import com.wangming.generator.common.copycat.annotaion.Le;
import com.wangming.generator.common.copycat.query.AbstractQuery;
import org.springframework.util.StringUtils;

import java.lang.reflect.Field;

/**
 * {@link Le} 注解处理器
 *
 * @param <QUERY>  自定义查询 Query
 * @param <ENTITY> 查询想对应的实体类型
 * @author wangming
 * @date 2020-05-12 00:00:00
 */
@ConditionProcessor(targetAnnotation = Le.class)
public class LeProcessor<QUERY extends AbstractQuery, ENTITY>
        extends CriteriaAnnotationProcessorAdaptor<Le, QUERY, QueryWrapper<ENTITY>, ENTITY> {

    @Override
    public boolean process(QueryWrapper<ENTITY> queryWrapper, Field field, QUERY query, Le criteriaAnnotation) {

        final Object value = this.columnValue(field, query);
        if (this.isNullOrEmpty(value)) {
            // 属性值为 Null OR Empty 跳过
            return true;
        }

        String columnName = criteriaAnnotation.alias();
        if (StringUtils.isEmpty(columnName)) {
            columnName = this.columnName(field, criteriaAnnotation.naming());
        }
        assert columnName != null;
        queryWrapper.le(null != value, columnName, value);

        return true;
    }

}
