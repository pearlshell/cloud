package com.wangming.generator.support.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.wangming.generator.common.cache.DictCacheUtils;
import com.wangming.generator.common.constant.Constants;
import com.wangming.generator.common.exception.BusinessException;
import com.wangming.generator.common.utils.text.Convert;
import com.wangming.generator.support.domain.SysConfig;
import com.wangming.generator.support.domain.SysDictData;
import com.wangming.generator.support.mapper.SysDictDataMapper;
import com.wangming.generator.support.query.DictDataQuery;
import com.wangming.generator.support.service.ISysDictDataService;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 字典 业务层处理
 *
 * @author wangming
 */
@Service
public class SysDictDataServiceImpl extends ServiceImpl<SysDictDataMapper, SysDictData> implements ISysDictDataService {

    /**
     * 根据条件分页查询字典数据
     *
     * @param dataQuery 字典数据信息
     * @return 字典数据集合信息
     */
    @Override
    public List<SysDictData> selectDictDataList(DictDataQuery dataQuery) {
        return this.list(dataQuery.autoPageWrapper());
    }

    /**
     * 根据字典类型查询字典数据
     *
     * @param dictType 字典类型
     * @return 字典数据集合信息
     */
    @Override
    public List<SysDictData> selectDictDataByType(String dictType) {
        LambdaQueryWrapper<SysDictData> queryWrapper = Wrappers.lambdaQuery();
        queryWrapper.eq(SysDictData::getStatus, Constants.DICT_NORMAL);
        queryWrapper.eq(SysDictData::getDictType, dictType);
        return this.list(queryWrapper);
    }

    /**
     * 根据字典类型和字典键值查询字典数据信息
     *
     * @param dictType  字典类型
     * @param dictValue 字典键值
     * @return 字典标签
     */
    @Override
    public String selectDictLabel(String dictType, String dictValue) {
        LambdaQueryWrapper<SysDictData> queryWrapper = Wrappers.lambdaQuery();
        queryWrapper.eq(SysDictData::getDictType, dictType).eq(SysDictData::getDictValue, dictValue);
        return this.getOne(queryWrapper).getDictLabel();
    }

    /**
     * 根据字典数据ID查询信息
     *
     * @param dictCode 字典数据ID
     * @return 字典数据
     */
    @Override
    public SysDictData selectDictDataById(Long dictCode) {
        return this.getById(dictCode);
    }

    /**
     * 批量删除字典数据
     *
     * @param ids 需要删除的数据
     * @return 结果
     */
    @Override
    public int deleteDictDataByIds(List<Long> ids) {
        for (Long dataCode : ids) {
            SysDictData dictData = this.getById(dataCode);
            if (dictData.isBuiltIn()) {
                throw new BusinessException(String.format("内置参数【%1$s】不能删除 ", dictData.getDictLabel()));
            }
        }
        int row = this.baseMapper.deleteBatchIds(ids);
        if (row > 0) {
            DictCacheUtils.clearDictCache();
        }
        return row;
    }

    /**
     * 新增保存字典数据信息
     *
     * @param dictData 字典数据信息
     * @return 结果
     */
    @Override
    public int insertDictData(SysDictData dictData) {
        dictData.setIsBuiltIn(Constants.NO);
        int row = this.baseMapper.insert(dictData);
        if (row > 0) {
            DictCacheUtils.clearDictCache();
        }
        return row;
    }

    /**
     * 修改保存字典数据信息
     *
     * @param dictData 字典数据信息
     * @return 结果
     */
    @Override
    public int updateDictData(SysDictData dictData) {
        SysDictData template = this.getById(dictData.getDictCode());
        if (template.isBuiltIn()) {
            throw new BusinessException(String.format("内置参数【%1$s】不能修改 ", template.getDictLabel()));
        }
        int row = this.baseMapper.updateById(dictData);
        if (row > 0) {
            DictCacheUtils.clearDictCache();
        }
        return row;
    }

    /**
     * 根据字典类型获取字典数据数量
     *
     * @param dictType 字典类型
     * @return 字典数据
     */
    @Override
    public int countDictDataByType(String dictType) {
        LambdaQueryWrapper<SysDictData> queryWrapper = Wrappers.lambdaQuery();
        queryWrapper.eq(SysDictData::getDictType, dictType);
        return this.count(queryWrapper);
    }

    /**
     * 同步修改字典数据类型
     *
     * @param oldDictType 旧字典类型
     * @param newDictType 新旧字典类型
     * @return 结果
     */
    @Override
    public int updateDictDataType(String oldDictType, String newDictType) {
        LambdaQueryWrapper<SysDictData> queryWrapper = Wrappers.lambdaQuery();
        queryWrapper.eq(SysDictData::getDictType, oldDictType);
        return this.baseMapper.update(new SysDictData().setDictType(newDictType), queryWrapper);
    }

}
