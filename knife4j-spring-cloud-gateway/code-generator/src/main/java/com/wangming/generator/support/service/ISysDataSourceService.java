package com.wangming.generator.support.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.wangming.generator.support.domain.SysDataSource;

/**
 * 系统数据源 业务层
 *
 * @author wangming
 */
public interface ISysDataSourceService extends IService<SysDataSource> {

}
