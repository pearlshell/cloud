package com.wangming.generator.support.query;

import com.wangming.generator.common.copycat.annotaion.Eq;
import com.wangming.generator.common.copycat.annotaion.Like;
import com.wangming.generator.common.copycat.query.AbstractQuery;
import com.wangming.generator.support.domain.SysDictData;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 字典数据查询参数
 * </p>
 *
 * @author wangming
 * @date 2020-11-07 13:26
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "DictDataQuery", description = "字典数据查询参数")
public class DictDataQuery extends AbstractQuery<SysDictData> {

    @Eq
    @ApiModelProperty("字典编码")
    private Long dictCode;

    @Like
    @ApiModelProperty("字典标签")
    private String dictLabel;

    @Eq
    @ApiModelProperty("字典类型")
    private String dictType;

    @Eq
    @ApiModelProperty("是否默认（Y是 N否）")
    private String isDefault;

    @Eq
    @ApiModelProperty("状态（0正常 1停用）")
    private String status;

}
