package com.wangming.generator.support.service;

import com.wangming.generator.support.domain.GenTable;
import com.wangming.generator.support.domain.GenTableColumn;
import com.wangming.generator.support.query.GenTableQuery;

import java.util.List;

/**
 * 从库数据源 数据层
 *
 * @author wangming
 */
public interface BaseService {

    /**
     * 查询据库列表
     *
     * @param tableQuery 业务信息
     * @return 数据库表集合
     */
    List<GenTable> selectDbTableList(GenTableQuery tableQuery);

    /**
     * 查询据库列表
     *
     * @param tableNames 表名称组
     * @return 数据库表集合
     */
    List<GenTable> selectDbTableListByNames(String[] tableNames);

    /**
     * 根据表名称查询列信息
     *
     * @param tableName 表名称
     * @return 列信息
     */
    List<GenTableColumn> selectDbTableColumnsByName(String tableName);

}