package com.wangming.generator.support.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.wangming.generator.support.domain.GenTableColumn;

import java.util.List;

/**
 * 业务字段 服务层
 *
 * @author wangming
 */
public interface IGenTableColumnService extends IService<GenTableColumn> {

    /**
     * 根据业务主键获取业务字段列表
     *
     * @param tableId 业务表主键
     * @return 业务字段集合
     */
    List<GenTableColumn> selectGenTableColumnListByTableId(long tableId);

    /**
     * 根据业务主键批量删除业务字段
     *
     * @param tableIds 业务表主键
     */
    void deleteGenTableColumnByTableId(List<Long> tableIds);

}
