package com.wangming.generator.common.config;

import com.wangming.generator.common.utils.StringUtils;
import com.wangming.generator.common.utils.spring.SpringUtils;
import lombok.Data;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * 全局配置类
 *
 * @author wangming
 * @date 2020-11-15 20:22:12
 */
@Data
@Component
@ConfigurationProperties(prefix = "system")
public class Global {

    /** 项目名称 */
    @Value("${system.name}")
    private String nameGlobal;
    /** 版本 */
    @Value("${system.version}")
    private String versionGlobal;
    /** 版权年份 */
    @Value("${system.copyrightYear}")
    private String copyrightYearGlobal;
    /** 上传路径 */
    @Value("${system.profile}")
    private String profileGlobal;

    public static Global getInstance() {
        return SpringUtils.getBean(Global.class);
    }

    /**
     * 获取项目名称
     */
    public static String getName() {
        return StringUtils.nvl(getInstance().getNameGlobal(), "code-generator");
    }

    /**
     * 获取项目版本
     */
    public static String getVersion() {
        return StringUtils.nvl(getInstance().getVersionGlobal(), "1.1.0");
    }

    /**
     * 获取版权年份
     */
    public static String getCopyrightYear() {
        return StringUtils.nvl(getInstance().getCopyrightYearGlobal(), "2020");
    }

    /**
     * 获取文件上传路径
     */
    public static String getProfile() {
        return getInstance().getProfileGlobal();
    }

    /**
     * 获取下载路径
     */
    public static String getDownloadPath() {
        return getProfile() + "download/";
    }

}
