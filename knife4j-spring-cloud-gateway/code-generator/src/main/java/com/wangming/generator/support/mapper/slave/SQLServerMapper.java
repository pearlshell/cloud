package com.wangming.generator.support.mapper.slave;

import com.wangming.generator.support.service.BaseService;

/**
 * SQLServer数据源 数据层
 *
 * @author wangming
 */
public interface SQLServerMapper extends BaseService {

}