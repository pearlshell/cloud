package com.wangming.generator.common.copycat.annotaion;

import java.lang.annotation.*;

/**
 * 用于条件注解上面
 *
 * @author wangming
 * @date 2020-05-12 00:00:00
 */
@Inherited
@Documented
@Target({ElementType.ANNOTATION_TYPE})
@Retention(RetentionPolicy.RUNTIME)
public @interface CriteriaQuery {
}
