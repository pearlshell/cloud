package com.wangming.generator.support.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.wangming.generator.support.domain.GenTableColumn;

/**
 * 业务字段 数据层
 *
 * @author wangming
 */
public interface GenTableColumnMapper extends BaseMapper<GenTableColumn> {

}