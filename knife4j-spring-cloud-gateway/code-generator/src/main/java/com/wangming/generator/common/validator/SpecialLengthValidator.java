package com.wangming.generator.common.validator;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

/**
 * SpecialLengthValidator
 *
 * @author wangming
 * @date 2020-11-25 14:41:21
 */
public class SpecialLengthValidator implements ConstraintValidator<SpecialLength, String> {

    private SpecialLength specialLength;

    @Override
    public void initialize(SpecialLength specialLength) {
        this.specialLength = specialLength;
    }

    @Override
    public boolean isValid(String value, ConstraintValidatorContext context) {
        if (value == null || value.length() == specialLength.len()) {
            return true;
        }

        // 重置错误提示信息 message
        context.disableDefaultConstraintViolation();
        context.buildConstraintViolationWithTemplate("数据长度必须为" + specialLength.len()).addConstraintViolation();

        return false;
    }

}
