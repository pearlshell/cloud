package com.wangming.generator.support.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.wangming.generator.support.domain.SysDictData;

/**
 * 字典表 数据层
 *
 * @author wangming
 */
public interface SysDictDataMapper extends BaseMapper<SysDictData> {

}
