package com.wangming.generator.common.copycat.processor;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.wangming.generator.common.copycat.annotaion.ConditionProcessor;
import com.wangming.generator.common.copycat.annotaion.IsNull;
import com.wangming.generator.common.copycat.query.AbstractQuery;
import org.springframework.util.StringUtils;

import java.lang.reflect.Field;

/**
 * {@link IsNull} 注解处理器
 *
 * @param <QUERY>  自定义查询 Query
 * @param <ENTITY> 查询想对应的实体类型
 * @author wangming
 * @date 2020-05-12 00:00:00
 */
@ConditionProcessor(targetAnnotation = IsNull.class)
public class IsNullProcessor<QUERY extends AbstractQuery, ENTITY>
        extends CriteriaAnnotationProcessorAdaptor<IsNull, QUERY, QueryWrapper<ENTITY>, ENTITY> {

    @Override
    public boolean process(QueryWrapper<ENTITY> queryWrapper, Field field, QUERY query, IsNull criteriaAnnotation) {
        String columnName = criteriaAnnotation.alias();
        if (StringUtils.isEmpty(columnName)) {
            columnName = this.columnName(field, criteriaAnnotation.naming());
        }
        assert columnName != null;
        queryWrapper.isNull(columnName);

        return true;
    }

}
