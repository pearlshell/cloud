package com.wangming.generator.common.cache;

import com.wangming.generator.common.constant.Constants;
import com.wangming.generator.common.constant.GenConstants;

/**
 * <p>
 * 代码生成配置相关
 * </p>
 *
 * @author wangming
 * @date 2020-11-14 20:09
 */
public class GenCacheUtils {

    private static final String GEN_AUTHOR = "wangming";
    private static final String GEN_PACKAGE_NAME = "com.wangming.generator";
    private static final String GEN_TEMPLATE = "mybatis-plus";
    private static final String GEN_AUTO_REMOVE_PRE = Constants.YES;
    private static final String GEN_TABLE_PREFIX = "gen_";
    private static final String GEN_PREVIEW_THEME = "googlecode";
    private static final String GEN_SWAGGER_ENABLE = Constants.YES;
    private static final String GEN_EXCEL_ENABLE = Constants.YES;

    /**
     * 默认作者
     */
    public static String getAuthor() {
        return SysCacheUtils.getToString(GenConstants.AUTHOR, GEN_AUTHOR);
    }

    /**
     * 默认生成包路径
     */
    public static String getPackageName() {
        return SysCacheUtils.getToString(GenConstants.PACKAGE_NAME, GEN_PACKAGE_NAME);
    }

    /**
     * 代码生成模版（mybatis、 mybatis-plus、ruoyi）
     */
    public static String getGenTemplate() {
        return SysCacheUtils.getToString(GenConstants.GEN_TEMPLATE, GEN_TEMPLATE);
    }

    /**
     * 是否自动去除表前缀（0否 1是）
     */
    public static String getAutoRemovePre() {
        return SysCacheUtils.getToString(GenConstants.AUTO_REMOVE_PRE, GEN_AUTO_REMOVE_PRE);
    }

    /**
     * 表前缀
     */
    public static String getTablePrefix() {
        return SysCacheUtils.getToString(GenConstants.TABLE_PREFIX, GEN_TABLE_PREFIX);
    }

    /**
     * 默认代码预览主题
     */
    public static String getPreviewTheme() {
        return SysCacheUtils.getToString(GenConstants.PREVIEW_THEME, GEN_PREVIEW_THEME);
    }

    /**
     * 字段是否启用swagger注解（0否 1是）
     */
    public static String getSwaggerEnable() {
        return SysCacheUtils.getToString(GenConstants.SWAGGER_ENABLE, GEN_SWAGGER_ENABLE);
    }

    /**
     * 字段是否启用excel注解（0否 1是）
     */
    public static String getExcelEnable() {
        return SysCacheUtils.getToString(GenConstants.EXCEL_ENABLE, GEN_EXCEL_ENABLE);
    }

}
