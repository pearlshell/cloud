package com.wangming.generator.common.cache;

import com.wangming.generator.common.utils.StringUtils;
import com.wangming.generator.common.utils.spring.SpringUtils;
import net.sf.ehcache.Cache;
import net.sf.ehcache.CacheManager;
import net.sf.ehcache.Element;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Set;

/**
 * <p>
 * EhCache工具类
 * </p>
 *
 * @author wangming
 * @date 2020-11-15 21:19
 */
public class CacheUtils {

    private static final Logger logger = LoggerFactory.getLogger(CacheUtils.class);

    private static final CacheManager CACHE_MANAGER = SpringUtils.getBean(CacheManager.class);

    /**
     * 根据CacheName与ConfigKey获取ConfigValue，为空则返回默认值
     *
     * @param cacheName    缓存名称
     * @param configKey    参数键名
     * @param defaultValue 默认返回值
     * @return 参数键值
     */
    public static Object get(String cacheName, String configKey, Object defaultValue) {
        Object value = get(cacheName, getKey(configKey));
        return StringUtils.isNull(value) ? defaultValue : value;
    }

    /**
     * 根据CacheName与ConfigKey获取ConfigValue
     *
     * @param cacheName 缓存名称
     * @param configKey 参数键名
     * @return 参数键值
     */
    public static Object get(String cacheName, String configKey) {
        Element element = getCache(cacheName).get(getKey(configKey));
        return StringUtils.isNull(element) ? null : element.getObjectValue();
    }

    /**
     * 根据CacheName获得一个Cache，没有则显示日志
     *
     * @param cacheName 缓存名称
     * @return Cache
     */
    private static Cache getCache(String cacheName) {
        Cache cache = CACHE_MANAGER.getCache(cacheName);
        if (cache == null) {
            throw new RuntimeException("当前系统中没有定义“" + cacheName + "”这个缓存。");
        }
        return cache;
    }

    /**
     * 将配置信息写入指定CacheName缓存
     *
     * @param cacheName   缓存名称
     * @param configKey   参数键名
     * @param configValue 参数键值
     */
    public static void put(String cacheName, String configKey, Object configValue) {
        getCache(cacheName).put(new Element(getKey(configKey), configValue));
        logger.debug("新增缓存: {} => key: {}, value: {}", cacheName, getKey(configKey), configValue);
    }

    /**
     * 根据cacheName从缓存中移除所有配置
     *
     * @param cacheName 缓存名称
     */
    public static void removeAll(String cacheName) {
        Cache cache = getCache(cacheName);
        cache.removeAll();
        logger.debug("清理缓存： {} => {}", cacheName, cache.getKeys());
    }

    /**
     * 根据cacheName与configKeys从缓存中批量移除配置
     *
     * @param cacheName  缓存名称
     * @param configKeys 参数键名
     */
    public static void removeByKeys(String cacheName, Set<String> configKeys) {
        for (String key : configKeys) {
            remove(cacheName, key);
        }
        logger.debug("清理缓存： {} => {}", cacheName, configKeys);
    }

    /**
     * 根据cacheName与configKey从缓存中移除指定配置
     *
     * @param cacheName 缓存名称
     * @param configKey 参数键名
     */
    public static void remove(String cacheName, String configKey) {
        getCache(cacheName).remove(getKey(configKey));
        logger.debug("清理缓存： {} => {}", cacheName, configKey);
    }

    /**
     * 获取缓存键名
     *
     * @param key 缓存键名
     * @return 缓存键名
     */
    public static String getKey(String key) {
        return StringUtils.trim(key);
    }

}
