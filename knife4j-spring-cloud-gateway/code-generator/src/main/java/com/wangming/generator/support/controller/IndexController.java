package com.wangming.generator.support.controller;

import com.wangming.generator.common.core.controller.BaseController;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;

/**
 * 首页 业务处理
 *
 * @author wangming
 */
@Controller
public class IndexController extends BaseController {

    /** 系统首页 */
    @GetMapping("/index")
    public String index(ModelMap modelMap) {
        return "index";
    }

}
