package com.wangming.generator.framework.aspectj.lang.enums;

/**
 * 操作状态
 *
 * @author wangming
 */
public enum BusinessStatus {

    /**
     * 成功
     */
    SUCCESS,

    /**
     * 失败
     */
    FAIL,

}
