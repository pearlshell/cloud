package com.wangming.generator.common.copycat.enums;

/**
 * 比较运行符
 *
 * @author wangming
 * @date 2020-05-12 00:00:00
 */
public enum CompareEnum {

    /**
     * 等于
     */
    EQ,
    /**
     * 不等于
     */
    NE,
    /**
     * 大于等于
     */
    GE,
    /**
     * 大于
     */
    GT,
    /**
     * 小于等于
     */
    LE,
    /**
     * 小于
     */
    LT,

}
