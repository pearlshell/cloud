package com.wangming.generator.common.copycat.annotaion;

import com.wangming.generator.common.copycat.enums.ColumnNamingStrategy;

import java.lang.annotation.*;

/**
 * 大于等于(>=) 条件注解
 *
 * @author wangming
 * @date 2020-05-12 00:00:00
 */
@Documented
@CriteriaQuery
@Target({ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
public @interface Ge {

    /**
     * 自定义的属性值
     */
    String alias() default "";

    /**
     * 默认下划线
     *
     * @return ColumnNamingStrategy
     */
    ColumnNamingStrategy naming() default ColumnNamingStrategy.LOWER_CASE_UNDER_LINE;

}
