package com.wangming.generator.common.copycat.time;

import com.wangming.generator.common.copycat.util.TimeUtils;

import java.util.Date;

/**
 * 默认的时间处理器-处理为 {@link Date} 对象
 *
 * @author wangming
 * @date 2020-09-20 00:00:00
 * @since 1.2.0
 */
public class DefaultTimeProcessor implements TimeProcessor<Date> {

    @Override
    public boolean supports(Class<?> clazz) {
        return Date.class.equals(clazz);
    }

    @Override
    public Date handleTime(Long timeStamp, Class<?> clazz) {
        return TimeUtils.toTime(timeStamp, clazz);
    }

}
