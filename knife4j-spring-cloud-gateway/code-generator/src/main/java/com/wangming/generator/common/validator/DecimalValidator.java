package com.wangming.generator.common.validator;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.math.BigDecimal;

/**
 * @author wangming
 * @date 2020-07-09 13:57
 */
public class DecimalValidator implements ConstraintValidator<Decimal, String> {

    @Override
    public boolean isValid(String value, ConstraintValidatorContext context) {
        if (value != null) {
            try {
                new BigDecimal(value);
            } catch (NumberFormatException e) {
                return false;
            }
        }
        return true;
    }

}
