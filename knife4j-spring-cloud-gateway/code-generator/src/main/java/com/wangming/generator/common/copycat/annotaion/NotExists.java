package com.wangming.generator.common.copycat.annotaion;

import com.wangming.generator.common.copycat.enums.ColumnNamingStrategy;

import java.lang.annotation.*;

/**
 * 不存在(NOT EXISTS)  条件注解
 *
 * @author wangming
 * @date 2020-05-12 00:00:00
 */
@Documented
@CriteriaQuery
@Target({ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
public @interface NotExists {

    /**
     * 自定义的属性值
     */
    String alias() default "";

    /**
     * TODO 还未想好
     * not exists Sql
     */
    String existsSql() default "";

    /**
     * 默认下划线
     *
     * @return ColumnNamingStrategy
     */
    ColumnNamingStrategy naming() default ColumnNamingStrategy.LOWER_CASE_UNDER_LINE;

}
