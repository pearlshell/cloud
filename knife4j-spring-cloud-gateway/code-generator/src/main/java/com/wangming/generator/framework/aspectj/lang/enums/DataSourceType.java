package com.wangming.generator.framework.aspectj.lang.enums;

/**
 * 数据源
 *
 * @author wangming
 */
public enum DataSourceType {
    /**
     * 主库
     */
    MASTER,
    /**
     * 从库
     */
    SLAVE
}
