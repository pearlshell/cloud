package com.wangming.generator.common.cache;

import java.util.HashSet;
import java.util.Set;

/**
 * <p>
 * 系统配置缓存管理
 * </p>
 *
 * @author wangming
 * @date 2020-11-14 12:43
 */
public class SysCacheUtils {

    /** 参数管理 cache name */
    private static final String SYS_CONFIG_CACHE = "sys-config";
    /** 参数管理 cache key */
    private static final String SYS_CONFIG_KEY = "sys_key:";

    /**
     * 根据ConfigKey获取ConfigValue，为空则返回默认值
     *
     * @param configKey 参数键名
     * @return 参数键值
     */
    public static String getToString(String configKey, String defaultValue) {
        return CacheUtils.get(getCacheName(), getConfigKey(configKey), defaultValue).toString();
    }

    /**
     * 根据ConfigKey获取ConfigValue
     *
     * @param configKey 参数键名
     * @return 参数键值
     */
    public static Object get(String configKey) {
        return CacheUtils.get(getCacheName(), getConfigKey(configKey));
    }

    /**
     * 将配置信息写入缓存
     *
     * @param configKey   参数键名
     * @param configValue 参数键值
     */
    public static void put(String configKey, Object configValue) {
        CacheUtils.put(getCacheName(), getConfigKey(configKey), configValue);
    }

    /**
     * 从缓存中移除所有配置
     */
    public static void removeAll() {
        CacheUtils.removeAll(getCacheName());
    }

    /**
     * 根据configKeys从缓存中批量移除配置
     *
     * @param configKeys 参数键名
     */
    public static void removeByKeys(Set<String> configKeys) {
        Set<String> set = new HashSet<>();
        for (String key : configKeys) {
            set.add(getConfigKey(key));
        }
        CacheUtils.removeByKeys(getCacheName(), set);
    }

    /**
     * 根据configKey从缓存中移除指定配置
     *
     * @param configKey 参数键名
     */
    public static void remove(String configKey) {
        CacheUtils.remove(getCacheName(), getConfigKey(configKey));
    }

    /**
     * 获取系統 cache name
     *
     * @return 缓存名
     */
    private static String getCacheName() {
        return SYS_CONFIG_CACHE;
    }

    /**
     * 设置系統 cache key
     *
     * @param configKey configKey
     * @return 缓存键key
     */
    private static String getConfigKey(String configKey) {
        return SYS_CONFIG_KEY + CacheUtils.getKey(configKey);
    }

}
