package com.wangming.generator.support.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.wangming.generator.support.domain.SysDataSource;
import com.wangming.generator.support.mapper.SysDataSourceMapper;
import com.wangming.generator.support.service.ISysDataSourceService;
import org.springframework.stereotype.Service;

/**
 * 系统数据源 业务层处理
 *
 * @author wangming
 */
@Service
public class SysDataSourceServiceImpl extends ServiceImpl<SysDataSourceMapper, SysDataSource> implements ISysDataSourceService {

}
