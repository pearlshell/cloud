package com.wangming.generator.common.validator;

import javax.validation.Constraint;
import javax.validation.OverridesAttribute;
import javax.validation.Payload;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @author wangming
 * @date 2020-11-25 13:57
 */
@Target({ElementType.FIELD, ElementType.PARAMETER})
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = ListIntegerValueValidator.class)
public @interface ListIntegerValue {

    @OverridesAttribute(constraint = Min.class, name = "value") long min() default 0;

    @OverridesAttribute(constraint = Max.class, name = "value") long max() default Long.MAX_VALUE;

    @OverridesAttribute(constraint = Max.class, name = "value") long regular() default Long.MAX_VALUE;
    String message() default "不在要求范围之内";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};

}
