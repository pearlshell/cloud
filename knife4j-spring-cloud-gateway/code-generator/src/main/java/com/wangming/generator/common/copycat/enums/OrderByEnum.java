package com.wangming.generator.common.copycat.enums;

/**
 * SQL 排序
 *
 * @author wangming
 * @date 2020-05-12 00:00:00
 */
public enum OrderByEnum {

    /**
     * 升序
     */
    ASC,
    /**
     * 降序
     */
    DESC;

}
