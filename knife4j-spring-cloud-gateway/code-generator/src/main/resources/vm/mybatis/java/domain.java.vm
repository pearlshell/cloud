package ${packageName}.domain;

#if(${swaggerConfig})
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
#end
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import ${packageName}.common.core.domain.BaseEntity;

#foreach ($import in $importList)
import ${import};
#end

/**
 * <p>
 * ${functionName}
 * </p>
 *
 *@author ${author}
 *@date ${datetime}
 */
@Data
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
#if(${swaggerConfig})
@ApiModel(value = "${ClassName}${functionName}", description = "${functionName}")
#end
public class ${ClassName} extends BaseEntity {

#foreach ($column in $columns)
#if(!$table.isSuperColumn($column.javaF1ield))
    /** $column.columnComment */
#if(${swaggerConfig})
    @ApiModelProperty("$column.columnComment")
#end
#end
#if(${excel})
#set($parentheseIndex=$column.columnComment.indexOf("��"))
#if($parentheseIndex != -1)
    #set($comment=$column.columnComment.substring(0, $parentheseIndex))
#else
    #set($comment=$column.columnComment)
#end
#if($parentheseIndex != -1)
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
#elseif($column.javaType == 'Date')
    @Excel(name = "${comment}", width = 30, dateFormat = "yyyy-MM-dd")
#else
    @Excel(name = "${comment}")
#end
#end
    private $column.javaType $column.javaField;
#end
#end

}
