package cn.com.kyb.common.utils;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class UploadUtils {

    public static final SimpleDateFormat sdf1=new SimpleDateFormat("yyyyMMddHHmmss");

    // 案例附件路径
    public static final String CASE = "case";
    // 故障附件路径
    public static final String FAULT = "fault";
    // 故障
    public static final Integer CASE_RESOURCE = 1;
    // 案例
    public static final Integer FAULT_RESOURCE = 2;

    public final static String BASE_PATH = "src/main/resources/";
    // 项目根路径下的目录  -- SpringBoot static 目录相当于是根路径下（SpringBoot 默认）
    public final static String IMG_PATH_PREFIX = "static/upload/imgs";

    public static File getImgDirFile(String fileName,Integer type,Date date){

        //组装文件新路径
        String filePath = getFileUri(type, date);
        //拼接新的文件名称
        String newFileName = getNewFileName(fileName, date);
        // 构建上传文件的存放文件（空文件，流转换输入）
        String fileDirPath = new String(BASE_PATH + IMG_PATH_PREFIX+filePath);

        File fileDir = new File(fileDirPath);
        if(!fileDir.exists()){
            // 递归生成文件夹
            fileDir.mkdirs();
        }
        File newFile = new File(fileDir.getAbsolutePath() + File.separator + newFileName);
        return newFile;
    }

    public static String getFileUri(Integer type, Date date){
        Calendar cal = Calendar.getInstance();
        Integer year = cal.get(Calendar.YEAR);
        Integer month = cal.get(Calendar.MONTH);
        Integer day = cal.get(Calendar.DATE);
        String yearStr = year.toString();
        String monthStr = ((Integer)(month+1)).toString();
        String dataStr=day+"";

        //选择上传路径
        String filePath = null;
        if(CASE_RESOURCE==type){
            //案例上传
            filePath = CASE;
        }else if(FAULT_RESOURCE==type){
            //故障上传
            filePath = FAULT;
        }else{
            filePath = CASE;
        }

        //组装文件新路径
        filePath += "/"+yearStr+"/"+monthStr+"/"+dataStr;
        return filePath;
    }

    public static String getNewFileName(String fileName, Date date){
        //组装新文件名称
        String filenameStr = sdf1.format(date);
        //获取后缀为止
        int index = fileName.lastIndexOf('.');
        //获取后缀
        String suffix = fileName.substring(index);
        //拼接新的文件名称
        String newFileName = filenameStr+suffix;
        return newFileName;
    }

}
