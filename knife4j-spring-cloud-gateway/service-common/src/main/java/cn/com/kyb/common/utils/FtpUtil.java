package cn.com.kyb.common.utils;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;

import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.ftp.FTPFile;
import org.apache.commons.net.ftp.FTPReply;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class FtpUtil {

    private static Logger logger = LoggerFactory.getLogger(FtpUtil.class);

    private static final String PATH_F = "/";

    /** ftp服务器地址  */
    @Value("${ftp.hostname}")
    public static String hostname;

    /** ftp服务器端口号默认为21  */
    @Value("${ftp.port}")
    public static Integer port;

    /** ftp登录账号  */
    @Value("${ftp.username}")
    public static String username;

    /** ftp登录密码  */
    @Value("${ftp.password}")
    public static String password;


    /**
     * *上传文件
     * @param pathname ftp服务保存地址
     * @param fileName 上传到ftp的文件名
     * @return
     */
    public static boolean uploadFile(String pathname, String fileName,byte[] inputByte){
        FTPClient ftpClient = getClient();
        InputStream inputStream = null;
        boolean flag = false;
        try{
            logger.info("开始上传文件");
            ftpClient.setFileType(FTPClient.BINARY_FILE_TYPE);
            //将客户端设置为被动模式
            ftpClient.enterLocalPassiveMode();
            createDirecroty(ftpClient, pathname);
            inputStream = new ByteArrayInputStream(inputByte);
            ftpClient.storeFile(fileName, inputStream);
            ftpClient.logout();
            logger.info("上传文件成功");
            flag = true;
        }catch (Exception e) {
            logger.error("上传文件失败");
            logger.error(e.getMessage());
        }finally{
            if(ftpClient.isConnected()){
                try{
                    ftpClient.disconnect();
                }catch(IOException e){
                    logger.error(e.getMessage());
                }
            }
            if(null != inputStream){
                try {
                    inputStream.close();
                } catch (IOException e) {
                    logger.error(e.getMessage());
                }
            }
        }
        return flag;
    }

    //改变目录路径
    private static boolean changeWorkingDirectory(FTPClient ftpClient, String directory) {
        boolean flag = true;
        try {
            flag = ftpClient.changeWorkingDirectory(directory);
            if (flag) {
                logger.info("进入文件夹" + directory + " 成功！");

            } else {
                flag = false;
                logger.info("进入文件夹" + directory + " 失败！开始创建文件夹");
            }
        } catch (IOException ioe) {
            logger.error(ioe.getMessage());
        }
        return flag;
    }

    //创建多层目录文件，如果有ftp服务器已存在该文件，则不创建，如果无，则创建
    private static boolean createDirecroty(FTPClient ftpClient, String remote) throws IOException {
        boolean success = true;
        String directory = remote + PATH_F;
        // 如果远程目录不存在，则递归创建远程服务器目录
        if (!directory.equalsIgnoreCase(PATH_F) && !changeWorkingDirectory(ftpClient, new String(directory))) {
            int start = 0;
            int end = 0;
            if (directory.startsWith(PATH_F)) {
                start = 1;
            } else {
                start = 0;
            }
            end = directory.indexOf(PATH_F, start);
            String path = "";
            String paths = "";
            while (true) {
                String subDirectory = new String(remote.substring(start, end).getBytes("GBK"), "iso-8859-1");
                path = path + PATH_F + subDirectory;
                if (!existFile(ftpClient, path)) {
                    if (makeDirectory(ftpClient, subDirectory)) {
                        changeWorkingDirectory(ftpClient, subDirectory);
                    } else {
                        logger.info("创建目录[" + subDirectory + "]失败");
                        changeWorkingDirectory(ftpClient, subDirectory);
                    }
                } else {
                    changeWorkingDirectory(ftpClient, subDirectory);
                }

                paths = paths + PATH_F + subDirectory;
                start = end + 1;
                end = directory.indexOf(PATH_F, start);
                // 检查所有目录是否创建完毕
                if (end <= start) {
                    break;
                }
            }
        }
        return success;
    }

    /**
     * *判断ftp服务器文件是否存在
     * @param ftpClient
     * @param path
     * @return
     * @throws IOException
     */
    private static boolean existFile(FTPClient ftpClient, String path) throws IOException {
        boolean flag = false;
        FTPFile[] ftpFileArr = ftpClient.listFiles(path);
        if (ftpFileArr.length > 0) {
            flag = true;
        }
        return flag;
    }

    /**
     * *创建目录
     * @param ftpClient
     * @param dir
     * @return
     */
    private static boolean makeDirectory(FTPClient ftpClient, String dir) {
        boolean flag = true;
        try {
            flag = ftpClient.makeDirectory(dir);
            if (flag) {
                logger.info("创建文件夹" + dir + " 成功！");

            } else {
                flag = false;
                logger.error("创建文件夹" + dir + " 失败！");
            }
        } catch (Exception e) {
            logger.error(e.getMessage());
        }
        return flag;
    }

    /** * 下载文件 *
     * @param dir FTP服务器文件目录 *
     * @param filename 文件名称 *
     * @return
     * */
    public static byte[] downloadFile(String dir, String filename){
        byte[] bytes = null;
        String path = dir + PATH_F + filename;
        InputStream in = null;
        FTPClient ftpClient  = getClient();
        try {
            ftpClient.setFileType(FTPClient.BINARY_FILE_TYPE);

            // 转到指定下载目录
            if (path != null) {// 验证是否有该文件夹，有就转到，没有创建后转到该目录下
                ftpClient.changeWorkingDirectory(path);// 转到指定目录下
            }
            // 不需要遍历，改为直接用文件名取
            String remoteAbsoluteFile = toFtpFilename(path);

            // 下载文件
            ftpClient.setBufferSize(1024 * 1024);
            ftpClient.setControlEncoding("UTF-8");
            ftpClient.enterLocalPassiveMode();
            ftpClient.setFileType(FTPClient.BINARY_FILE_TYPE);
            in = ftpClient.retrieveFileStream(remoteAbsoluteFile);
            bytes = toByteArray(in);
            ftpClient.logout();
            logger.info("下载文件成功");
        } catch (Exception e) {
            logger.error("下载文件失败");
            logger.error(e.getMessage());
        } finally {
            if (ftpClient.isConnected()) {
                try {
                    ftpClient.disconnect();
                } catch (IOException e) {
                    logger.error(e.getMessage());
                }
            }
            if (null != in) {
                try {
                    in.close();
                } catch (IOException e) {
                    logger.error(e.getMessage());
                }
            }
        }
        return bytes;

    }

    /** * 删除文件 *
     * @param pathname FTP服务器保存目录 *
     * @param filename 要删除的文件名称 *
     * @return */
    public static boolean deleteFile(String pathname, String filename){
        boolean flag = false;
        FTPClient ftpClient  = getClient();
        try {
            logger.info("开始删除文件");
            //切换FTP目录
            ftpClient.changeWorkingDirectory(pathname);
            ftpClient.dele(filename);
            ftpClient.logout();
            flag = true;
            logger.info("删除文件成功");
        } catch (Exception e) {
            logger.error("删除文件失败");
            logger.error(e.getMessage());
        } finally {
            if(ftpClient.isConnected()){
                try{
                    ftpClient.disconnect();
                }catch(IOException e){
                    logger.error(e.getMessage());
                }
            }
        }
        return flag;
    }

    /**
     * * 获取连接
     * @return
     */
    private static FTPClient getClient() {
        FTPClient ftpClient = new FTPClient();
        ftpClient.setControlEncoding("utf-8");
        try {
            logger.info("connecting...ftp服务器:"+hostname+":"+port);
            ftpClient.connect(hostname, port);
            ftpClient.login(username, password);
            int replyCode = ftpClient.getReplyCode();
            if(!FTPReply.isPositiveCompletion(replyCode)){
                logger.info("connect failed...ftp服务器:"+hostname+":"+port);
            }

        }catch (MalformedURLException e) {
            logger.error(e.getMessage());
        }catch (IOException e) {
            logger.error(e.getMessage());
        }
        return ftpClient;
    }


    private static byte[] toByteArray(InputStream input) throws IOException {
        ByteArrayOutputStream output = new ByteArrayOutputStream();
        byte[] buffer = new byte[1024 * 4];
        int n = 0;
        while (-1 != (n = input.read(buffer))) {
            output.write(buffer, 0, n);
        }
        return output.toByteArray();
    }


    /** 转化输出的编码 */
    private static String toFtpFilename(String fileName) throws Exception {
        return new String(fileName.getBytes("UTF-8"), "ISO8859-1");
    }

}
