package cn.com.kyb.common.utils;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.serializer.SimplePropertyPreFilter;
import cn.com.kyb.common.rest.LevelPropertyPreFilter;

public class ResponseUtil {

    //过滤指定层级字段
    public static JSONObject filterLevelFields(String[] filters, Object o){
        LevelPropertyPreFilter propertyPreFilter = new LevelPropertyPreFilter();
        propertyPreFilter.addExcludes(filters);
        String jsonStr = JSON.toJSONString(o, propertyPreFilter);
        JSONObject json = JSON.parseObject(jsonStr, JSONObject.class);
        return new JSONObject(json);
    }

    //过滤所有同名字段
    public static JSONObject filterFields(String[] filters, Object o){
        SimplePropertyPreFilter filter = new SimplePropertyPreFilter();
        for(int i=0; i<filters.length; i++){
            filter.getExcludes().add(filters[i]);
        }
        String jsonStr = JSON.toJSONString(o, filter);
        JSONObject json = JSON.parseObject(jsonStr, JSONObject.class);
        return new JSONObject(json);
    }
}
