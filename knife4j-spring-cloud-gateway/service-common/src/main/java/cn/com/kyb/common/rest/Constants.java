package cn.com.kyb.common.rest;

public class Constants {

    //成功响应码
    public static final int SUCCESS=1000;

    //失败响应码
    public static final int FAIL=4000;

    //参数异常
    public static final int ILLEGAL_PARAM=8000;

}
