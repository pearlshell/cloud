package cn.com.kyb.common.rest;

import lombok.Data;

import java.util.List;

@Data
public class PageData<E> {

    private long pageNumber;
    private long total;
    private List<E> list;

    public PageData(long pageNumber,long total,List<E> list){
        this.pageNumber=pageNumber;
        this.total=total;
        this.list=list;
    }

}
