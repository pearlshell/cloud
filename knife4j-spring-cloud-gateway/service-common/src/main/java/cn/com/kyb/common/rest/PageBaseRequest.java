package cn.com.kyb.common.rest;

import lombok.Data;

@Data
public class PageBaseRequest extends BaseRequest {

    private Integer pageNumber = 1;

    private Integer pageSize = 15;
}
