package cn.com.kyb.common.service;

import cn.com.kyb.common.rest.PageData;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import cn.com.kyb.common.rest.ApiPageResult;
import cn.com.kyb.common.rest.Constants;
import cn.com.kyb.common.rest.BasePageRequest;

public class BaseServiceImpl<M extends BaseMapper<T>,T> extends ServiceImpl<M,T> implements BaseService<T>{

    protected <E> ApiPageResult<E> toPageResult(Page<E> data){
        PageData<E> pageData = new PageData<>(data.getCurrent(),data.getTotal(),data.getRecords());
        ApiPageResult<E> ret = new ApiPageResult<>(Constants.SUCCESS,"",pageData);
        return ret;
    }

    @Override
    public ApiPageResult<T> page(BasePageRequest page, Wrapper<T> query) {
        Page<T> pageRequest = new Page<>(page.getPageNumber(),page.getPageSize());
        this.page(pageRequest, query);
        return toPageResult(pageRequest);
    }
}
