package cn.com.kyb.common.rest;

import lombok.Data;

/**
 * 通用列表查询条件，兼容分页和不分页 ct
 * @param <T>
 */
@Data
public class BasePageRequest<T> extends BaseRequest{

    private Integer pageNumber = 1;

    private Integer pageSize = 15;

    //是否分页，默认为分页
    private Boolean isPage = true;

    //查询条件
    private T con;

    public BasePageRequest(){}

    public BasePageRequest(int pageNumber, int pageSize, T con, boolean isPage){
        this.pageNumber = pageNumber;
        this.pageSize = pageSize;
        this.con = con;
        this.isPage = isPage;
    }

}
