package cn.com.kyb.common.rest;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.poi.ss.formula.functions.T;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import springfox.documentation.spring.web.json.Json;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

public class BaseController {

    //获取Request
    public HttpServletRequest getRequest() {
        return ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
    }

    //获取Response
    public HttpServletResponse getResponse() {
        return ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getResponse();
    }

    //获取token
    public String getToken(){
        return this.getRequest().getHeader("token");
    }

    public ApiResult success(){
        return new ApiResult(Constants.SUCCESS, "操作成功", null);
    }

    public ApiResult success(Object data){
        return new ApiResult(Constants.SUCCESS, "操作成功", data);
    }

    /*public JSONObject success(Object data){
        ApiResult apiResult = new ApiResult(Constants.SUCCESS, "操作成功", data);
        JSONObject jsonObject = (JSONObject)JSONObject.toJSON(apiResult);
        return jsonObject;
    }*/

    public ApiPageResult successPage(){
        return new ApiPageResult(Constants.SUCCESS, "操作成功", null);
    }

    public ApiPageResult successPage(Page<?> data){
        PageData<?> pageData = new PageData<>(data.getCurrent(),data.getTotal(),data.getRecords());
        return new ApiPageResult(Constants.SUCCESS, "操作成功", pageData);
    }

    /*public JSONObject successPage(Page<?> data){
        PageData<?> pageData = new PageData<>(data.getCurrent(),data.getTotal(),data.getRecords());
        ApiPageResult apiPageResult = new ApiPageResult(Constants.SUCCESS, "操作成功", pageData);
        JSONObject jsonObject = (JSONObject)JSONObject.toJSON(apiPageResult);
        return jsonObject;
    }*/

    public ApiResult error(){
        return new ApiResult(Constants.FAIL, "操作失败", null);
    }

    public ApiResult error(Object data){
        return new ApiResult(Constants.FAIL, "操作失败", data);
    }

    public ApiPageResult errorPage(){
        return new ApiPageResult(Constants.FAIL, "操作失败", null);
    }

    public ApiPageResult errorPage(Page<T> data){
        PageData<T> pageData = new PageData<>(data.getCurrent(),data.getTotal(),data.getRecords());
        return new ApiPageResult(Constants.FAIL, "操作失败", pageData);
    }

    public ApiResult illegal(){
        return new ApiResult(Constants.ILLEGAL_PARAM, "请求参数不合法", null);
    }

    public ApiPageResult illegalPage(){
        return new ApiPageResult(Constants.ILLEGAL_PARAM, "请求参数不合法", null);
    }

    public ApiResult setIllegal(String msg){
        return new ApiResult(Constants.ILLEGAL_PARAM, msg, null);
    }

    public ApiPageResult setIllegalPage(String msg){
        return new ApiPageResult(Constants.ILLEGAL_PARAM, msg, null);
    }

}
