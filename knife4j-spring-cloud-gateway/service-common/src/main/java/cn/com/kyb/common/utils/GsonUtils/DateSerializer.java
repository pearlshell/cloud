package cn.com.kyb.common.utils.GsonUtils;

import java.lang.reflect.Type;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.Date;

import com.google.gson.JsonElement;
import com.google.gson.JsonPrimitive;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

//类型为Date
//public class DateSerializer implements JsonSerializer<Date> {
//    public JsonElement serialize(Date src, Type typeOfSrc, JsonSerializationContext context) {
//        return new JsonPrimitive(src.getTime());
//    }
//}

//类型为LocalDateTime
public class DateSerializer implements JsonSerializer<LocalDateTime> {
    public JsonElement serialize(LocalDateTime src, Type typeOfSrc, JsonSerializationContext context) {
        //毫秒
        Long milliSecond = LocalDateTime.now().toInstant(ZoneOffset.of("+8")).toEpochMilli();
        return new JsonPrimitive(milliSecond);
    }
}