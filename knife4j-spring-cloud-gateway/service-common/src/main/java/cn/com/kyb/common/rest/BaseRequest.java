package cn.com.kyb.common.rest;

import lombok.Data;

@Data
public class BaseRequest {
    protected String keyword;
}
