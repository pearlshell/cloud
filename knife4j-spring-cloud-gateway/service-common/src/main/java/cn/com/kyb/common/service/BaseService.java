package cn.com.kyb.common.service;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.extension.service.IService;
import cn.com.kyb.common.rest.ApiPageResult;
import cn.com.kyb.common.rest.BasePageRequest;

public interface BaseService<T> extends IService<T> {
    ApiPageResult<T> page (BasePageRequest page, Wrapper<T> query);
}
