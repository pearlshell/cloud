package cn.com.kyb.common.rest;

import lombok.Data;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;
@Data
public class ApiPageResult<T> {

    private int code;

    private String message;

    private PageData<T> data;

    public ApiPageResult(){}

    public ApiPageResult(int code, String message, PageData<T> data){
        this.code = code;
        this.message = message;
        this.data = data;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ApiPageResult<T> that = (ApiPageResult<T>) o;
        return new EqualsBuilder()
                .append(code, that.code)
                .append(message, that.message)
                .append(data, that.data)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
                .append(code)
                .append(message)
                .append(data)
                .toHashCode();
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .append("code", code)
                .append("message", message)
                .append("data", data)
                .toString();
    }
}

