package  cn.com.kyb.common.utils.GsonUtils;

import java.lang.reflect.Type;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.Date;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;

//类型为Date
//public class DateDeserializer implements JsonDeserializer<Date> {
////
////    public Date deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
////        return new Date(json.getAsJsonPrimitive().getAsLong());
////    }
////
////}

//类型为LocalDateTime
public class DateDeserializer implements JsonDeserializer<LocalDateTime> {

    public LocalDateTime deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
        Long datetime = json.getAsJsonPrimitive().getAsLong();
        return LocalDateTime.ofEpochSecond(datetime,0, ZoneOffset.ofHours(8));
    }

}