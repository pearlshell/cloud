package cn.com.kyb.common.utils.GsonUtils;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import java.text.DateFormat;
import java.time.LocalDateTime;
import java.util.List;

public class GsonUtil {

    /**
     * json串转实体
     * @param jsonData
     * @param type
     * @param <T>
     * @return
     */
    public static <T> T json2Entity(String jsonData, Class<T> type) {
        Gson gson = create();
        T result = gson.fromJson(jsonData, type);
        return result;
    }


    /**
     * json串转List<E>
     * @param jsonData
     * @param type
     * @param <T>
     * @return
     */
    public static <T> List<T> json2List(String jsonData, Class<T> type) {
        Gson gson = create();
        List<T> result = gson.fromJson(jsonData, new TypeToken<List<T>>() { }.getType());
        return result;
    }

    public static<T> Object json2Object(String jsonStr,Class<T> obj) {
        T t = null;
        try {
            ObjectMapper objectMapper = new ObjectMapper();
            t = objectMapper.readValue(jsonStr,
                    obj);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return t;
    }


    public static Gson create() {
        GsonBuilder gb = new GsonBuilder();
        //类型为Date
        //gb.registerTypeAdapter(Date.class, new DateSerializer()).setDateFormat(DateFormat.LONG);
        //gb.registerTypeAdapter(Date.class, new DateDeserializers.DateDeserializer()).setDateFormat(DateFormat.LONG);

        //类型为LocalDateTime
        gb.registerTypeAdapter(LocalDateTime.class, new DateSerializer()).setDateFormat(DateFormat.LONG);
        gb.registerTypeAdapter(LocalDateTime.class, new DateDeserializer()).setDateFormat(DateFormat.LONG);
        Gson gson = gb.serializeNulls().create();
        return gson;
    }

}



