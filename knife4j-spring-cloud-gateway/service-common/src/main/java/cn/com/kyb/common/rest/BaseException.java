package cn.com.kyb.common.rest;

public class BaseException extends RuntimeException{
    public BaseException(String msg){
        super(msg);
    }
}
