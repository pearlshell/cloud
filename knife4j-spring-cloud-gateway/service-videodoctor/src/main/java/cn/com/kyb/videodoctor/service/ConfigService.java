package cn.com.kyb.videodoctor.service;
import cn.com.kyb.videodoctor.entity.Config;
import cn.com.kyb.common.rest.*;
import cn.com.kyb.common.service.BaseService;

import java.util.List;

/**
 * 代码生成器-参数配置表 服务类
 * @author chentao
 * @since 2021-01-08
 */
public interface ConfigService extends BaseService<Config> {

    //查询全部
    List<Config> selectAll(Config req);

    //分页
    ApiPageResult<Config> page(BasePageRequest<Config> req);

}