package cn.com.kyb.videodoctor.config;

import com.alibaba.fastjson.serializer.SerializerFeature;
import com.alibaba.fastjson.serializer.ValueFilter;
import com.alibaba.fastjson.support.spring.FastJsonHttpMessageConverter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.MediaType;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.ArrayList;
import java.util.List;

@Configuration
public class MyWebMvcConfigurer implements WebMvcConfigurer {

    @Value("${upload.basePath}")
    private String basePath;

    @Value("${upload.accessPath}")
    private String accessPath;


    //配置跨域
    @Override
    public void addCorsMappings(CorsRegistry registry) {
        registry.addMapping("/**")
        .allowedOrigins("*")
        .allowedMethods("GET","PUT","HEAD","DELETE","OPTIONS")
        .allowCredentials(true)
        .maxAge(3600)
        .allowedHeaders("*");
    }

    //配置访问路径，映射静态资源
    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        registry.addResourceHandler("/upload/**")
        .addResourceLocations("file:"+basePath)
        .addResourceLocations("file:"+accessPath)
        .addResourceLocations("classPath:/META-INF/resources/")
        .addResourceLocations("classPath:/resources")
        .addResourceLocations("classPath:/static/")
        .addResourceLocations("classPath:/public/");
    }

    /*//配置fastjson
    @Bean
    public HttpMessageConverter<Object> fastJsonHttpMessageConverter() {
        //1、定义一个convert转换消息的对象
        FastJsonHttpMessageConverter fastConverter = new FastJsonHttpMessageConverter();
        // 2.添加fastJson的配置信息,比如，是否需要格式化返回的json数据
        com.alibaba.fastjson.support.config.FastJsonConfig fastJsonConfig = new com.alibaba.fastjson.support.config.FastJsonConfig();

        fastJsonConfig.setSerializeFilters((ValueFilter) (o, s, source) -> {
            if (source == null) {
                return "";//此处是关键,如果返回对象的变量为null,则自动变成""
            }

            System.out.println(source.getClass().toString());
            //LocalDateTime类型时返回时间戳
            if (source instanceof LocalDateTime) {
                return ((LocalDateTime) source).toInstant(ZoneOffset.of("+8")).toEpochMilli();
            }

            return source;
        });
        fastJsonConfig.setSerializerFeatures(
                SerializerFeature.BrowserCompatible,
                SerializerFeature.WriteNullStringAsEmpty
        );

        // 处理中文乱码问题
        List<MediaType> fastMediaTypes = new ArrayList<>();
        fastMediaTypes.add(MediaType.APPLICATION_JSON_UTF8);
        fastConverter.setSupportedMediaTypes(fastMediaTypes);

        // 3.在convert中添加配置信息
        fastConverter.setFastJsonConfig(fastJsonConfig);
        return fastConverter;
    }

    // 配置消息转换器
    @Override
    public void configureMessageConverters(List<HttpMessageConverter<?>> converters) {
        converters.add(fastJsonHttpMessageConverter());

    }*/
}
