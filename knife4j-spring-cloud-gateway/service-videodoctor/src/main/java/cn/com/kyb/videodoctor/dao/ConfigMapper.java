package cn.com.kyb.videodoctor.dao;

import cn.com.kyb.videodoctor.entity.Config;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 代码生成器-参数配置表 Mapper 接口
 * </p>
 *
 * @author chentao
 * @since 2021-01-08
 */
public interface ConfigMapper extends BaseMapper<Config> {

}
