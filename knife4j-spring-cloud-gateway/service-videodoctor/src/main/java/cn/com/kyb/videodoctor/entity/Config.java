package cn.com.kyb.videodoctor.entity;

import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.baomidou.mybatisplus.annotation.Version;
import java.time.LocalDateTime;
import com.baomidou.mybatisplus.annotation.TableLogic;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import com.baomidou.mybatisplus.annotation.TableName;

/**
 * @desc
 * @author chentao
 * @since 2021-01-08
 */
@Data
@EqualsAndHashCode(callSuper = false)

@TableName("sys_config")
@ApiModel(value="Config", description="代码生成器-参数配置表")
public class Config extends Model<Config> {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "参数主键")
        private String id;

    @ApiModelProperty(value = "参数名称")
    private String configName;

    @ApiModelProperty(value = "参数键名")
    private String configKey;

    @ApiModelProperty(value = "参数键值")
    private String configValue;

    @ApiModelProperty(value = "系统内置（Y是 N否）")
    private String isBuiltIn;

    @ApiModelProperty(value = "关联的字典类型")
    private String dictType;

    @ApiModelProperty(value = "输入类型（0-text 1-radio 2-select）")
    private String inputType;

    @ApiModelProperty(value = "创建者")
    private String createBy;

    @ApiModelProperty(value = "创建时间")
    private LocalDateTime createTime;

    @ApiModelProperty(value = "更新者")
    private String updateBy;

    @ApiModelProperty(value = "更新时间")
    private LocalDateTime updateTime;

    @ApiModelProperty(value = "备注")
    private String remark;

    @ApiModelProperty(value = "是否删除")
    @TableLogic
    private Integer deleted;


    @Override
    protected Serializable pkVal() {
          return this.id;
    }

}
