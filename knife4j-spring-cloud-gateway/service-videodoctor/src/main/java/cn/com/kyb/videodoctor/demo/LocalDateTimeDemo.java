package cn.com.kyb.videodoctor.demo;

import java.time.*;
import java.time.temporal.ChronoField;

public class LocalDateTimeDemo {

    public static void main(String[] args) {
        //============localdate
        //获取当前年月日
        LocalDate localDate = LocalDate.now();
        System.out.print("当前的年月日:"+localDate);
        //构造指定的年月日
        LocalDate localDate1 = LocalDate.of(2019, 9, 10);
        System.out.println("指定的年月日:"+localDate1);

        int year = localDate.getYear();
        int year1 = localDate.get(ChronoField.YEAR);
        System.out.println("当前年:"+year);
        System.out.println("当前年:"+year1);
        Month month = localDate.getMonth();
        int month1 = localDate.get(ChronoField.MONTH_OF_YEAR);
        System.out.println("当前月:"+month);
        System.out.println("当前月:"+month1);
        int day = localDate.getDayOfMonth();
        int day1 = localDate.get(ChronoField.DAY_OF_MONTH);
        System.out.println("当前天:"+day);
        System.out.println("当前天:"+day1);
        DayOfWeek dayOfWeek = localDate.getDayOfWeek();
        int dayOfWeek1 = localDate.get(ChronoField.DAY_OF_WEEK);
        System.out.println("当前星期:"+dayOfWeek);
        System.out.println("当前星期:"+dayOfWeek1);

        //========localtime
        LocalTime localTime = LocalTime.of(13, 51, 10);
        LocalTime localTime1 = LocalTime.now();
        System.out.println("当前时间："+localTime);
        System.out.println("当前时间："+localTime1);

        //获取时分秒
        //获取小时
        int hour = localTime.getHour();
        int hour1 = localTime.get(ChronoField.HOUR_OF_DAY);
        System.out.println("当前小时："+hour);
        System.out.println("当前小时："+hour1);
        //获取分
        int minute = localTime.getMinute();
        int minute1 =             localTime.get(ChronoField.MINUTE_OF_HOUR);
        System.out.println("当前分钟："+minute);
        System.out.println("当前分钟："+minute1);
        //获取秒
        int second = localTime.getMinute();
        int second1 = localTime.get(ChronoField.SECOND_OF_MINUTE);
        System.out.println("当前秒："+second);
        System.out.println("当前秒："+second1);

        //===========localdatetime
        LocalDateTime localDateTime = LocalDateTime.now();
        LocalDateTime localDateTime1 = LocalDateTime.of(2019, Month.SEPTEMBER, 10, 14, 46, 56);
        LocalDateTime localDateTime2 = LocalDateTime.of(localDate, localTime);
        LocalDateTime localDateTime3 = localDate.atTime(localTime);
        LocalDateTime localDateTime4 = localTime.atDate(localDate);

        //获取秒数
        Long seconds = LocalDateTime.now().toEpochSecond(ZoneOffset.of("+8"));
        //获取毫秒数
        Long milliSecond = LocalDateTime.now().toInstant(ZoneOffset.of("+8")).toEpochMilli();

        System.out.println(localDateTime);
    }

}
