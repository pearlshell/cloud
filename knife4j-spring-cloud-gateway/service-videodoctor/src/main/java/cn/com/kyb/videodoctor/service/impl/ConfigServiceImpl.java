package cn.com.kyb.videodoctor.service.impl;

import cn.com.kyb.videodoctor.entity.Config;
import cn.com.kyb.videodoctor.dao.ConfigMapper;
import cn.com.kyb.videodoctor.service.ConfigService;
import cn.com.kyb.common.rest.*;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import cn.com.kyb.common.service.BaseServiceImpl;

import java.util.List;

/**
 * 代码生成器-参数配置表 服务实现类
 * @author chentao
 * @since 2021-01-08
 */
@Service
@Transactional
public class ConfigServiceImpl extends BaseServiceImpl<ConfigMapper, Config> implements ConfigService {

    @Autowired
    private ConfigMapper configMapper;

    @Override
    public List<Config> selectAll(Config req) {

        LambdaQueryWrapper<Config> query = new LambdaQueryWrapper();
        //组装查询条件

        return configMapper.selectList(query);
    }

    @Override
    public ApiPageResult<Config> page(BasePageRequest<Config> req) {


        Page<Config> page = new Page<>(req.getPageNumber(), req.getPageSize());
        LambdaQueryWrapper<Config> query = new LambdaQueryWrapper();
        //组装查询条件

        //分页
        if(req.getIsPage()){
            configMapper.selectPage(page, query);
            return toPageResult(page);
        }
        //不分页
        else{
            List<Config> configList = configMapper.selectList(query);
            if(!configList.isEmpty()){
                page.setTotal(configList.size());
                page.setRecords(configList);
                page.setCurrent(0);
            }
            return toPageResult(page);
        }
    }

}