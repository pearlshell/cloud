package cn.com.kyb.videodoctor.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;
import cn.com.kyb.common.rest.BaseController;

import cn.com.kyb.common.rest.*;
import org.springframework.util.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import cn.com.kyb.videodoctor.common.constant.GlobalConstant;

import cn.com.kyb.videodoctor.service.ConfigService;
import cn.com.kyb.videodoctor.entity.Config;
import java.util.List;
import java.time.LocalDateTime;

/**
 * 代码生成器-参数配置表控制器
 * @author chentao
 * @since 2021-01-08
 */
@RestController
@Slf4j
@Api(tags="代码生成器-参数配置表")
@RequestMapping("/config")
public class ConfigController extends BaseController {

    @Autowired
    public ConfigService configService;

    @PostMapping(value = "/incrUpd")
    @ResponseBody
    @ApiOperation(value = "增量更新", notes = "有则更新，无责新增；支持批量")
    public ApiResult saveOrUpdBatch(@RequestBody List<Config> records) {

        if(!records.isEmpty()){
            records.forEach(record -> {
                if(StringUtils.isEmpty(record.getId())){
                    record.setCreateTime(LocalDateTime.now());
                    record.setDeleted(GlobalConstant.NO);
                }else{
                    record.setUpdateTime(LocalDateTime.now());
                }
            });
        }
        configService.saveOrUpdateBatch(records);
        return success();
    }

    @PostMapping(value = "/get")
    @ResponseBody
    @ApiOperation(value = "查询明细", notes = "")
    public ApiResult get(@RequestBody BaseRequest req) {
        String id = req.getKeyword();
        if(StringUtils.isEmpty(id)){
            return setIllegal("缺少主键");
        }
        Config config = configService.getById(id);
        return success(config);
    }

    @PostMapping(value = "/del")
    @ResponseBody
    @ApiOperation(value = "删除", notes = "")
    public ApiResult del(@RequestBody BaseRequest req) {
        String id = req.getKeyword();
        if(StringUtils.isEmpty(id)){
            return setIllegal("缺少主键");
        }
        configService.removeById(id);
        return success();
    }

    @PostMapping("/page")
    @ResponseBody
    @ApiOperation(value = "查询列表", notes = "")
    public ApiPageResult<Config> page(@RequestBody BasePageRequest<Config> req) {
        return configService.page(req);
    }

    @PostMapping("/list")
    @ResponseBody
    @ApiOperation(value = "查询全部", notes = "")
    public ApiResult<Config> list(@RequestBody Config req) {
        List<Config> list = configService.selectAll(req);
        return success(list);
    }

}
