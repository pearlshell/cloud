package cn.com.kyb.videodoctor.config;

import cn.com.kyb.common.rest.ApiResult;
import cn.com.kyb.common.rest.Constants;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpRequest;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * 全局异常
 */
@Slf4j
@ControllerAdvice
@ResponseBody
public class GlobalExceptionHandler {

    /**
     * spring已统一提升为Exception，之定义全局异常即可
     */
    /*@ExceptionHandler(value = Exception.class)
    public ApiResult<String> exceptionHandler(HttpRequest request, Exception e){
        String msg = "GlobalError" + request.getURI();
        log.error(msg,e);
        return new ApiResult<>(Constants.FAIL,"服务器出现异常","");
    }*/

}
