
正常lambda表达式
// 方式一
LambdaQueryWrapper<User> lambda = new QueryWrapper<User>().lambda();
// 方式二
LambdaQueryWrapper<User> lambda2 = new LambdaQueryWrapper<>();
// 方式三
LambdaQueryWrapper<User> lambda3 = Wrappers.<User>lambdaQuery();

//链式lambda
List<BannerItem> bannerItems = new LambdaQueryChainWrapper<>(bannerItemMapper)
                        .eq(BannerItem::getBannerId, id)
                        .list();
