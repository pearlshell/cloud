积木思维：
PO=entity——单独积木块
BO=多个有关系的PO拼凑，如一个订单
DTO和VO,略有区别，如返回是否删除，DTO字段为枚举1，VO字段则为具体显示值，如已删除，考虑接口公用性，推荐web和rpc层使用DTO

dao层处理PO的CRUD操作

使用定时任务，启动类上添加@EnableSheduling
每天凌晨1点
直接在方法中使用@Sheduled(cron="0 0 1 * * ? ")

1，局部主键策略实现

在实体类中 ID属性加注解

@TableId(type = IdType.AUTO) 主键自增 数据库中需要设置主键自增
private Long id;
@TableId(type = IdType.NONE) 默认 跟随全局策略走
private Long id;
@TableId(type = IdType.UUID) UUID类型主键
private Long id;
@TableId(type = IdType.ID_WORKER) 数值类型  数据库中也必须是数值类型 否则会报错
private Long id;
@TableId(type = IdType.ID_WORKER_STR) 字符串类型   数据库也要保证一样字符类型
private Long id;
@TableId(type = IdType.INPUT) 用户自定义了  数据类型和数据库保持一致就行
private Long id;
 
 
2，全局主键策略实现
 需要在application.yml文件中
添加
mybatis-plus:
  mapper-locations:
    - com/mp/mapper/*
  global-config:
    db-config:
      id-type: uuid/none/input/id_worker/id_worker_str/auto   表示全局主键都采用该策略（如果全局策略和局部策略都有设置，局部策略优先级高）
      
      
      
 
 分页：服务类中只返回查询的数据集合List<E>，通过page判断是否分页true false判断